import eslintJs from '@eslint/js';
import parserTs from '@typescript-eslint/parser';
import eslintTs from '@typescript-eslint/eslint-plugin';
import globals from 'globals';
import eslintPrettier from 'eslint-plugin-prettier';
import eslintVitest from 'eslint-plugin-vitest';

const fastEslint = process.env.ESLINT_FAST === 'true';

export default [
    {
        files: ['{common,stages}/**/*.{js,jsx,ts,tsx,mjs,cjs,mts,cts}'],
        ignores: [
            '**/node_modules/**/*',
            '{common,stages}/*/lib/**/*',
            '{common,stages}/*/types/**/*',
            '{common,stages}/*/build/**/*',
            '{common,stages}/*/vite.config.mts',
            '{common,stages}/*/vitest.config.mts',
        ],
        languageOptions: {
            ecmaVersion: 'latest',
            globals: {
                ...globals.node
            }
        },
        plugins: {
            prettier: eslintPrettier,
            vitest: eslintVitest
        },
        rules: {
            ...eslintJs.configs.recommended.rules,
            ...eslintVitest.configs.recommended.rules,
            'prettier/prettier': ['error', {}, { usePrettierrc: true }]
        }
    },
    {
        files: ['{common,stages}/**/*.{ts,tsx,mts,cts}'],
        ignores: [
            '**/node_modules/**/*',
            '{common,stages}/*/lib/**/*',
            '{common,stages}/*/types/**/*',
            '{common,stages}/*/build/**/*',
            '{common,stages}/*/vite.config.mts',
            '{common,stages}/*/vitest.config.mts',
        ],
        languageOptions: {
            parser: parserTs,
            parserOptions: {
                project: true
            }
        },
        plugins: {
            '@typescript-eslint': eslintTs
        },
        rules: {
            ...eslintTs.configs['eslint-recommended'].overrides[0].rules,
            ...(fastEslint
                ? eslintTs.configs.recommended.rules
                : eslintTs.configs['recommended-type-checked'].rules),
            '@typescript-eslint/no-misused-promises': [
                'error',
                {
                    checksVoidReturn: {
                        arguments: false,
                        attributes: false,
                        properties: true,
                        returns: true,
                        variables: true
                    }
                }
            ]
        }
    }
];
