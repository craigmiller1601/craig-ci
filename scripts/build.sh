#!/bin/bash

set -euo pipefail

BUILD_DIR=./build

if [ -d "$BUILD_DIR" ]; then
  rm -rf "$BUILD_DIR"
fi

mkdir "$BUILD_DIR"

pnpm -r run build
stages=$(ls stages/*/build/*.js)
for stage in $stages; do
  cp "$stage" "$BUILD_DIR"
done