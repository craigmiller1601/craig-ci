# Craig-CI

This is the custom CI/CD pipeline supporting Craig Miller's application development.

## Development

There is a `rapid-prototype.sh` script that builds, validates, and deploys everything locally to support accelerated testing speed.

Also, when building this project via GitLab CI, the concurrent docker phases have intermittent failures. If one of them fails, just run it again. I've done a lot to make it stable, not sure where the intermittent failures come from.

### Special Note About Cypress

The docker image at `base-images/cypress` requires a specific Google Chrome version. Google does not consistently maintain the hosting of their old images, and finding the correct version is hard. Cypress provides the necessary links to see version lists at this url: https://github.com/cypress-io/cypress-docker-images/blob/master/factory/README.md.

## Usage

### Branch Patterns

The pipeline will trigger for branches with the following names:

- `main` = this is the primary pipeline for the primary brach.
- `integration` = this is for running pipelines on any arbitrary code. For example, if an old version needs to be patched, checkout the tag, then create the `integration` branch with the changes.

### Maven Library

A Java project using the Maven build tool that generates a library artifact.

Monorepo Supported: No

#### Expected Project Structure

```
/
    pom.xml
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/maven-library.yml'

variables:
  # Optional - if true, will setup GPG key for artifact signing
  SIGN_ARTIFACT: true
  # Optional - the repository to publish release artifacts to. 'nexus' or 'central', defaults to 'nexus'
  OVERRIDE_RELEASE_REPO: ###
  # Optional - the repository to publish snapshot artifacts to. 'nexus' or 'central', defaults to 'nexus'
  OVERRIDE_SNAPSHOT_REPO: ###
```

#### Additional Details

- For artifact signing, the GPG key ID will be exposed via an environment variable called `CRAIGMILLER160_GPG_KEY_ID`, and the GPG key password will be exposed via an environment variable called `CRAIGMILLER160_GPG_KEY_PASSWORD`. The pom.xml must be configured to use these variables.

### Maven Application

A Java project using the Maven build tool that is deployed to Kubernetes.

Monorepo Supported: No

#### Expected Project Structure

```
/
    deploy/
        chart/
            Chart.yaml
            values.yaml
        # Optional terraform directory, if present terraform scripts will be run
        terraform/
    pom.xml
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/maven-application.yml'

variables:
  # Required = The namespace to deploy the application to
  K8S_NAMESPACE: XXXX
  
  # Optional = A string of extra helm CLI values arguments. Used to provide secret values from CI variables.
  HELM_EXTRA_VALUES: XXXX

  # Optional = If the application needs an env suffix (ie, -prod)
  K8S_DEPLOY_ENV: XXXX

  # Optional = If there is a terraform script, and it needs some additional variables to run
  TERRAFORM_VARS: XXXX
```

### NPM Library

An NPM library.

Monorepo Supported: No

#### Expected Project Structure

```
/
    package.json
    # Either package-lock.json, yarn.lock, or pnpm-lock.yaml must be present
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/npm-library.yml'

variables:
  # Optional - If set to true, the pipeline expects to find a 'cypress' NPM script and will execute it in a special image for cypress.
  CYPRESS_INCLUDED: true
```

### NPM Application

An NPM application that is deployed to Kubernetes.

Monorepo Supported: No

#### Expected Project Structure

```
/
    deploy/
        chart/
            Chart.yaml
            values.yaml
        # Optional terraform directory, if present terraform scripts will be run
        terraform/
    package.json
    # Either package-lock.json, yarn.lock, or pnpm-lock.yaml must be present
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/npm-application.yml'

variables:
  # Required = The namespace to deploy the application to
  K8S_NAMESPACE: XXXX
  
  # Optional = A string of extra helm CLI values arguments. Used to provide secret values from CI variables.
  HELM_EXTRA_VALUES: XXXX

  # Optional = If the application needs an env suffix (ie, -prod)
  K8S_DEPLOY_ENV: XXXX

  # Optional = If there is a terraform script, and it needs some additional variables to run
  TERRAFORM_VARS: XXXX
  
  # Optional - If set to true, the pipeline expects to find a 'cypress' NPM script and will execute it in a special image for cypress.
  CYPRESS_INCLUDED: true
```

### Gradle Library

A Java project using the Gradle build tool that generates a library artifact.

Monorepo Supported: No

#### Expected Project Structure

Using the Groovy DSL

```
/
    build.gradle
```

Using the Kotlin DSL

```
/
    build.gradle.kts
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/gradle-library.yml'
```

### Gradle Application

A Java project using the Gradle build tool that is deployed to Kubernetes.

Monorepo Supported: No

#### Expected Project Structure

Using the Groovy DSL

```
/
    deploy/
        chart/
            Chart.yaml
            values.yaml
        # Optional terraform directory, if present terraform scripts will be run
        terraform/
    build.gradle
```

Using the Kotlin DSL

```
/
    deploy/
        chart/
            Chart.yaml
            values.yaml
        # Optional terraform directory, if present terraform scripts will be run
        terraform/
    build.gradle.kts
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/gradle-application.yml'

variables:
  # Required = The namespace to deploy the application to
  K8S_NAMESPACE: XXXX
  
  # Optional = A string of extra helm CLI values arguments. Used to provide secret values from CI variables.
  HELM_EXTRA_VALUES: XXXX

  # Optional = If the application needs an env suffix (ie, -prod)
  K8S_DEPLOY_ENV: XXXX

  # Optional = If there is a terraform script, and it needs some additional variables to run
  TERRAFORM_VARS: XXXX
```

### Helm Library

A library that creates a Helm chart for applications to use.

Monorepo Supported: No

#### Expected Project Structure

```
/
    deploy/
        chart/
            Chart.yaml
            values.yaml
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/helm-library.yml'
```

### Helm Application

An application using the Helm tool that is deployed to Kubernetes.

Monorepo Supported: No

#### Expected Project Structure

```
/
    deploy/
        chart/
            Chart.yaml
            values.yaml
        # Optional terraform directory, if present terraform scripts will be run
        terraform/
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/helm-application.yml'

variables:
  # Required = The namespace to deploy the application to
  K8S_NAMESPACE: XXXX
  
  # Optional = A string of extra helm CLI values arguments. Used to provide secret values from CI variables.
  HELM_EXTRA_VALUES: XXXX

  # Optional = If the application needs an env suffix (ie, -prod)
  K8S_DEPLOY_ENV: XXXX

  # Optional = If there is a terraform script, and it needs some additional variables to run
  TERRAFORM_VARS: XXXX
```

### Docker Image

A docker image to be consumed by other applications.

Monorepo Supported: No

#### Expected Project Structure

```
/
    deploy/
        Dockerfile
    docker.json
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/docker-image.yml'
```

### Docker Application

A project that builds a docker image for an application that deploys to Kubernetes.

Monorepo Supported: No

#### Expected Project Structure

```
/
    deploy/
        chart/
            Chart.yaml
            values.yaml
        Dockerfile
        # Optional terraform directory, if present terraform scripts will be run
        terraform/
    docker.json
```

#### Sample Pipeline

```yaml
include:
  - project: craigmiller1601/craig-ci
    ref: main
    file: 'workflows/docker-application.yml'

variables:
  # Required = The namespace to deploy the application to
  K8S_NAMESPACE: XXXX
  
  # Optional = A string of extra helm CLI values arguments. Used to provide secret values from CI variables.
  HELM_EXTRA_VALUES: XXXX

  # Optional = If the application needs an env suffix (ie, -prod)
  K8S_DEPLOY_ENV: XXXX

  # Optional = If there is a terraform script, and it needs some additional variables to run
  TERRAFORM_VARS: XXXX
```