#!/bin/sh

if [ ! -d /kaniko/.docker ]; then
  mkdir -p /kaniko/.docker
fi

cat <<EOF > /kaniko/.docker/config.json
{
  "auths": {
    "nexus-docker-secure.nexus/v1": {
      "auth": "$(echo -n ${NEXUS_USER}:${NEXUS_PASSWORD} | base64)"
    },
    "nexus-docker-secure.nexus/v2": {
      "auth": "$(echo -n ${NEXUS_USER}:${NEXUS_PASSWORD} | base64)"
    }
  }
}
EOF

