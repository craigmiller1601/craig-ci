#!/bin/bash

set -euo pipefail

mv /kaniko-2 /kaniko

echo | openssl s_client \
  -showcerts \
  -servername nexus-docker-secure.nexus \
  -connect nexus-docker-secure.nexus:443 2>/dev/null | \
  openssl x509 -outform PEM > /kaniko/ssl/nexus.pem

if [ -f /scripts/kaniko-auth.sh ]; then
  /scripts/kaniko-auth.sh
else
  /kaniko-auth.sh
fi