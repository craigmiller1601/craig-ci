#!/bin/bash

set -euo pipefail

GIT_HOOK_DIR="$CI_PROJECT_DIR/.git/hooks"

echo "Initializing stage"
if [ ! -d "$GIT_HOOK_DIR" ]; then
  echo "Creating git hooks directory"
  mkdir -p "$CI_PROJECT_DIR/.git/hooks"
fi