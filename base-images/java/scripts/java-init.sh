#!/bin/bash

set -euo pipefail

MAVEN_HOME="$CI_PROJECT_DIR/maven"
GRADLE_HOME="$CI_PROJECT_DIR/gradle"

echo "Setting up maven home: $MAVEN_HOME"
if [ ! -d "$MAVEN_HOME" ]; then
  echo "Creating maven home directory"
  mkdir "$MAVEN_HOME"
else
  echo "Maven home directory already exists"
fi
echo "Copying settings.xml to maven home directory"
cp /files/settings.xml "$MAVEN_HOME"

echo "Setting up gradle home $GRADLE_HOME"
if [ ! -d "$GRADLE_HOME" ]; then
  echo "Creating gradle home directory"
  mkdir "$GRADLE_HOME"
else
  echo "Gradle home directory already exists"
fi
echo "Copying init.gradle.kts to gradle home directory"
cp /files/init.gradle.kts "$GRADLE_HOME"
