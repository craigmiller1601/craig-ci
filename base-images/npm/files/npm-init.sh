#!/bin/bash

set -euo pipefail

set +u
ci_project_dir="$CI_PROJECT_DIR"
nexus_npm_token="$NEXUS_NPM_TOKEN"
set -u

PNPM_STORE_DIR="$ci_project_dir/pnpm"

echo "Setting up PNPM store directory: $PNPM_STORE_DIR"
if [ ! -d "$PNPM_STORE_DIR" ]; then
  echo "Creating PNPM store directory"
  mkdir "$PNPM_STORE_DIR"
else
  echo "PNPM store directory already exists"
fi
pnpm config set store-dir "$PNPM_STORE_DIR"

echo "Setting authentication token to global NPM config for artifact publishing"
echo "//nexus.craigmiller160.us/repository/npm-private/:_authToken=${nexus_npm_token}" >> "$(npm config get globalconfig)"