#!/bin/bash

set -euo pipefail

npm_build() {
  pnpm install
  pnpm build
  pnpm validate
}

base_image_build() {
  image="$1"
  tag="nexus-docker.craigmiller160.us/craig-ci/${image}:latest"

  echo "Building $tag"

  (
    cd "./base-images/$image"

    docker build \
      --platform=linux/amd64 \
      -t "$tag" \
      --build-arg='NPM_REGISTRY=https://nexus.craigmiller160.us/repository/npm-group' \
      .

    docker push \
      "$tag"
  )
  sleep 1
}

stage_image_build() {
  stage_dir="$1"
  image="$2"
  tag="nexus-docker.craigmiller160.us/craig-ci/${image}:latest"

  image_dir_path="./base-images/$stage_dir"
  js_file_path="./stages/$image/build/$image.js"

  find "$image_dir_path" -name "*.js" -exec rm {} \;
  cp "$js_file_path" "$image_dir_path"

  echo "Building $tag"

  (
    cd "$image_dir_path"

    docker build \
      --platform=linux/amd64 \
      -t "$tag" \
      --build-arg='NPM_REGISTRY=https://nexus.craigmiller160.us/repository/npm-group' \
      .

    docker push \
      "$tag"
  )
  sleep 1
}

npm_build
base_image_build "core"
base_image_build "java"
base_image_build "npm"

stage_image_build "java-stages" "project-info-maven"
stage_image_build "java-stages" "build-and-test-maven"
stage_image_build "java-stages" "publish-artifact-maven"
stage_image_build "java-stages" "project-validation-maven"
stage_image_build "git-stages" "git-tag"
stage_image_build "argocd-stages" "deploy-argocd"
stage_image_build "core-stages" "project-info-helm"
stage_image_build "terraform-stages" "run-terraform"
stage_image_build "helm-stages" "build-and-test-helm"
stage_image_build "core-stages" "project-info-docker"
stage_image_build "core-stages" "project-validation-docker"
stage_image_build "java-stages" "project-info-gradle"
stage_image_build "java-stages" "build-and-test-gradle"
stage_image_build "java-stages" "project-validation-gradle"
stage_image_build "java-stages" "publish-artifact-gradle"
stage_image_build "npm-stages" "project-info-npm"
stage_image_build "npm-stages" "build-and-test-npm"
stage_image_build "npm-stages" "project-validation-npm"
stage_image_build "npm-stages" "publish-artifact-npm"
stage_image_build "npm-stages" "install-dependencies-npm"
stage_image_build "cypress" "run-cypress"
stage_image_build "helm-stages" "validate-helm"