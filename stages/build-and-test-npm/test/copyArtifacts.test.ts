import { test, beforeEach, afterEach, expect } from 'vitest';
import path from 'path';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { match } from 'ts-pattern';
import fs from 'fs';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { copyArtifacts } from '../src/copyArtifacts';
import { runCommand } from '@craig-ci/context/cmd';
import { either } from 'fp-ts';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'npmBuildCopyArtifacts'
);
const appWorkingDir = path.join(WORKING_DIR_ROOT, 'application');
const libWorkingDir = path.join(WORKING_DIR_ROOT, 'library');
const libMissingFilesWorkingDir = path.join(
    WORKING_DIR_ROOT,
    'libraryMissingFiles'
);

const clean = (workingDir: string) =>
    fs
        .readdirSync(workingDir)
        .filter((file) => file === 'artifacts')
        .forEach((file) =>
            fs.rmSync(path.join(workingDir, file), {
                recursive: true,
                force: true
            })
        );

beforeEach(() => {
    clean(appWorkingDir);
    clean(libWorkingDir);
    clean(libMissingFilesWorkingDir);
});

afterEach(() => {
    clean(appWorkingDir);
    clean(libWorkingDir);
    clean(libMissingFilesWorkingDir);
});

type NpmProjectType = Extract<
    ProjectType,
    'NpmLibrary' | 'NpmApplication' | 'abc'
>;

const getWorkingDir = (projectType: NpmProjectType): string =>
    match(projectType)
        .with('NpmLibrary', () => libWorkingDir)
        .with('NpmApplication', () => appWorkingDir)
        .exhaustive();

const createContext = (projectType: NpmProjectType): BuildContext => ({
    projectType,
    projectInfo: {
        group: 'craigmiller160',
        name: 'test',
        version: '1.0.0',
        versionType: 'Release',
        repoType: 'polyrepo'
    }
});

const validateLibraryFiles = (artifactsDir: string) => {
    const helloFile = path.join(artifactsDir, 'hello.js');
    expect(fs.existsSync(helloFile)).toBe(true);

    const abcFile = path.join(artifactsDir, 'lib', 'abc.js');
    expect(fs.existsSync(abcFile)).toBe(true);

    const worldFile = path.join(artifactsDir, 'lib', 'world.js');
    expect(fs.existsSync(worldFile)).toBe(true);
};

const validateApplicationFiles = async (artifactsDir: string) => {
    const tarFile = path.join(artifactsDir, 'test.tgz');
    expect(fs.existsSync(tarFile)).toBe(true);
    const result = await runCommand(`tar -tvf ${tarFile}`)();
    expect(result).toBeRight();
    const tarFiles = (result as either.Right<string>).right
        .split('\n')
        .filter((line) => line.trim().length > 0)
        .map((line) => {
            const parts = line.split(/\s+/);
            return parts[parts.length - 1];
        });
    expect(tarFiles).toContain('./');
    expect(tarFiles).toContain('./index.html');
    expect(tarFiles).toContain('./world.js');
    expect(tarFiles).toContain('./hello.js');
};

test('handles library where some items in files array are not present', async () => {
    const context = createContext('NpmLibrary');

    const result = await copyArtifacts(context, libMissingFilesWorkingDir)();
    expect(result).toBeRight();

    validateLibraryFiles(path.join(libMissingFilesWorkingDir, 'artifacts'));
});

test.each<NpmProjectType>(['NpmLibrary', 'NpmApplication'])(
    'Copies artifacts after build for %s',
    async (projectType) => {
        const workingDir = getWorkingDir(projectType);
        const context = createContext(projectType);

        const result = await copyArtifacts(context, workingDir)();
        expect(result).toBeRight();

        const artifactsDir = path.join(workingDir, 'artifacts');
        expect(fs.existsSync(artifactsDir)).toBe(true);

        if (projectType === 'NpmLibrary') {
            validateLibraryFiles(artifactsDir);
        } else {
            await validateApplicationFiles(artifactsDir);
        }
    }
);
