import { test, beforeEach, vi, expect, MockedFunction } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { buildAndTest } from '../src/buildAndTest';
import { taskEither } from 'fp-ts';

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

beforeEach(() => {
    vi.resetAllMocks();
});

test('Runs build & validate scripts', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const result = await buildAndTest(runCommandMock)();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(2);
    expect(runCommandMock).toHaveBeenNthCalledWith(1, 'npm run build', {
        returnOutput: false
    });
    expect(runCommandMock).toHaveBeenNthCalledWith(2, 'npm run validate', {
        returnOutput: false
    });
});
