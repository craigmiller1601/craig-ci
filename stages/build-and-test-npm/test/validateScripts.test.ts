import { test, expect, beforeEach, vi, MockedFunction } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import fs from 'fs';
import { taskEither } from 'fp-ts';
import { validateScripts } from '../src/validateScripts';

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();
const npmOuptutDir = path.join(__dirname, '__npm-output__');

beforeEach(() => {
    vi.resetAllMocks();
});

type Scenario = 'all-scripts' | 'only-validate' | 'only-build' | 'none';

test.each<Scenario>(['all-scripts', 'only-validate', 'only-build', 'none'])(
    'Validates available scripts for scenario %s',
    async (scenario) => {
        const outputJsonPath = path.join(npmOuptutDir, `${scenario}.json`);
        const outputJson = fs.readFileSync(outputJsonPath, 'utf8');
        runCommandMock.mockImplementation(() => taskEither.right(outputJson));

        const result = await validateScripts(runCommandMock)();
        if (scenario === 'all-scripts') {
            expect(result).toBeRight();
        } else {
            expect(result).toEqualLeft(
                new Error(
                    'Project must have validate & build scripts for pipeline'
                )
            );
        }

        expect(runCommandMock).toHaveBeenCalledWith('npm run --json');
    }
);
