import { function as func, taskEither } from 'fp-ts';
import { readContext } from '@craig-ci/context/io';
import { validateScripts } from './validateScripts';
import { buildAndTest } from './buildAndTest';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { copyArtifacts } from './copyArtifacts';

console.log('Building and testing npm project');

void func.pipe(
    readContext(),
    taskEither.fromEither,
    taskEither.chainFirst(() => validateScripts()),
    taskEither.chainFirst(() => buildAndTest()),
    taskEither.chain(copyArtifacts),
    terminateTaskEither
)();
