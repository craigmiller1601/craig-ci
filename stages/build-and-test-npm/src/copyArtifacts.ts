import {
    either,
    taskEither,
    function as func,
    option,
    readonlyArray
} from 'fp-ts';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { isApplication } from '@craig-ci/context/utils/projectTypeUtils';
import { PackageJson, readNpmProjectFile } from '@craig-ci/npm-support';
import path from 'path';
import { cpSync, existsSync, mkdirSync } from '@craig-ci/fp-io';
import { runCommand } from '@craig-ci/context/cmd';

const extractFilesFromPackageJson = (
    packageJson: PackageJson
): either.Either<Error, ReadonlyArray<string>> =>
    func.pipe(
        option.fromNullable(packageJson.files),
        either.fromOption(
            () =>
                new Error(
                    'Project does not specify files, cannot copy library artifacts'
                )
        )
    );

const copyFiles =
    (cwd: string, artifactsDir: string) =>
    (files: ReadonlyArray<string>): either.Either<Error, void> =>
        func.pipe(
            files,
            readonlyArray.map((file) => [
                path.join(cwd, file),
                path.join(artifactsDir, file)
            ]),
            readonlyArray.filter(([src]) => existsSync(src)),
            readonlyArray.map(([src, dest]) =>
                cpSync(src, dest, {
                    recursive: true
                })
            ),
            either.sequenceArray,
            either.map(() => func.constVoid())
        );

const ensureDirectory = (directory: string): either.Either<Error, void> => {
    if (existsSync(directory)) {
        return either.right(func.constVoid());
    }

    return mkdirSync(directory);
};

const copyLibraryArtifacts = (cwd: string): either.Either<Error, void> => {
    const artifactsDir = path.join(cwd, 'artifacts');
    return func.pipe(
        ensureDirectory(artifactsDir),
        either.chain(() => readNpmProjectFile(cwd)),
        either.chain(extractFilesFromPackageJson),
        either.chain(copyFiles(cwd, artifactsDir))
    );
};

const copyApplicationArtifacts = (
    context: BuildContext,
    cwd: string
): taskEither.TaskEither<Error, void> => {
    const buildDir = path.join(cwd, 'build');
    if (!existsSync(buildDir)) {
        return taskEither.left(
            new Error(
                'No build directory found. All NPM applications must output their files to the build directory'
            )
        );
    }
    const artifactsDir = path.join(cwd, 'artifacts');
    const tarOutputFile = path.join(
        artifactsDir,
        `${context.projectInfo.name}.tgz`
    );

    return func.pipe(
        ensureDirectory(artifactsDir),
        taskEither.fromEither,
        taskEither.chain(() =>
            runCommand(`tar -czvf ${tarOutputFile} -C ${buildDir} .`)
        ),
        taskEither.map(() => func.constVoid())
    );
};

export const copyArtifacts = (
    context: BuildContext,
    cwd: string = process.cwd()
): taskEither.TaskEither<Error, void> => {
    console.log('Copying artifacts from build');
    if (isApplication(context.projectType)) {
        return copyApplicationArtifacts(context, cwd);
    }
    return func.pipe(copyLibraryArtifacts(cwd), taskEither.fromEither);
};
