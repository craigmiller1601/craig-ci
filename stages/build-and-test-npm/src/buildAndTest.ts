import {
    runCommand as defaultRunCommand,
    RunCommandType
} from '@craig-ci/context/cmd';
import { taskEither, function as func } from 'fp-ts';

export const buildAndTest = (
    runCommand: RunCommandType = defaultRunCommand
): taskEither.TaskEither<Error, string> =>
    func.pipe(
        runCommand('npm run build', {
            returnOutput: false
        }),
        taskEither.chain(() =>
            runCommand('npm run validate', {
                returnOutput: false
            })
        )
    );
