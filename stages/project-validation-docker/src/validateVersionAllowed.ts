import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither } from 'fp-ts';
import {
    NexusRepoGroupSearchFn,
    searchForDockerReleases as defaultSearchForDockerReleases
} from '@craig-ci/nexus-api';
import { validateProjectVersionAllowed } from '@craig-ci/project-version-validation';

export const validateDockerProjectVersionAllowed = (
    context: BuildContext,
    searchForDockerReleases: NexusRepoGroupSearchFn = defaultSearchForDockerReleases
): taskEither.TaskEither<Error, BuildContext> =>
    validateProjectVersionAllowed(context, searchForDockerReleases);
