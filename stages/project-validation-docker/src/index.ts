import { function as func, taskEither } from 'fp-ts';
import { readContext } from '@craig-ci/context/io';
import { validateDockerProjectVersionAllowed } from './validateVersionAllowed';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Validating docker project');

void func.pipe(
    readContext(),
    taskEither.fromEither,
    taskEither.chain(validateDockerProjectVersionAllowed),
    terminateTaskEither
)();
