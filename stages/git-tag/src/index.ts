import { function as func, taskEither } from 'fp-ts';
import { readContext } from '@craig-ci/context/io';
import { gitTag } from './gitTag';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Performing git tag');

void func.pipe(
    readContext(),
    taskEither.fromEither,
    taskEither.chain(gitTag),
    terminateTaskEither
)();
