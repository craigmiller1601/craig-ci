import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import { taskEither, function as func, either } from 'fp-ts';
import { isPreRelease } from '@craig-ci/context/utils/versionTypeUtils';

type ProcessEnv = typeof process.env;

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    env: ProcessEnv;
}>;

const convertRepositoryUrl = (
    env: ProcessEnv
): either.Either<Error, string> => {
    const repoUrl = env['CI_REPOSITORY_URL'];
    if (!repoUrl) {
        return either.left(
            new Error('Cannot find CI_REPOSITORY_URL env variable')
        );
    }

    const ending = repoUrl.split('@')[1];
    const url = `https://oauth2:$ACCESS_TOKEN@${ending}`;

    console.log(`Repository URL: ${url}`);
    return either.right(url);
};

export const gitTag = (
    context: BuildContext,
    dependencies: Dependencies = {
        runCommand,
        env: process.env
    }
): taskEither.TaskEither<Error, BuildContext> => {
    if (isPreRelease(context.projectInfo.versionType)) {
        console.log('Skipping tagging for pre-release version');
        return taskEither.right(context);
    }

    return func.pipe(
        dependencies.runCommand('git config user.email "craig-ci@gitlab.com"'),
        taskEither.chain(() =>
            dependencies.runCommand('git config user.name "craig-ci"')
        ),
        taskEither.chain(() =>
            func.pipe(
                convertRepositoryUrl(dependencies.env),
                taskEither.fromEither,
                taskEither.chain((repoUrl) =>
                    dependencies.runCommand(
                        `git remote add gitlab_origin ${repoUrl}`
                    )
                )
            )
        ),
        taskEither.chain(() =>
            dependencies.runCommand(`git tag ${context.projectInfo.version}`)
        ),
        taskEither.chain(() =>
            dependencies.runCommand('git push gitlab_origin --tags')
        ),
        taskEither.map(() => context)
    );
};
