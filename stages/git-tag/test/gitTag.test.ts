import { beforeEach, expect, MockedFunction, test, vi } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { gitTag } from '../src/gitTag';
import { isPreRelease } from '@craig-ci/context/utils/versionTypeUtils';
import { taskEither } from 'fp-ts';

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

const createBuildContext = (versionType: VersionType): BuildContext => ({
    projectType: 'MavenLibrary',
    projectInfo: {
        group: 'us.craigmiller160',
        name: 'project',
        version: '1.0.0',
        versionType,
        repoType: 'polyrepo'
    }
});

test('Fails if the CI_REPOSITORY_URL variable is not present', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const context = createBuildContext('Release');
    const result = await gitTag(context, {
        runCommand: runCommandMock,
        env: {}
    })();
    expect(result).toEqualLeft(
        new Error('Cannot find CI_REPOSITORY_URL env variable')
    );
});

test.each<VersionType>(['Release', 'PreRelease'])(
    'Performs a git tag for version type %s',
    async (type) => {
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const context = createBuildContext(type);
        const result = await gitTag(context, {
            runCommand: runCommandMock,
            env: {
                CI_REPOSITORY_URL:
                    'https://gitlab-ci-token:abcdefg@gitlab.com/craigmiller1601/oauth2-utils-core.git'
            }
        })();
        expect(result).toEqualRight(context);

        if (isPreRelease(type)) {
            expect(runCommandMock).not.toHaveBeenCalled();
            return;
        }

        expect(runCommandMock).toHaveBeenCalledTimes(5);
        expect(runCommandMock).toHaveBeenNthCalledWith(
            1,
            'git config user.email "craig-ci@gitlab.com"'
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(
            2,
            'git config user.name "craig-ci"'
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(
            3,
            'git remote add gitlab_origin https://oauth2:$ACCESS_TOKEN@gitlab.com/craigmiller1601/oauth2-utils-core.git'
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(
            4,
            `git tag ${context.projectInfo.version}`
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(
            5,
            'git push gitlab_origin --tags'
        );
    }
);
