import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { either, function as func } from 'fp-ts';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import {
    applicationVersionTimestamp,
    AppVersionTimestamp
} from '@craig-ci/context/date';
import { readNpmProjectFile } from '@craig-ci/npm-support';
import { isApplication } from '@craig-ci/context/utils/projectTypeUtils';

const BETA_VERSION_REGEX = /^.*-beta/;

type Dependencies = Readonly<{
    cwd: string;
    applicationVersionTimestamp: AppVersionTimestamp;
}>;

export const npmSeparateGroupAndName = (fullName: string): [string, string] => {
    const nameParts = fullName.split('/');
    const group = nameParts.length === 2 ? nameParts[0].replace(/^@/, '') : '';
    const name = nameParts.length === 2 ? nameParts[1] : nameParts[0];
    return [group, name];
};

export const getProjectInfo = (
    projectType: ProjectType,
    dependencies: Dependencies = {
        cwd: process.cwd(),
        applicationVersionTimestamp
    }
): either.Either<Error, ProjectInfo> =>
    func.pipe(
        readNpmProjectFile(dependencies.cwd),
        either.map((packageJson): ProjectInfo => {
            const [group, name] = npmSeparateGroupAndName(packageJson.name);
            return {
                group,
                name,
                version: isApplication(projectType)
                    ? dependencies.applicationVersionTimestamp()
                    : packageJson.version,
                versionType:
                    isApplication(projectType) ||
                    !BETA_VERSION_REGEX.test(packageJson.version)
                        ? 'Release'
                        : 'PreRelease',
                repoType: 'polyrepo'
            };
        })
    );
