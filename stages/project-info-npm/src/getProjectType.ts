import { either } from 'fp-ts';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { getDeployDir, getNpmProjectFile } from '@craig-ci/npm-support';
import { existsSync } from '@craig-ci/fp-io';

export const getProjectType = (
    cwd: string = process.cwd()
): either.Either<Error, ProjectType> => {
    const projectFile = getNpmProjectFile(cwd);
    const deployDir = getDeployDir(cwd);

    if (!existsSync(projectFile)) {
        return either.left(new Error('Unable to identify NPM project type'));
    }

    if (existsSync(deployDir)) {
        return either.right('NpmApplication');
    }

    return either.right('NpmLibrary');
};
