import { test, expect } from 'vitest';
import path from 'path';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { match } from 'ts-pattern';
import { getProjectType } from '../src/getProjectType';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

const getWorkingDirName = (projectType: ProjectType): string =>
    match(projectType)
        .with('NpmLibrary', () => 'npmReleaseLibrary')
        .with('NpmApplication', () => 'npmReleaseApplication')
        .with('Unknown', () => '.')
        .run();

test.each<ProjectType>(['NpmLibrary', 'NpmApplication', 'Unknown'])(
    'Identifies the project type for %s',
    (projectType) => {
        const workingDirName = getWorkingDirName(projectType);
        const workingDir = path.join(WORKING_DIR_ROOT, workingDirName);
        const result = getProjectType(workingDir);
        if (projectType === 'Unknown') {
            expect(result).toBeLeft();
        } else {
            expect(result).toEqualRight(projectType);
        }
    }
);
