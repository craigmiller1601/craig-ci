import { test, expect } from 'vitest';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { match } from 'ts-pattern';
import path from 'path';
import { applicationVersionTimestamp } from '@craig-ci/context/date';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { getProjectInfo } from '../src/getProjectInfo';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

type Scenario = Readonly<{
    projectType: ProjectType;
    versionType: VersionType;
}>;

const getWorkingDirName = (scenario: Scenario): string =>
    match(scenario)
        .with(
            { projectType: 'NpmLibrary', versionType: 'PreRelease' },
            () => 'npmBetaLibrary'
        )
        .with(
            { projectType: 'NpmLibrary', versionType: 'Release' },
            () => 'npmReleaseLibrary'
        )
        .with({ projectType: 'NpmApplication' }, () => 'npmReleaseApplication')
        .run();

const getVersion = (scenario: Scenario, timestamp: string): string =>
    match(scenario)
        .with(
            { projectType: 'NpmLibrary', versionType: 'PreRelease' },
            () => '1.0.0-beta'
        )
        .with(
            { projectType: 'NpmLibrary', versionType: 'Release' },
            () => '1.0.0'
        )
        .with({ projectType: 'NpmApplication' }, () => timestamp)
        .run();

test.each<Scenario>([
    { projectType: 'NpmApplication', versionType: 'Release' },
    { projectType: 'NpmLibrary', versionType: 'PreRelease' },
    { projectType: 'NpmLibrary', versionType: 'Release' }
])(
    'Gets npm project info for $projectType and $versionType',
    ({ projectType, versionType }) => {
        const workingDirName = getWorkingDirName({ projectType, versionType });
        const workingDir = path.join(WORKING_DIR_ROOT, workingDirName);
        const timestamp = applicationVersionTimestamp();
        const version = getVersion({ projectType, versionType }, timestamp);

        const expected: ProjectInfo = {
            group: 'craigmiller160',
            name: 'craig-build',
            version,
            versionType,
            repoType: 'polyrepo'
        };

        const actual = getProjectInfo(projectType, {
            cwd: workingDir,
            applicationVersionTimestamp: () => timestamp
        });
        expect(actual).toEqualRight(expected);
    }
);
