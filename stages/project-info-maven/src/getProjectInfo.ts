import { either, function as func, readonlyArray, option } from 'fp-ts';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { getRawProjectData } from '@craig-ci/maven-support/getRawProjectData';
import path from 'path';
import { MavenModules } from '@craig-ci/maven-support/PomXml';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { match } from 'ts-pattern';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import {
    AppVersionTimestamp,
    applicationVersionTimestamp as defaultAppVersionTimestamp
} from '@craig-ci/context/date';
import { isApplication } from '@craig-ci/context/utils/projectTypeUtils';

const SNAPSHOT_VERSION_REGEX = /^.*-SNAPSHOT/;

const getVersionType = (version: string): VersionType =>
    match<string, VersionType>(version)
        .when(
            (_) => SNAPSHOT_VERSION_REGEX.test(_),
            () => 'PreRelease'
        )
        .otherwise(() => 'Release');

const getVersionAndType = (
    projectType: ProjectType,
    version: string,
    applicationVersionTimestamp: AppVersionTimestamp
): [string, VersionType] => {
    if (isApplication(projectType)) {
        return [applicationVersionTimestamp(), 'Release'];
    }

    return [version, getVersionType(version)];
};

const readMavenProjectInfo = (
    projectType: ProjectType,
    cwd: string = process.cwd(),
    applicationVersionTimestamp: AppVersionTimestamp,
    rootProjectInfo: ProjectInfo | undefined = undefined
): either.Either<Error, ProjectInfo> =>
    func.pipe(
        getRawProjectData(cwd),
        either.flatMap((pomXml) => {
            const version =
                pomXml.project.version?.[0] ?? rootProjectInfo?.version;
            const group = pomXml.project.groupId?.[0] ?? rootProjectInfo?.group;
            if (!version || !group) {
                return either.left(
                    new Error('Cannot find required fields from pom.xml')
                );
            }

            const [realVersion, versionType] = getVersionAndType(
                projectType,
                version,
                applicationVersionTimestamp
            );

            const projectInfo: ProjectInfo = {
                group,
                name: pomXml.project.artifactId[0],
                version: realVersion,
                versionType,
                repoType:
                    rootProjectInfo !== undefined ? 'monorepo' : 'polyrepo'
            };

            if (pomXml.project.modules && projectType === 'MavenApplication') {
                return either.left(
                    new Error('Monorepo not supported for this project type')
                );
            }

            if (pomXml.project.modules) {
                return func.pipe(
                    pomXml.project.modules,
                    readonlyArray.head,
                    option.getOrElse((): MavenModules => ({ module: [] })),
                    (m) => m.module,
                    readonlyArray.map((module) => path.join(cwd, module)),
                    readonlyArray.map((modulePath) =>
                        readMavenProjectInfo(
                            projectType,
                            modulePath,
                            applicationVersionTimestamp,
                            projectInfo
                        )
                    ),
                    either.sequenceArray,
                    either.map(
                        (monorepoChildren): ProjectInfo => ({
                            ...projectInfo,
                            monorepoChildren,
                            repoType: 'monorepo'
                        })
                    )
                );
            }
            return either.right(projectInfo);
        })
    );

export const getProjectInfo = (
    projectType: ProjectType,
    cwd: string = process.cwd(),
    applicationVersionTimestamp: AppVersionTimestamp = defaultAppVersionTimestamp
): either.Either<Error, ProjectInfo> =>
    readMavenProjectInfo(projectType, cwd, applicationVersionTimestamp);
