import { either } from 'fp-ts';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import {
    getDeployDirectory,
    getMavenProjectFilePath
} from '@craig-ci/maven-support/paths';
import { existsSync } from '@craig-ci/fp-io';

export const getProjectType = (
    cwd: string = process.cwd()
): either.Either<Error, ProjectType> => {
    const pomXmlExists = existsSync(getMavenProjectFilePath(cwd));
    const deployDirExists = existsSync(getDeployDirectory(cwd));

    if (pomXmlExists && deployDirExists) {
        return either.right('MavenApplication');
    }

    if (pomXmlExists) {
        return either.right('MavenLibrary');
    }

    return either.left(new Error('Unable to identify ProjectType'));
};
