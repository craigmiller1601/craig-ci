import { either, function as func } from 'fp-ts';
import { getProjectType } from './getProjectType';
import { getProjectInfo } from './getProjectInfo';
import { terminateEither } from '@craig-ci/context/terminate';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { writeContext } from '@craig-ci/context/io';

console.log('Getting project info for maven project');

func.pipe(
    getProjectType(),
    either.bindTo('projectType'),
    either.bind('projectInfo', ({ projectType }) =>
        getProjectInfo(projectType)
    ),
    either.map(
        ({ projectType, projectInfo }): BuildContext => ({
            projectType,
            projectInfo
        })
    ),
    either.chain(writeContext),
    terminateEither
);
