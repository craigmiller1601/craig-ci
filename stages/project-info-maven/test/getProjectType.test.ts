import { test, expect } from 'vitest';
import path from 'path';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { getProjectType } from '../src/getProjectType';
import { match } from 'ts-pattern';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

test.each<ProjectType>(['MavenLibrary', 'MavenApplication', 'Unknown'])(
    'Gets the project type for %s',
    (expectedProjectType) => {
        const suffix = match<ProjectType, string>(expectedProjectType)
            .with('MavenApplication', () => 'mavenReleaseApplication')
            .with('MavenLibrary', () => 'mavenReleaseLibrary')
            .otherwise(() => '');
        const cwd = path.join(WORKING_DIR_ROOT, suffix);
        const result = getProjectType(cwd);
        if (expectedProjectType === 'Unknown') {
            expect(result).toEqualLeft(
                new Error('Unable to identify ProjectType')
            );
        } else {
            expect(result).toEqualRight(expectedProjectType);
        }
    }
);
