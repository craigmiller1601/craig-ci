import { expect, test } from 'vitest';
import path from 'path';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { RepoType } from '@craig-ci/context/types/RepoType';
import { match } from 'ts-pattern';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { getProjectInfo } from '../src/getProjectInfo';
import { applicationVersionTimestamp } from '@craig-ci/context/date';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

type GetProjectIfoArgs = Readonly<{
    versionType: VersionType;
    repoType: RepoType;
}>;

type VersionAndRepoType = Readonly<{
    versionType: VersionType;
    repoType: RepoType;
}>;

type VersionTypeValues = Readonly<{
    workingDir: string;
    version: string;
}>;

type ProjectTypeValues = Readonly<{
    directory: string;
    name: string;
    version: string;
}>;

test.each<ProjectType>(['MavenApplication', 'MavenLibrary'])(
    'Maven getProjectInfo for %s',
    (projectType) => {
        const timestamp = applicationVersionTimestamp();
        const { directory, name, version } = match<
            ProjectType,
            ProjectTypeValues
        >(projectType)
            .with('MavenLibrary', () => ({
                directory: 'mavenReleaseLibrary',
                name: 'email-service',
                version: '1.2.0'
            }))
            .with('MavenApplication', () => ({
                directory: 'mavenReleaseApplication',
                name: 'email-service',
                version: timestamp
            }))
            .run();
        const workingDir = path.join(WORKING_DIR_ROOT, directory);

        const expectedProjectInfo: ProjectInfo = {
            group: 'io.craigmiller160',
            name,
            version,
            versionType: 'Release',
            repoType: 'polyrepo'
        };

        const result = getProjectInfo(projectType, workingDir, () => timestamp);
        expect(result).toEqualRight(expectedProjectInfo);
    }
);

test.each<GetProjectIfoArgs>([
    { versionType: 'Release', repoType: 'polyrepo' },
    { versionType: 'PreRelease', repoType: 'polyrepo' },
    { versionType: 'Release', repoType: 'monorepo' }
])(
    'Maven getProjectInfo for $versionType and $repoType',
    ({ versionType, repoType }) => {
        const { workingDir, version } = match<
            VersionAndRepoType,
            VersionTypeValues
        >({ versionType, repoType })
            .with({ versionType: 'Release', repoType: 'polyrepo' }, () => ({
                workingDir: 'mavenReleaseLibrary',
                version: '1.2.0'
            }))
            .with({ versionType: 'PreRelease', repoType: 'polyrepo' }, () => ({
                workingDir: 'mavenSnapshotLibrary',
                version: '1.2.0-SNAPSHOT'
            }))
            .with({ versionType: 'Release', repoType: 'monorepo' }, () => ({
                workingDir: 'mavenReleaseLibraryMonorepo',
                version: '1.2.0'
            }))
            .run();

        const cwd = path.resolve(WORKING_DIR_ROOT, workingDir);
        const projectType: ProjectType = 'MavenLibrary';

        const monorepoChildren: ReadonlyArray<ProjectInfo> = [
            {
                group: 'io.craigmiller160',
                name: 'email-service-child-1',
                version,
                versionType,
                repoType
            },
            {
                group: 'io.craigmiller160',
                name: 'email-service-child-2',
                version,
                versionType,
                repoType
            }
        ];

        const expectedProjectInfo: ProjectInfo = {
            group: 'io.craigmiller160',
            name: 'email-service',
            version,
            versionType,
            repoType,
            monorepoChildren:
                repoType === 'monorepo' ? monorepoChildren : undefined
        };
        const result = getProjectInfo(projectType, cwd);
        expect(result).toEqualRight(expectedProjectInfo);
    }
);
