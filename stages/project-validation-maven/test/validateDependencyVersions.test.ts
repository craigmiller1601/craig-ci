import { test, expect } from 'vitest';
import path from 'path';
import { match } from 'ts-pattern';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { validateDependencyVersions } from '../src/validateDependencyVersions';
import { RepoType } from '@craig-ci/context/types/RepoType';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';

type DependencyValidationScenario = 'all valid' | 'invalid dependencies';
type MonorepoDependencyValidationScenario =
    | DependencyValidationScenario
    | 'all valid with children'
    | 'invalid child dependencies';
type MavenDependencyValidationScenario =
    | MonorepoDependencyValidationScenario
    | 'invalid plugins'
    | 'invalid child plugins';

const mavenMonorepoScenarios: ReadonlyArray<MavenDependencyValidationScenario> =
    [
        'all valid with children',
        'invalid child dependencies',
        'invalid child plugins'
    ];
const mavenValidScenarios: ReadonlyArray<MavenDependencyValidationScenario> = [
    'all valid',
    'all valid with children'
];

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

const createBuildContext = (
    repoType: RepoType,
    versionType: VersionType
): BuildContext => ({
    projectType: 'MavenApplication',
    projectInfo: {
        group: 'us.craigmiller160',
        name: 'temp',
        version: isRelease(versionType) ? '1.0.0' : '1.0.0-SNAPSHOT',
        versionType,
        repoType
    }
});

test.each<MavenDependencyValidationScenario>([
    'all valid',
    'all valid with children',
    'invalid dependencies',
    'invalid plugins',
    'invalid child dependencies',
    'invalid child plugins'
])('validating dependencies for maven. Scenario: %s', (scenario) => {
    const workingDir = match(scenario)
        .with('all valid', () => 'mavenReleaseApplication')
        .with('all valid with children', () => 'mavenReleaseLibraryMonorepo')
        .with(
            'invalid dependencies',
            () => 'mavenReleaseApplicationBadDependency'
        )
        .with('invalid plugins', () => 'mavenReleaseApplicationBadPlugin')
        .with(
            'invalid child dependencies',
            () => 'mavenReleaseLibraryMonorepoInvalidDependency'
        )
        .with(
            'invalid child plugins',
            () => 'mavenReleaseLibraryMonorepoInvalidPlugin'
        )
        .exhaustive();
    const cwd = path.resolve(WORKING_DIR_ROOT, workingDir);
    const buildContext = createBuildContext(
        mavenMonorepoScenarios.includes(scenario) ? 'monorepo' : 'polyrepo',
        'Release'
    );

    const result = validateDependencyVersions(buildContext, cwd);
    if (mavenValidScenarios.includes(scenario)) {
        expect(result).toEqualRight(buildContext);
    } else {
        expect(result).toEqualLeft(
            new Error(
                'Cannot have SNAPSHOT dependencies or plugins in Maven release'
            )
        );
    }
});

test('permissive versioning for pre-release projects', () => {
    const buildContext = createBuildContext('polyrepo', 'PreRelease');
    const cwd = path.resolve(
        WORKING_DIR_ROOT,
        'mavenReleaseApplicationBadDependency'
    );

    const result = validateDependencyVersions(buildContext, cwd);
    expect(result).toEqualRight(buildContext);
});
