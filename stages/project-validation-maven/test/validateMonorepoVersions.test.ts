import { expect, test } from 'vitest';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { validateMonorepoVersions } from '../src/validateMonorepoVersions';
import { RepoType } from '@craig-ci/context/types/RepoType';

const createBuildContext = (
    parentVersionType: VersionType,
    childVersionTypes: ReadonlyArray<VersionType>,
    repoType: RepoType
): BuildContext => {
    const projectInfo: ProjectInfo = {
        group: 'us.craigmiller160',
        name: 'temp',
        version: isRelease(parentVersionType) ? '1.0.0' : '1.0.0-SNAPSHOT',
        versionType: parentVersionType,
        repoType
    };
    return {
        projectType: 'MavenLibrary',
        projectInfo: {
            ...projectInfo,
            monorepoChildren: childVersionTypes.map((versionType) => ({
                ...projectInfo,
                version: isRelease(versionType) ? '1.0.0' : '1.0.0-SNAPSHOT',
                versionType
            }))
        }
    };
};

type ExecuteArgs = Readonly<{
    label: string;
    parentVersionType: VersionType;
    childVersionTypes: ReadonlyArray<VersionType>;
    valid: boolean;
}>;

test.each<ExecuteArgs>([
    {
        label: 'All version types are release',
        parentVersionType: 'Release',
        childVersionTypes: ['Release', 'Release'],
        valid: true
    },
    {
        label: 'Parent version type is pre-release, children are mixed',
        parentVersionType: 'PreRelease',
        childVersionTypes: ['Release', 'PreRelease'],
        valid: true
    },
    {
        label: 'Parent version type is release, children are mixed',
        parentVersionType: 'Release',
        childVersionTypes: ['Release', 'PreRelease'],
        valid: false
    }
])(
    'validateMonorepoVersion executes for $label',
    ({ parentVersionType, childVersionTypes, valid }) => {
        const context = createBuildContext(
            parentVersionType,
            childVersionTypes,
            'monorepo'
        );
        const result = validateMonorepoVersions(context);
        if (valid) {
            expect(result).toEqualRight(context);
        } else {
            expect(result).toEqualLeft(
                new Error('Invalid version types in monorepo child projects')
            );
        }
    }
);

test('skips validation for polyrepos', () => {
    const context = createBuildContext(
        'Release',
        ['Release', 'PreRelease'],
        'polyrepo'
    );
    const result = validateMonorepoVersions(context);
    expect(result).toEqualRight(context);
});
