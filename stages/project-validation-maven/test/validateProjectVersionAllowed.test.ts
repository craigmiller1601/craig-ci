import { test, vi, MockedFunction, beforeEach, expect } from 'vitest';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { match, P } from 'ts-pattern';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { RepoType } from '@craig-ci/context/types/RepoType';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';
import { validateMavenProjectVersionAllowed } from '../src/validateMavenProjectVersionAllowed';
import { NexusRepoGroupSearchFn } from '@craig-ci/nexus-api';
import { NexusSearchResult } from '@craig-ci/nexus-api/NexusSearchResult';
import { taskEither } from 'fp-ts';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { formatInTimeZone } from 'date-fns-tz';
import { addDays, subDays } from 'date-fns';
import { TIME_ZONE, TIMESTAMP_FORMAT } from '@craig-ci/context/date';

type BuildContextArgs = Readonly<{
    repoType: RepoType;
    parentVersionType: VersionType;
    projectType?: ProjectType;
    childVersionType?: VersionType;
    now?: Date;
}>;

const createBuildContext = (args: BuildContextArgs): BuildContext => {
    const {
        repoType,
        parentVersionType,
        projectType = 'MavenLibrary',
        childVersionType = parentVersionType,
        now = new Date()
    } = args;

    const projectInfo: ProjectInfo = {
        group: 'us.craigmiller160',
        name: 'temp',
        version: '',
        versionType: 'Unknown',
        repoType
    };

    const parentVersion = match({ parentVersionType, repoType, projectType })
        .with(
            {
                parentVersionType: 'Release',
                repoType: 'polyrepo',
                projectType: 'MavenLibrary'
            },
            () => '1.0.0'
        )
        .with(
            {
                parentVersionType: 'Release',
                repoType: 'monorepo',
                projectType: 'MavenLibrary'
            },
            () => '3.0.0'
        )
        .with(
            {
                parentVersionType: 'Release',
                repoType: 'polyrepo',
                projectType: 'MavenApplication'
            },
            () => formatInTimeZone(now, TIME_ZONE, TIMESTAMP_FORMAT)
        )
        .otherwise(() => '1.0.0-SNAPSHOT');

    return {
        projectType,
        projectInfo: {
            ...projectInfo,
            version: parentVersion,
            versionType: parentVersionType,
            monorepoChildren:
                repoType === 'monorepo'
                    ? [
                          {
                              ...projectInfo,
                              version: isRelease(childVersionType)
                                  ? '1.0.0'
                                  : '1.0.0-SNAPSHOT',
                              versionType: childVersionType
                          }
                      ]
                    : undefined
        }
    };
};

type VersionValidationScenario =
    | 'valid-release'
    | 'valid-pre-release'
    | 'release-with-higher-release'
    | 'release-non-unique'
    | 'pre-release-with-higher-release';

const getVersionTypeForScenario = (
    scenario: VersionValidationScenario
): VersionType =>
    match<VersionValidationScenario, VersionType>(scenario)
        .with(
            P.union(
                'valid-release',
                'release-with-higher-release',
                'release-non-unique'
            ),
            () => 'Release'
        )
        .with(
            P.union('valid-pre-release', 'pre-release-with-higher-release'),
            () => 'PreRelease'
        )
        .exhaustive();

const mockSearchForMavenReleases: MockedFunction<NexusRepoGroupSearchFn> =
    vi.fn();

const getNexusResponseForScenario = (
    scenario: VersionValidationScenario
): NexusSearchResult => {
    const version = match<VersionValidationScenario, string>(scenario)
        .with(P.union('valid-release', 'valid-pre-release'), () => '0.0.1')
        .with(
            P.union(
                'pre-release-with-higher-release',
                'release-with-higher-release'
            ),
            () => '2.0.0'
        )
        .with('release-non-unique', () => '1.0.0')
        .exhaustive();
    return {
        items: [
            {
                version,
                id: '',
                repository: '',
                format: '',
                group: '',
                name: '',
                assets: []
            }
        ]
    };
};

const validScenarios: ReadonlyArray<VersionValidationScenario> = [
    'valid-release',
    'valid-pre-release'
];

const invalidVersionScenarios: ReadonlyArray<VersionValidationScenario> = [
    'release-with-higher-release',
    'pre-release-with-higher-release',
    'release-non-unique'
];

beforeEach(() => {
    vi.resetAllMocks;
});

test.each<VersionValidationScenario>([
    'valid-release',
    'valid-pre-release',
    'release-with-higher-release',
    'pre-release-with-higher-release',
    'release-non-unique'
])('validates the artifact version for a polyrepo: %s', async (scenario) => {
    const response = getNexusResponseForScenario(scenario);
    mockSearchForMavenReleases.mockImplementation(() =>
        taskEither.right(response)
    );

    const versionType = getVersionTypeForScenario(scenario);
    const context = createBuildContext({
        repoType: 'polyrepo',
        parentVersionType: versionType
    });
    const result = await validateMavenProjectVersionAllowed(
        context,
        mockSearchForMavenReleases
    )();

    if (validScenarios.includes(scenario)) {
        expect(result).toEqualRight(context);
        return;
    }

    if (invalidVersionScenarios.includes(scenario)) {
        const projectInfo = context.projectInfo;
        expect(result).toEqualLeft(
            new Error(
                `Project version is invalid. Project: ${projectInfo.group}:${projectInfo.name} Version: ${projectInfo.version}`
            )
        );
        return;
    }

    throw new Error(`Scenario without assertion: ${scenario}`);
});

test.each<VersionValidationScenario>([
    'valid-release',
    'valid-pre-release',
    'release-with-higher-release',
    'pre-release-with-higher-release',
    'release-non-unique'
])(
    'validates the artifact version for a child of a monorepo: %s',
    async (scenario) => {
        const response = getNexusResponseForScenario(scenario);
        mockSearchForMavenReleases.mockImplementation(() =>
            taskEither.right(response)
        );

        const versionType = getVersionTypeForScenario(scenario);
        const context = createBuildContext({
            repoType: 'monorepo',
            parentVersionType: 'Release',
            childVersionType: versionType
        });
        const result = await validateMavenProjectVersionAllowed(
            context,
            mockSearchForMavenReleases
        )();

        if (validScenarios.includes(scenario)) {
            expect(result).toEqualRight(context);
            return;
        }

        if (invalidVersionScenarios.includes(scenario)) {
            const projectInfo = context.projectInfo.monorepoChildren?.[0];
            if (!projectInfo) {
                throw new Error(
                    'Should have one monorepo child ProjectInfo for this test'
                );
            }
            expect(result).toEqualLeft(
                new Error(
                    `Project version is invalid. Project: ${projectInfo.group}:${projectInfo.name} Version: ${projectInfo.version}`
                )
            );
            return;
        }

        throw new Error(`Scenario without assertion: ${scenario}`);
    }
);

type ApplicationVersionValidationScenario = Exclude<
    VersionValidationScenario,
    'valid-pre-release' | 'pre-release-with-higher-release'
>;

const getNexusResponseForApplicationScenario = (
    scenario: ApplicationVersionValidationScenario,
    now: Date
): NexusSearchResult => {
    const date = match<ApplicationVersionValidationScenario, Date>(scenario)
        .with('valid-release', () => subDays(now, 1))
        .with('release-non-unique', () => now)
        .with('release-with-higher-release', () => addDays(now, 1))
        .exhaustive();

    const version = formatInTimeZone(date, TIME_ZONE, TIMESTAMP_FORMAT);

    return {
        items: [
            {
                version,
                id: '',
                repository: '',
                format: '',
                group: '',
                name: '',
                assets: []
            }
        ]
    };
};

test.each<ApplicationVersionValidationScenario>([
    'valid-release',
    'release-with-higher-release',
    'release-non-unique'
])(
    'validates the artifact version for an application polyrepo: %s',
    async (scenario) => {
        const now = new Date();
        const response = getNexusResponseForApplicationScenario(scenario, now);
        mockSearchForMavenReleases.mockImplementation(() =>
            taskEither.right(response)
        );

        const context = createBuildContext({
            repoType: 'polyrepo',
            parentVersionType: 'Release',
            projectType: 'MavenApplication',
            now
        });

        const result = await validateMavenProjectVersionAllowed(
            context,
            mockSearchForMavenReleases
        )();

        if (scenario === 'valid-release') {
            expect(result).toEqualRight(context);
            return;
        }

        const projectInfo = context.projectInfo;
        expect(result).toEqualLeft(
            new Error(
                `Project version is invalid. Project: ${projectInfo.group}:${projectInfo.name} Version: ${projectInfo.version}`
            )
        );
    }
);
