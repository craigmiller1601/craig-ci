const { createConfig } = require('@craig-ci/webpack/webpack.config');
const packageJson = require('./package.json');

const fileName = packageJson.name.replace('@craig-ci/', '');

module.exports = createConfig(fileName);
