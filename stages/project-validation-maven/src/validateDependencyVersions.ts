import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { either, function as func, readonlyArray, option } from 'fp-ts';
import { isPreRelease } from '@craig-ci/context/utils/versionTypeUtils';
import { getRawProjectData } from '@craig-ci/maven-support/getRawProjectData';
import {
    MavenArtifact,
    MavenModules,
    PomXml
} from '@craig-ci/maven-support/PomXml';
import path from 'path';
import { match } from 'ts-pattern';

const MAVEN_PROPERTY_REGEX = /\${.*}/;
const PROJECT_PROP_REGEX = /^\${project\..+}$/;
type MavenProperties = { [key: string]: string };
type PomAndProps = [PomXml, MavenProperties];

const getMavenProperties = (pomXml: PomXml): MavenProperties =>
    func.pipe(
        option.fromNullable(pomXml.project.properties),
        option.chain(readonlyArray.head),
        option.map((props) =>
            Object.entries(props).reduce(
                (mvnProps: MavenProperties, [key, value]) => {
                    mvnProps[key] = func.pipe(
                        value,
                        readonlyArray.head,
                        option.getOrElse(() => '')
                    );
                    return mvnProps;
                },
                {}
            )
        ),
        option.getOrElse(() => ({}))
    );

const mavenFormatArtifactVersion = (dependency: MavenArtifact): string =>
    func.pipe(
        option.fromNullable(dependency.version),
        option.chain(readonlyArray.head),
        option.getOrElse(() => '')
    );

const excludeIfUsingProjectProp = (version: string): boolean =>
    !PROJECT_PROP_REGEX.test(version);

const mavenReplaceVersionProperty = (
    mvnProps: MavenProperties,
    version: string
): string =>
    match(version)
        .when(
            (_) => MAVEN_PROPERTY_REGEX.test(_),
            (_) => mvnProps[_.replace(/^\${/, '').replace(/}$/, '')]
        )
        .otherwise(() => version);

const mavenHasNoSnapshotDependencies = ([
    pomXml,
    mvnProps
]: PomAndProps): boolean => {
    if (!pomXml.project.dependencies) {
        return true;
    }
    const hasNoSnapshotDependencies =
        func.pipe(
            pomXml.project.dependencies[0].dependency,
            readonlyArray.map(mavenFormatArtifactVersion),
            readonlyArray.filter(excludeIfUsingProjectProp),
            readonlyArray.filter((version) =>
                mavenReplaceVersionProperty(mvnProps, version).includes(
                    'SNAPSHOT'
                )
            )
        ).length === 0;
    const hasNoSnapshotPlugins =
        func.pipe(
            pomXml.project.build?.[0].plugins?.[0].plugin ?? [],
            readonlyArray.map(mavenFormatArtifactVersion),
            readonlyArray.filter(excludeIfUsingProjectProp),
            readonlyArray.filter((version) =>
                mavenReplaceVersionProperty(mvnProps, version).includes(
                    'SNAPSHOT'
                )
            )
        ).length === 0;

    return hasNoSnapshotDependencies && hasNoSnapshotPlugins;
};

const validateMavenPomXml = (
    context: BuildContext,
    cwd: string
): either.Either<Error, BuildContext> =>
    func.pipe(
        getRawProjectData(cwd),
        either.map(
            (pomXml): PomAndProps => [pomXml, getMavenProperties(pomXml)]
        ),
        either.filterOrElse(
            mavenHasNoSnapshotDependencies,
            () =>
                new Error(
                    'Cannot have SNAPSHOT dependencies or plugins in Maven release'
                )
        ),
        either.map(() => context)
    );

export const validateDependencyVersions = (
    context: BuildContext,
    cwd: string = process.cwd()
): either.Either<Error, BuildContext> => {
    if (isPreRelease(context.projectInfo.versionType)) {
        console.log(
            'Permissive dependency versioning allowed for pre-release projects'
        );
        return either.right(context);
    }

    return func.pipe(
        getRawProjectData(cwd),
        either.flatMap((pomXml) => {
            if (pomXml.project.modules) {
                return func.pipe(
                    pomXml.project.modules,
                    readonlyArray.head,
                    option.getOrElse((): MavenModules => ({ module: [] })),
                    (m) => m.module,
                    readonlyArray.map((module) => path.join(cwd, module)),
                    readonlyArray.map((modulePath) =>
                        validateMavenPomXml(context, modulePath)
                    ),
                    either.sequenceArray,
                    either.map(() => context)
                );
            }
            return validateMavenPomXml(context, cwd);
        })
    );
};
