import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { either } from 'fp-ts';
import { isPreRelease } from '@craig-ci/context/utils/versionTypeUtils';

export const validateMonorepoVersions = (
    context: BuildContext
): either.Either<Error, BuildContext> => {
    if (context.projectInfo.repoType === 'polyrepo') {
        console.log('Skipping monorepo version validation for polyrepo');
        return either.right(context);
    }

    if (isPreRelease(context.projectInfo.versionType)) {
        console.log('Permissive versioning allowed for pre-release monorepos');
        return either.right(context);
    }

    const hasPreReleaseVersion = !!context.projectInfo.monorepoChildren
        ?.map((childInfo) => childInfo.versionType)
        ?.find((versionType) => 'PreRelease' === versionType);

    if (!hasPreReleaseVersion) {
        return either.right(context);
    }
    return either.left(
        new Error('Invalid version types in monorepo child projects')
    );
};
