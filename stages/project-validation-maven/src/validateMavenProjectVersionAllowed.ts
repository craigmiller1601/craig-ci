import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither } from 'fp-ts';
import {
    NexusRepoGroupSearchFn,
    searchForMavenReleases as defaultSearchForMavenReleases
} from '@craig-ci/nexus-api';
import { validateProjectVersionAllowed } from '@craig-ci/project-version-validation';

export const validateMavenProjectVersionAllowed = (
    context: BuildContext,
    searchForMavenReleases: NexusRepoGroupSearchFn = defaultSearchForMavenReleases
): taskEither.TaskEither<Error, BuildContext> =>
    validateProjectVersionAllowed(context, searchForMavenReleases);
