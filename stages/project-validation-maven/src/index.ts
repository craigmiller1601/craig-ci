import { readContext } from '@craig-ci/context/io';
import { either, function as func, taskEither } from 'fp-ts';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { validateMonorepoVersions } from './validateMonorepoVersions';
import { validateDependencyVersions } from './validateDependencyVersions';
import { validateMavenProjectVersionAllowed } from './validateMavenProjectVersionAllowed';

console.log('Performing project validations');

void func.pipe(
    readContext(),
    either.chain(validateMonorepoVersions),
    either.chain(validateDependencyVersions),
    taskEither.fromEither,
    taskEither.chain(validateMavenProjectVersionAllowed),
    terminateTaskEither
)();
