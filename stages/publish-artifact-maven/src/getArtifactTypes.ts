import { match, P } from 'ts-pattern';
import { either, function as func } from 'fp-ts';
import { ArtifactType, getArtifactsDir } from './common';
import { readdirSync } from '@craig-ci/fp-io';

const SOURCES_JAR_REGEX = /^.+-sources\.jar$/;
const JAR_REGEX = /^(?!.*-(sources|javadoc)\.jar$).*\.jar$/;
const POM_REGEX = /^.*\.pom$/;
const SIGNATURE_REGEX = /^.*\.asc$/;
const JAVADOC_JAR_REGEX = /^.+-javadoc\.jar$/;

const identifyArtifactTypes = (
    files: ReadonlyArray<string>
): ReadonlyArray<ArtifactType> => {
    const artifactTypesArray = files
        .map((file) =>
            match<string, ArtifactType | undefined>(file)
                .with(P.string.regex(SOURCES_JAR_REGEX), () => 'source')
                .with(P.string.regex(JAVADOC_JAR_REGEX), () => 'javadoc')
                .with(P.string.regex(POM_REGEX), () => 'pom')
                .with(P.string.regex(SIGNATURE_REGEX), () => 'signature')
                .with(P.string.regex(JAR_REGEX), () => 'code')
                .otherwise(() => undefined)
        )
        .filter((type): type is ArtifactType => !!type);
    return [...new Set(artifactTypesArray)];
};

const validateArtifactsPresent = (
    types: ReadonlyArray<ArtifactType>
): either.Either<Error, ReadonlyArray<ArtifactType>> => {
    if (types.length === 0) {
        return either.left(new Error('No artifacts are present'));
    }
    return either.right(types);
};

export const getArtifactTypes = (
    cwd: string = process.cwd()
): either.Either<Error, ReadonlyArray<ArtifactType>> => {
    const artifactsDir = getArtifactsDir(cwd);
    return func.pipe(
        readdirSync(artifactsDir),
        either.map(identifyArtifactTypes),
        either.chain(validateArtifactsPresent)
    );
};
