import { either, taskEither, function as func } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { ArtifactType, PUBLISH_REPO_IDS, PublishRepoId } from './common';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { match, P } from 'ts-pattern';
import { VersionType } from '@craig-ci/context/types/VersionType';

export const CENTRAL_RELEASE_URL =
    'https://s01.oss.sonatype.org/service/local/staging/deploy/maven2';
export const CENTRAL_SNAPSHOT_URL =
    'https://s01.oss.sonatype.org/content/repositories/snapshots';
export const NEXUS_RELEASE_URL =
    'http://nexus-standard.nexus/repository/maven-releases';
export const NEXUS_SNAPSHOT_URL =
    'http://nexus-standard.nexus/repository/maven-snapshots';

type ArtifactTypeWithoutSignature = Exclude<ArtifactType, 'signature'>;

type PublishCommandArgs = Readonly<{
    projectInfo: ProjectInfo;
    cwd: string;
    env: NodeJS.ProcessEnv;
    artifactTypes: ReadonlyArray<ArtifactType>;
}>;

type Dependencies = Readonly<{
    cwd: string;
    env: NodeJS.ProcessEnv;
    runCommand: RunCommandType;
}>;

const createSignatures = (
    projectInfo: ProjectInfo,
    artifactsDir: string,
    artifactTypesWithoutSignatures: ReadonlyArray<ArtifactTypeWithoutSignature>
): string => {
    const { name, version } = projectInfo;
    const codeSignatureFileName = `${name}-${version}.jar.asc`;
    const sourceSignatureFileName = `${name}-${version}-sources.jar.asc`;
    const javadocSignatureFileName = `${name}-${version}-javadoc.jar.asc`;
    const pomSignatureFileName = `${name}-${version}.pom.asc`;

    const signatures = artifactTypesWithoutSignatures
        .map((type) =>
            match<ArtifactTypeWithoutSignature, string>(type)
                .with('code', () => `${artifactsDir}/${codeSignatureFileName}`)
                .with('pom', () => `${artifactsDir}/${pomSignatureFileName}`)
                .with(
                    'source',
                    () => `${artifactsDir}/${sourceSignatureFileName}`
                )
                .with(
                    'javadoc',
                    () => `${artifactsDir}/${javadocSignatureFileName}`
                )
                .exhaustive()
        )
        .join(',');
    if (signatures) {
        return `-Dfiles=${signatures}`;
    }
    return '';
};

const createSignatureTypes = (
    artifactTypesWithoutSignatures: ReadonlyArray<ArtifactTypeWithoutSignature>
): string => {
    const types = artifactTypesWithoutSignatures
        .map((type) =>
            match<ArtifactTypeWithoutSignature, string>(type)
                .with('code', () => 'jar.asc')
                .with('source', () => 'sources.jar.asc')
                .with('javadoc', () => 'javadoc.jar.asc')
                .with('pom', () => 'pom.asc')
                .exhaustive()
        )
        .join(',');
    if (types) {
        return `-Dtypes=${types}`;
    }
    return '';
};

const createSignatureClassifiers = (
    artifactTypesWithoutSignatures: ReadonlyArray<ArtifactTypeWithoutSignature>
): string => {
    const classifiers = artifactTypesWithoutSignatures
        .map(() => 'signature')
        .join(',');
    if (classifiers) {
        return `-Dclassifiers=${classifiers}`;
    }
    return '';
};

const createRepository = (
    env: NodeJS.ProcessEnv,
    versionType: VersionType
): either.Either<Error, string> => {
    const publishRepoId: PublishRepoId = match({ env, versionType })
        .with(
            {
                env: { OVERRIDE_RELEASE_REPO: P.not(P.nullish) },
                versionType: 'Release'
            },
            ({ env: innerEnv }) =>
                innerEnv.OVERRIDE_RELEASE_REPO as PublishRepoId
        )
        .with(
            {
                env: { OVERRIDE_SNAPSHOT_REPO: P.not(P.nullish) },
                versionType: 'PreRelease'
            },
            ({ env: innerEnv }) =>
                innerEnv.OVERRIDE_SNAPSHOT_REPO as PublishRepoId
        )
        .otherwise(() => 'nexus');

    if (!PUBLISH_REPO_IDS.includes(publishRepoId)) {
        return either.left(
            new Error(`Invalid publish repository id: ${publishRepoId}`)
        );
    }

    const url = match({ publishRepoId, versionType })
        .with(
            { publishRepoId: 'nexus', versionType: 'Release' },
            () => NEXUS_RELEASE_URL
        )
        .with(
            { publishRepoId: 'nexus', versionType: 'PreRelease' },
            () => NEXUS_SNAPSHOT_URL
        )
        .with(
            { publishRepoId: 'central', versionType: 'Release' },
            () => CENTRAL_RELEASE_URL
        )
        .with(
            { publishRepoId: 'central', versionType: 'PreRelease' },
            () => CENTRAL_SNAPSHOT_URL
        )
        .run();

    return either.right(`-DrepositoryId=${publishRepoId} -Durl=${url}`);
};

export const createPublishCommand = (
    args: PublishCommandArgs
): either.Either<Error, string> => {
    const { group, name, version } = args.projectInfo;
    const codeFileName = `${name}-${version}.jar`;
    const sourceFileName = `${name}-${version}-sources.jar`;
    const javadocFileName = `${name}-${version}-javadoc.jar`;
    const pomFileName = './pom.xml';
    const artifactsDir = `${args.cwd}/artifacts`;

    const projectInfo = `-DgroupId=${group} -DartifactId=${name} -Dversion=${version}`;

    const artifactTypesWithoutSignatures: ReadonlyArray<ArtifactTypeWithoutSignature> =
        args.artifactTypes.filter(
            (type): type is ArtifactTypeWithoutSignature => type !== 'signature'
        );

    const coreFiles = artifactTypesWithoutSignatures
        .map((type) =>
            match<ArtifactTypeWithoutSignature, string>(type)
                .with('code', () => `-Dfile=${artifactsDir}/${codeFileName}`)
                .with('pom', () => `-DpomFile=${artifactsDir}/${pomFileName}`)
                .with(
                    'source',
                    () => `-Dsources=${artifactsDir}/${sourceFileName}`
                )
                .with(
                    'javadoc',
                    () => `-Djavadoc=${artifactsDir}/${javadocFileName}`
                )
                .exhaustive()
        )
        .toSorted((a, b) => a.localeCompare(b))
        .join(' ');
    const hasSignatures = !!args.artifactTypes.find(
        (type) => type === 'signature'
    );
    const signatures = hasSignatures
        ? createSignatures(
              args.projectInfo,
              artifactsDir,
              artifactTypesWithoutSignatures
          )
        : '';
    const types = hasSignatures
        ? createSignatureTypes(artifactTypesWithoutSignatures)
        : '';
    const classifiers = hasSignatures
        ? createSignatureClassifiers(artifactTypesWithoutSignatures)
        : '';

    const settingsFile = `-s \${MAVEN_HOME_OVERRIDE}/settings.xml`;

    return func.pipe(
        createRepository(args.env, args.projectInfo.versionType),
        either.map(
            (repo) =>
                `mvn deploy:deploy-file ${settingsFile} -Dpackaging=jar ${projectInfo} ${repo} ${coreFiles} ${signatures} ${types} ${classifiers}`
        )
    );
};

export const publishArtifact = (
    context: BuildContext,
    types: ReadonlyArray<ArtifactType>,
    dependencies: Dependencies = {
        cwd: process.cwd(),
        env: process.env,
        runCommand
    }
): taskEither.TaskEither<Error, unknown> => {
    console.log(`Publishing artifact`);
    return func.pipe(
        createPublishCommand({
            artifactTypes: types,
            cwd: dependencies.cwd,
            env: dependencies.env,
            projectInfo: context.projectInfo
        }),
        taskEither.fromEither,
        taskEither.chain((command) => dependencies.runCommand(command))
    );
};
