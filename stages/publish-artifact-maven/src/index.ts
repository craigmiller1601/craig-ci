import { readContext } from '@craig-ci/context/io';
import { function as func, taskEither, either } from 'fp-ts';
import { publishArtifact } from './publishArtifact';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { getArtifactTypes } from './getArtifactTypes';

console.log('Publishing maven artifact');

void func.pipe(
    readContext(),
    either.bindTo('context'),
    either.bind('artifactType', () => getArtifactTypes()),
    taskEither.fromEither,
    taskEither.chain(({ context, artifactType }) =>
        publishArtifact(context, artifactType)
    ),
    terminateTaskEither
)();
