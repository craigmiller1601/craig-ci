import path from 'path';

export const getArtifactsDir = (cwd: string): string =>
    path.join(cwd, 'artifacts');

export type ArtifactType = 'code' | 'source' | 'javadoc' | 'signature' | 'pom';

export const PUBLISH_REPO_IDS = ['nexus', 'central'] as const;
export type PublishRepoId = (typeof PUBLISH_REPO_IDS)[number];
