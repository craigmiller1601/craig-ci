import { test, expect } from 'vitest';
import { ArtifactType } from '../src/common';
import path from 'path';
import { getArtifactTypes } from '../src/getArtifactTypes';
import { either } from 'fp-ts';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'getMavenArtifactType'
);

type Scenario = Readonly<{
    artifactTypes: 'invalid' | ReadonlyArray<ArtifactType>;
}>;

const getWorkingDir = (artifactTypes: Scenario['artifactTypes']): string => {
    if (artifactTypes === 'invalid') {
        return path.join(WORKING_DIR_ROOT, artifactTypes);
    }

    return path.join(WORKING_DIR_ROOT, artifactTypes.join('_'));
};

test.each<Scenario>([
    { artifactTypes: 'invalid' },
    { artifactTypes: ['code'] },
    { artifactTypes: ['code', 'source'] },
    { artifactTypes: ['code', 'pom'] },
    { artifactTypes: ['code', 'javadoc'] },
    { artifactTypes: ['code', 'signature'] },
    { artifactTypes: ['code', 'source', 'javadoc', 'signature', 'pom'] }
])('gets the type of the artifact for $artifactTypes', ({ artifactTypes }) => {
    const workingDir = getWorkingDir(artifactTypes);
    const result = getArtifactTypes(workingDir);
    if (artifactTypes === 'invalid') {
        expect(result).toEqualLeft(new Error('No artifacts are present'));
    } else {
        expect(result).toBeRight();
        const actual = (
            result as either.Right<ReadonlyArray<ArtifactType>>
        ).right.toSorted((a, b) => a.localeCompare(b));
        const expected = artifactTypes.toSorted((a, b) => a.localeCompare(b));
        expect(actual).toEqual(expected);
    }
});
