import { beforeEach, expect, MockedFunction, test, vi } from 'vitest';
import { ArtifactType, PublishRepoId } from '../src/common';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import {
    CENTRAL_RELEASE_URL,
    CENTRAL_SNAPSHOT_URL,
    createPublishCommand,
    NEXUS_RELEASE_URL,
    NEXUS_SNAPSHOT_URL,
    publishArtifact
} from '../src/publishArtifact';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither, either } from 'fp-ts';
import { match } from 'ts-pattern';

const REPO_REGEX = /-DrepositoryId=(?<repoId>.+?) -Durl=(?<url>.+?) /;
type RepoUrlRegexGroups = Readonly<{
    repoId: PublishRepoId;
    url: string;
}>;

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

const createContext = (versionType: VersionType): BuildContext => ({
    projectType: 'MavenLibrary',
    projectInfo: {
        group: 'us.craigmiller160',
        name: 'temp',
        version: isRelease(versionType) ? '1.0.0' : '1.0.0-SNAPSHOT',
        versionType,
        repoType: 'polyrepo'
    }
});

beforeEach(() => {
    vi.resetAllMocks;
});

type VersionTypeScenario = Readonly<{
    versionType: VersionType;
    repoId?: PublishRepoId;
}>;

const getEnv = (repoType?: PublishRepoId): NodeJS.ProcessEnv =>
    match(repoType)
        .with(undefined, () => ({}))
        .with('nexus', () => ({
            OVERRIDE_RELEASE_REPO: 'nexus',
            OVERRIDE_SNAPSHOT_REPO: 'nexus'
        }))
        .with('central', () => ({
            OVERRIDE_RELEASE_REPO: 'central',
            OVERRIDE_SNAPSHOT_REPO: 'central'
        }))
        .exhaustive();

test.each<VersionTypeScenario>([
    { versionType: 'Release', repoId: 'nexus' },
    { versionType: 'Release' },
    { versionType: 'Release', repoId: 'central' },
    { versionType: 'PreRelease', repoId: 'nexus' },
    { versionType: 'PreRelease' },
    { versionType: 'PreRelease', repoId: 'central' }
])(
    'Publishes for version type $versionType and repo type $repoId',
    async ({ versionType, repoId }) => {
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const context = createContext(versionType);
        const env: NodeJS.ProcessEnv = getEnv(repoId);
        const result = await publishArtifact(context, ['code'], {
            cwd: process.cwd(),
            env,
            runCommand: runCommandMock
        })();
        expect(result).toBeRight();

        const command = createPublishCommand({
            projectInfo: context.projectInfo,
            cwd: process.cwd(),
            env,
            artifactTypes: ['code']
        });
        expect(command).toBeRight();
        const commandValue = (command as either.Right<string>).right;
        console.log(`Expected Command: ${commandValue}`);
        expect(runCommandMock).toHaveBeenCalledWith(commandValue);

        const groups = REPO_REGEX.exec(commandValue)?.groups as
            | RepoUrlRegexGroups
            | undefined;
        expect(groups).not.toBeUndefined();
        if (!groups) {
            throw new Error();
        }
        const { repoId: actualRepoId, url } = groups;

        const expectedRepoId: PublishRepoId =
            repoId === undefined || repoId === 'nexus' ? 'nexus' : 'central';

        expect(actualRepoId).toEqual(expectedRepoId);

        if (versionType === 'Release' && expectedRepoId === 'nexus') {
            expect(url).toEqual(NEXUS_RELEASE_URL);
        } else if (versionType === 'Release' && expectedRepoId === 'central') {
            expect(url).toEqual(CENTRAL_RELEASE_URL);
        } else if (versionType === 'PreRelease' && expectedRepoId === 'nexus') {
            expect(url).toEqual(NEXUS_SNAPSHOT_URL);
        } else if (
            versionType === 'PreRelease' &&
            expectedRepoId === 'central'
        ) {
            expect(url).toEqual(CENTRAL_SNAPSHOT_URL);
        } else {
            throw new Error('Unexpected combination of arguments');
        }
    }
);

type ArtifactTypeScenario = Readonly<{
    artifactTypes: ReadonlyArray<ArtifactType>;
}>;

test.each<ArtifactTypeScenario>([
    { artifactTypes: ['code'] },
    { artifactTypes: ['code', 'source'] },
    { artifactTypes: ['code', 'javadoc'] },
    { artifactTypes: ['code', 'pom'] },
    { artifactTypes: ['code', 'signature'] },
    { artifactTypes: ['code', 'source', 'javadoc', 'signature', 'pom'] }
])(
    'Publishes for Release with artifact types $artifactTypes',
    async ({ artifactTypes }) => {
        const env: NodeJS.ProcessEnv = {};
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const context = createContext('Release');
        const result = await publishArtifact(context, artifactTypes, {
            cwd: process.cwd(),
            env,
            runCommand: runCommandMock
        })();
        expect(result).toBeRight();

        const command = createPublishCommand({
            projectInfo: context.projectInfo,
            env,
            cwd: process.cwd(),
            artifactTypes
        });
        expect(command).toBeRight();
        const commandValue = (command as either.Right<string>).right;
        console.log(`Expected Command: ${commandValue}`);
        expect(runCommandMock).toHaveBeenCalledWith(commandValue);

        expect(commandValue).toMatch(/-Dfile=.+\.jar/);
        if (artifactTypes.includes('signature')) {
            expect(commandValue).toMatch(
                /-Dfiles=.+(?<!sources|javadoc)\.jar\.asc/
            );
        }
        if (artifactTypes.includes('source')) {
            expect(commandValue).toMatch(/-Dsources=.+-sources\.jar/);
            if (artifactTypes.includes('signature')) {
                expect(commandValue).toMatch(/-Dfiles=.+-sources\.jar\.asc/);
            }
        }
        if (artifactTypes.includes('javadoc')) {
            expect(commandValue).toMatch(/-Djavadoc=.+-javadoc\.jar/);
            if (artifactTypes.includes('signature')) {
                expect(commandValue).toMatch(/-Dfiles=.+-javadoc\.jar\.asc/);
            }
        }
        if (artifactTypes.includes('pom')) {
            expect(commandValue).toMatch(/-DpomFile=.+pom\.xml/);
            if (artifactTypes.includes('signature')) {
                expect(commandValue).toMatch(/-Dfiles=.+\.pom\.asc/);
            }
        }
    }
);
