import { test, expect, beforeEach, vi, MockedFunction } from 'vitest';
import path from 'path';
import { match, P } from 'ts-pattern';
import { RunCommandType } from '@craig-ci/context/cmd';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { validateDependencyVersions } from '../src/validateDependencyVersions';
import { applicationVersionTimestamp } from '@craig-ci/context/date';
import { taskEither } from 'fp-ts';
import fs from 'fs';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

type DependencyValidationScenario = 'all valid' | 'invalid dependencies';
type GradleDependencyValidationScenario =
    | DependencyValidationScenario
    | 'invalid plugins';

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

beforeEach(() => {
    vi.resetAllMocks();
});

const gradleOutput = path.join(process.cwd(), 'test', '__gradle-output__');
const buildEnvironment = fs.readFileSync(
    path.join(gradleOutput, 'buildEnvironment.txt'),
    'utf8'
);
const buildEnvironmentWithSnapshot = fs.readFileSync(
    path.join(gradleOutput, 'buildEnvironment_withSnapshot.txt'),
    'utf8'
);
const dependencies = fs.readFileSync(
    path.join(gradleOutput, 'dependencies.txt'),
    'utf8'
);
const dependenciesWithSnapshot = fs.readFileSync(
    path.join(gradleOutput, 'dependencies_withSnapshot.txt'),
    'utf8'
);

const gradleRunCommandMockImpl = (
    command: string,
    scenario: GradleDependencyValidationScenario
): taskEither.TaskEither<Error, string> =>
    match({ command, scenario })
        .with(
            {
                command: 'gradle dependencies',
                scenario: P.union('all valid', 'invalid plugins')
            },
            () => taskEither.right(dependencies)
        )
        .with(
            {
                command: 'gradle buildEnvironment',
                scenario: P.union('all valid', 'invalid dependencies')
            },
            () => taskEither.right(buildEnvironment)
        )
        .with(
            {
                command: 'gradle dependencies',
                scenario: 'invalid dependencies'
            },
            () => taskEither.right(dependenciesWithSnapshot)
        )
        .with(
            { command: 'gradle buildEnvironment', scenario: 'invalid plugins' },
            () => taskEither.right(buildEnvironmentWithSnapshot)
        )
        .otherwise(() =>
            taskEither.left(
                new Error(`Invalid command: '${command}' Scenario: ${scenario}`)
            )
        );

test.each<GradleDependencyValidationScenario>([
    'all valid',
    'invalid dependencies',
    'invalid plugins'
])(
    'validating dependencies for gradle release. Scenario: %s',
    async (scenario) => {
        const workingDir = match(scenario)
            .with('all valid', () => 'gradleKotlinReleaseApplication')
            .with(
                'invalid dependencies',
                () => 'gradleKotlinReleaseApplicationBadDependency'
            )
            .with(
                'invalid plugins',
                () => 'gradleKotlinReleaseApplicationBadPlugin'
            )
            .exhaustive();
        const fullWorkingDir = path.join(WORKING_DIR_ROOT, workingDir);

        runCommandMock.mockImplementation((command: string) =>
            gradleRunCommandMockImpl(command, scenario)
        );

        const buildContext: BuildContext = {
            projectType: 'GradleApplication',
            projectInfo: {
                group: 'io.craigmiller160',
                name: 'name',
                version: applicationVersionTimestamp(),
                versionType: 'Release',
                repoType: 'polyrepo'
            }
        };
        const result = await validateDependencyVersions(buildContext, {
            runCommand: runCommandMock,
            cwd: fullWorkingDir
        })();
        if (scenario === 'all valid') {
            expect(result).toBeRight();
        } else {
            expect(result).toEqualLeft(
                new Error(
                    'Cannot have SNAPSHOT dependencies or plugins in Gradle release'
                )
            );
        }
    }
);

test.each<GradleDependencyValidationScenario>([
    'all valid',
    'invalid dependencies',
    'invalid plugins'
])(
    'validating dependencies for gradle pre-release. Scenario: %s',
    async (scenario) => {
        const workingDir = match(scenario)
            .with('all valid', () => 'gradleKotlinReleaseApplication')
            .with(
                'invalid dependencies',
                () => 'gradleKotlinReleaseApplicationBadDependency'
            )
            .with(
                'invalid plugins',
                () => 'gradleKotlinReleaseApplicationBadPlugin'
            )
            .exhaustive();
        const fullWorkingDir = path.join(WORKING_DIR_ROOT, workingDir);

        runCommandMock.mockImplementation((command: string) =>
            gradleRunCommandMockImpl(command, scenario)
        );

        const buildContext: BuildContext = {
            projectType: 'GradleApplication',
            projectInfo: {
                group: 'io.craigmiller160',
                name: 'name',
                version: '1.0.0-SNAPSHOT',
                versionType: 'PreRelease',
                repoType: 'polyrepo'
            }
        };
        const result = await validateDependencyVersions(buildContext, {
            runCommand: runCommandMock,
            cwd: fullWorkingDir
        })();
        expect(result).toBeRight();
    }
);
