import { BuildContext } from '@craig-ci/context/types/BuildContext';
import {
    taskEither,
    function as func,
    readonlyArray,
    string,
    predicate,
    readonlyNonEmptyArray
} from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import { isPreRelease } from '@craig-ci/context/utils/versionTypeUtils';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    cwd: string;
}>;

const extractGradleVersions = (output: string): ReadonlyArray<string> =>
    func.pipe(
        output,
        string.split('\n'),
        readonlyArray.filter((_) => /^.*?--- /.test(_)),
        readonlyArray.map(string.trim),
        readonlyArray.map(string.replace(/^.*?---/, '')),
        readonlyArray.map(string.replace(/\([*|c]\)$/, '')),
        readonlyArray.map(string.replace(/ -> /, ':')),
        readonlyArray.filter(predicate.not(string.isEmpty)),
        readonlyArray.map(
            func.flow(string.split(':'), readonlyNonEmptyArray.last)
        )
    );

const hasSnapshotVersion = (output: string): boolean =>
    func.pipe(
        extractGradleVersions(output),
        readonlyArray.filter((version) => version.endsWith('SNAPSHOT'))
    ).length === 0;

export const validateDependencyVersions = (
    context: BuildContext,
    dependencies: Dependencies = {
        runCommand,
        cwd: process.cwd()
    }
): taskEither.TaskEither<Error, void> => {
    if (isPreRelease(context.projectInfo.versionType)) {
        return taskEither.right(func.constVoid());
    }

    const hasSnapshotDependency = func.pipe(
        dependencies.runCommand('gradle dependencies', {
            cwd: dependencies.cwd,
            printOutput: false
        }),
        taskEither.map(hasSnapshotVersion)
    );

    const hasSnapshotPlugin = func.pipe(
        dependencies.runCommand('gradle buildEnvironment', {
            cwd: dependencies.cwd,
            printOutput: false
        }),
        taskEither.map(hasSnapshotVersion)
    );

    return func.pipe(
        taskEither.sequenceArray([hasSnapshotDependency, hasSnapshotPlugin]),
        taskEither.filterOrElse(
            ([dependencyResult, pluginResult]) =>
                dependencyResult && pluginResult,
            () =>
                new Error(
                    'Cannot have SNAPSHOT dependencies or plugins in Gradle release'
                )
        ),
        taskEither.map(() => func.constVoid())
    );
};
