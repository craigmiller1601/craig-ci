import { function as func, taskEither } from 'fp-ts';
import { validateDependencyVersions } from './validateDependencyVersions';
import { readContext } from '@craig-ci/context/io';
import { validateGradleProjectVersionAllowed } from './validateGradleProjectVersionAllowed';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Validating gradle project');

void func.pipe(
    readContext(),
    taskEither.fromEither,
    taskEither.chainFirst(validateDependencyVersions),
    taskEither.chain(validateGradleProjectVersionAllowed),
    terminateTaskEither
)();
