import { either, function as func } from 'fp-ts';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { match } from 'ts-pattern';
import {
    applicationVersionTimestamp as defaultAppVersionTimestamp,
    AppVersionTimestamp
} from '@craig-ci/context/date';
import {
    Chart,
    ChartType,
    readHelmChart,
    helmChartTypes
} from '@craig-ci/helm-support';

type ProcessEnv = typeof process.env;

const getProjectType = (type: string): ProjectType =>
    match<string, ProjectType>(type)
        .with('library', () => 'HelmLibrary')
        .with('application', () => 'HelmApplication')
        .run();

const getVersion = (
    chart: Chart,
    projectType: ProjectType,
    applicationVersionTimestamp: AppVersionTimestamp
): string =>
    match<ProjectType, string>(projectType)
        .with('HelmLibrary', () => chart.version)
        .with('HelmApplication', () => applicationVersionTimestamp())
        .run();

const constructBuildContext = (
    chart: Chart,
    env: ProcessEnv,
    applicationVersionTimestamp: AppVersionTimestamp
): either.Either<Error, BuildContext> => {
    if (
        !env.HELM_PROJECT_TYPE ||
        !helmChartTypes.includes(env.HELM_PROJECT_TYPE as ChartType)
    ) {
        return either.left(new Error('Missing or invalid HELM_PROJECT_TYPE'));
    }

    const projectType = getProjectType(env.HELM_PROJECT_TYPE);

    return either.right({
        projectType,
        projectInfo: {
            repoType: 'polyrepo',
            group: 'craigmiller160',
            name: chart.name,
            versionType: 'Release',
            version: getVersion(chart, projectType, applicationVersionTimestamp)
        }
    });
};

export const getHelmBuildContext = (
    cwd: string = process.cwd(),
    env: ProcessEnv = process.env,
    applicationVersionTimestamp: AppVersionTimestamp = defaultAppVersionTimestamp
): either.Either<Error, BuildContext> => {
    return func.pipe(
        readHelmChart(cwd),
        either.chain((chart) =>
            constructBuildContext(chart, env, applicationVersionTimestamp)
        )
    );
};
