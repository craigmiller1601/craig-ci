import { either, function as func } from 'fp-ts';
import { writeContext } from '@craig-ci/context/io';
import { terminateEither } from '@craig-ci/context/terminate';
import { getHelmBuildContext } from './getHelmBuildContext';

console.log('Getting project info for helm project');

func.pipe(getHelmBuildContext(), either.chain(writeContext), terminateEither);
