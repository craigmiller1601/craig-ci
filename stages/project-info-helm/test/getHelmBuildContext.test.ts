import { test, expect } from 'vitest';
import path from 'path';
import { match } from 'ts-pattern';
import { getHelmBuildContext } from '../src/getHelmBuildContext';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { applicationVersionTimestamp } from '@craig-ci/context/date';
import { ChartType } from '@craig-ci/helm-support';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

type ProcessEnv = typeof process.env;

const getWorkingDirName = (type?: ChartType): string =>
    match<ChartType | undefined, string>(type)
        .with('library', () => 'helmReleaseLibrary')
        .with('application', () => 'helmReleaseApplication')
        .with(undefined, () => 'helmReleaseLibrary')
        .exhaustive();

test.each<ChartType | undefined>(['library', 'application', undefined])(
    'Creates build context for helm for scenario %s',
    (chartType) => {
        const workingDirName = getWorkingDirName(chartType);
        const workingDir = path.join(WORKING_DIR_ROOT, workingDirName);
        const env: ProcessEnv = {
            HELM_PROJECT_TYPE: chartType
        };
        const timestamp = applicationVersionTimestamp();

        const result = getHelmBuildContext(workingDir, env, () => timestamp);

        const expectedBuildContext: BuildContext = {
            projectType:
                chartType === 'application' ? 'HelmApplication' : 'HelmLibrary',
            projectInfo: {
                group: 'craigmiller160',
                name: chartType === 'application' ? 'my_app' : 'my_lib',
                version: chartType === 'application' ? timestamp : '1.0.0',
                versionType: 'Release',
                repoType: 'polyrepo'
            }
        };

        if (!chartType) {
            expect(result).toEqualLeft(
                new Error('Missing or invalid HELM_PROJECT_TYPE')
            );
            return;
        }

        expect(result).toEqualRight(expectedBuildContext);
    }
);

test('Cannot find helm chart file', () => {
    const expectedPath = path.join(
        process.cwd(),
        'deploy',
        'chart',
        'Chart.yaml'
    );
    const result = getHelmBuildContext();
    expect(result).toEqualLeft(new Error(`Cannot find ${expectedPath}`));
});
