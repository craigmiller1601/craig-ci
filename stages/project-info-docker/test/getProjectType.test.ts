import { test, expect } from 'vitest';
import path from 'path';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { match } from 'ts-pattern';
import { getProjectType } from '../src/getProjectType';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

const getWorkingDir = (projectType: ProjectType): string => {
    const dirName = match<ProjectType, string>(projectType)
        .with('DockerImage', () => 'dockerReleaseImage')
        .with('DockerApplication', () => 'dockerReleaseApplication')
        .otherwise(() => '.');
    return path.join(WORKING_DIR_ROOT, dirName);
};

test.each<ProjectType>(['DockerImage', 'DockerApplication', 'Unknown'])(
    'identifies the project type for %s',
    (projectType) => {
        const workingDir = getWorkingDir(projectType);

        const result = getProjectType(workingDir);
        if (projectType === 'Unknown') {
            expect(result).toEqualLeft(new Error('Unknown project type'));
        } else {
            expect(result).toEqualRight(projectType);
        }
    }
);
