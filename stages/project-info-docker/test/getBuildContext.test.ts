import { test, expect } from 'vitest';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';
import path from 'path';
import { match } from 'ts-pattern';
import { getBuildContext } from '../src/getBuildContext';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { isApplication } from '@craig-ci/context/utils/projectTypeUtils';
import { applicationVersionTimestamp } from '@craig-ci/context/date';

const getVersion = (
    versionType: VersionType,
    projectType: ProjectType,
    timestamp: string
) => {
    if (isApplication(projectType)) {
        return timestamp;
    }

    if (isRelease(versionType)) {
        return '1.0.0';
    }

    return '1.0.0-beta';
};

const createBuildContext = (
    versionType: VersionType,
    projectType: ProjectType,
    timestamp: string
): BuildContext => ({
    projectType,
    projectInfo: {
        name: isApplication(projectType) ? 'my-app' : 'my-image',
        version: getVersion(versionType, projectType, timestamp),
        versionType,
        repoType: 'polyrepo',
        group: 'craigmiller160'
    }
});

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

const getWorkingDir = (
    versionType: VersionType,
    projectType: ProjectType
): string => {
    const dirName = match<Args, string>({ versionType, projectType })
        .with(
            { versionType: 'Release', projectType: 'DockerImage' },
            () => 'dockerReleaseImage'
        )
        .with(
            { versionType: 'PreRelease', projectType: 'DockerImage' },
            () => 'dockerPreReleaseImage'
        )
        .with(
            {
                versionType: 'Release',
                projectType: 'DockerApplication'
            },
            () => 'dockerReleaseApplication'
        )
        .run();
    return path.join(WORKING_DIR_ROOT, dirName);
};

type Args = Readonly<{
    versionType: VersionType;
    projectType: ProjectType;
}>;

test.each<Args>([
    { versionType: 'Release', projectType: 'DockerImage' },
    { versionType: 'PreRelease', projectType: 'DockerImage' },
    { versionType: 'Release', projectType: 'DockerApplication' }
])(
    'gets project info for version type $versionType and project type $projectType',
    ({ versionType, projectType }) => {
        const timestamp = applicationVersionTimestamp();
        const expectedContext = createBuildContext(
            versionType,
            projectType,
            timestamp
        );
        const workingDir = getWorkingDir(versionType, projectType);

        const result = getBuildContext(
            projectType,
            workingDir,
            () => timestamp
        );
        expect(result).toEqualRight(expectedContext);
    }
);
