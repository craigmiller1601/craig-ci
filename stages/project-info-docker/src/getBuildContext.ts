import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { either, function as func } from 'fp-ts';
import path from 'path';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { DOCKER_PROJECT_FILE } from './constants';
import {
    AppVersionTimestamp,
    applicationVersionTimestamp as defaultAppVersionTimestamp
} from '@craig-ci/context/date';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { isApplication } from '@craig-ci/context/utils/projectTypeUtils';
import { parseJson, readFileSync } from '@craig-ci/fp-io';

const BETA_VERSION_REGEX = /^.*-beta/;

type DockerJson = Readonly<{
    name: string;
    version: string;
}>;

const readDockerConfig = (cwd: string): either.Either<Error, DockerJson> => {
    const dockerJson = path.join(cwd, DOCKER_PROJECT_FILE);
    return func.pipe(
        readFileSync(dockerJson),
        either.chain((json) => parseJson<DockerJson>(json))
    );
};

export const separateGroupAndName = (fullName: string): [string, string] => {
    const nameParts = fullName.split('/');
    const group = nameParts.length === 2 ? nameParts[0].replace(/^@/, '') : '';
    const name = nameParts.length === 2 ? nameParts[1] : nameParts[0];
    return [group, name];
};

const getVersionType = (
    projectType: ProjectType,
    version: string
): VersionType => {
    if (isApplication(projectType)) {
        return 'Release';
    }

    return BETA_VERSION_REGEX.test(version) ? 'PreRelease' : 'Release';
};

const getVersion = (
    projectType: ProjectType,
    version: string,
    applicationVersionTimestamp: AppVersionTimestamp
): string => {
    if (isApplication(projectType)) {
        return applicationVersionTimestamp();
    }

    return version;
};

const createBuildContext = (
    projectType: ProjectType,
    dockerConfig: DockerJson,
    applicationVersionTimestamp: AppVersionTimestamp
): BuildContext => {
    const [group, name] = separateGroupAndName(dockerConfig.name);

    return {
        projectType,
        projectInfo: {
            group,
            name,
            version: getVersion(
                projectType,
                dockerConfig.version,
                applicationVersionTimestamp
            ),
            versionType: getVersionType(projectType, dockerConfig.version),
            repoType: 'polyrepo'
        }
    };
};

export const getBuildContext = (
    projectType: ProjectType,
    cwd: string = process.cwd(),
    applicationVersionTimestamp: AppVersionTimestamp = defaultAppVersionTimestamp
): either.Either<Error, BuildContext> =>
    func.pipe(
        readDockerConfig(cwd),
        either.map((dockerConfig) =>
            createBuildContext(
                projectType,
                dockerConfig,
                applicationVersionTimestamp
            )
        )
    );
