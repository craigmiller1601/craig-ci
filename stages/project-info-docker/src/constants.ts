import path from 'path';

export const DOCKER_PROJECT_FILE = 'docker.json';
export const DEPLOY_DIRECTORY = 'deploy';
export const HELM_DEPLOY_FILE = path.join(
    DEPLOY_DIRECTORY,
    'chart',
    'Chart.yaml'
);
