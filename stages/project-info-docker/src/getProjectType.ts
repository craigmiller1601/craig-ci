import { either } from 'fp-ts';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import path from 'path';
import { DOCKER_PROJECT_FILE, HELM_DEPLOY_FILE } from './constants';
import { existsSync } from '@craig-ci/fp-io';

export const getProjectType = (
    cwd: string = process.cwd()
): either.Either<Error, ProjectType> => {
    const dockerJson = path.join(cwd, DOCKER_PROJECT_FILE);
    const chartFile = path.join(cwd, HELM_DEPLOY_FILE);

    if (existsSync(dockerJson) && existsSync(chartFile)) {
        return either.right('DockerApplication');
    }

    if (existsSync(dockerJson)) {
        return either.right('DockerImage');
    }

    return either.left(new Error('Unknown project type'));
};
