import { function as func, either } from 'fp-ts';
import { getProjectType } from './getProjectType';
import { getBuildContext } from './getBuildContext';
import { writeContext } from '@craig-ci/context/io';
import { terminateEither } from '@craig-ci/context/terminate';

console.log('Getting docker project info');

func.pipe(
    getProjectType(),
    either.chain(getBuildContext),
    either.chain(writeContext),
    terminateEither
);
