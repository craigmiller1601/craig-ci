import { test, vi, beforeEach, MockedFunction, expect } from 'vitest';
import path from 'path';
import { match, P } from 'ts-pattern';
import { RunCommandType } from '@craig-ci/context/cmd';
import { runTerraform } from '../src/runTerraform';
import { taskEither } from 'fp-ts';

type ProcessEnv = typeof process.env;
type Scenario =
    | 'has-terraform'
    | 'has-terraform-with-variables'
    | 'does-not-have-terraform';

type HasTerraformScenario = Exclude<Scenario, 'does-not-have-terraform'>;

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'runTerraform'
);

const getWorkingDir = (scenario: Scenario): string =>
    match(scenario)
        .with(
            P.union('has-terraform', 'has-terraform-with-variables'),
            () => WORKING_DIR_ROOT
        )
        .with('does-not-have-terraform', () => process.cwd())
        .exhaustive();

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

const getEnv = (scenario: Scenario): ProcessEnv => {
    if (scenario === 'has-terraform-with-variables') {
        return {
            TERRAFORM_VARS: '--set abc=def'
        };
    }

    return {};
};

test.each<Scenario>([
    'has-terraform',
    'has-terraform-with-variables',
    'does-not-have-terraform'
])('runs terraform for scenario %s', async (scenario) => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const workingDir = getWorkingDir(scenario);
    const terraformDir = path.join(workingDir, 'deploy', 'terraform');
    const env = getEnv(scenario);
    const result = await runTerraform({
        cwd: workingDir,
        env,
        runCommand: runCommandMock
    })();
    expect(result).toBeRight();

    if (scenario === 'does-not-have-terraform') {
        expect(runCommandMock).not.toHaveBeenCalled();
        return;
    }

    expect(runCommandMock).toHaveBeenCalledTimes(2);
    expect(runCommandMock).toHaveBeenNthCalledWith(1, 'terraform init', {
        cwd: terraformDir
    });
    const command = match<HasTerraformScenario, string>(scenario)
        .with(
            'has-terraform-with-variables',
            () => 'terraform apply -auto-approve --set abc=def'
        )
        .with('has-terraform', () => 'terraform apply -auto-approve')
        .exhaustive();

    expect(runCommandMock).toHaveBeenNthCalledWith(2, command, {
        cwd: terraformDir,
        returnOutput: false
    });
});
