import { test, expect, beforeEach, afterEach } from 'vitest';
import { match } from 'ts-pattern';
import path from 'path';
import { setupKubernetesConfig } from '../src/setupKubernetesConfig';
import fs from 'fs';
import { KubernetesConfig } from '../src/KubernetesConfig';
import yaml from 'yaml';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'setupKubernetesConfig'
);

const CLUSTER_NAME = 'home-server';
const USER_NAME = 'admin';
const K8S_USER_TOKEN = 'token';
const KUBERNETES_SERVICE_HOST = 'host';
const K8S_CA_DATA = 'ca';

const config: KubernetesConfig = {
    apiVersion: 'v1',
    kind: 'Config',
    'current-context': CLUSTER_NAME,
    clusters: [
        {
            name: CLUSTER_NAME,
            cluster: {
                server: `https://${KUBERNETES_SERVICE_HOST}`,
                'certificate-authority-data': K8S_CA_DATA
            }
        }
    ],
    users: [
        {
            name: USER_NAME,
            user: {
                token: K8S_USER_TOKEN
            }
        }
    ],
    contexts: [
        {
            context: {
                cluster: CLUSTER_NAME,
                user: USER_NAME
            },
            name: CLUSTER_NAME
        }
    ]
};

type ProcessEnv = typeof process.env;
type Scenario = 'success' | 'missing-token' | 'missing-host' | 'missing-ca';

const getEnv = (scenario: Scenario): ProcessEnv =>
    match<Scenario, ProcessEnv>(scenario)
        .with('success', () => ({
            K8S_USER_TOKEN,
            KUBERNETES_SERVICE_HOST,
            K8S_CA_DATA
        }))
        .with('missing-token', () => ({
            KUBERNETES_SERVICE_HOST,
            K8S_CA_DATA
        }))
        .with('missing-host', () => ({
            K8S_USER_TOKEN,
            K8S_CA_DATA
        }))
        .with('missing-ca', () => ({
            K8S_USER_TOKEN,
            KUBERNETES_SERVICE_HOST
        }))
        .exhaustive();

const clean = () =>
    fs
        .readdirSync(WORKING_DIR_ROOT)
        .filter((file) => '.gitkeep' !== file)
        .forEach((file) =>
            fs.rmSync(path.join(WORKING_DIR_ROOT, file), {
                recursive: true,
                force: true
            })
        );

beforeEach(() => {
    clean();
});

afterEach(() => {
    clean();
});

test.each<Scenario>(['success', 'missing-ca', 'missing-host', 'missing-token'])(
    'Sets up kubernetes config for scenario: %s',
    (scenario) => {
        const env = getEnv(scenario);
        const result = setupKubernetesConfig(WORKING_DIR_ROOT, env);

        if (scenario !== 'success') {
            expect(result).toEqualLeft(
                new Error(
                    'Missing required environment variables for terraform kubernetes backend'
                )
            );
            expect(fs.readdirSync(WORKING_DIR_ROOT)).toHaveLength(1);
            return;
        }

        expect(result).toBeRight();

        const configPath = path.join(WORKING_DIR_ROOT, '.kube', 'config');
        expect(fs.existsSync(configPath)).toBe(true);
        const yamlString = fs.readFileSync(configPath, 'utf8');
        const actualConfig = yaml.parse(yamlString) as unknown;
        expect(actualConfig).toEqual(config);
    }
);
