import { function as func, taskEither } from 'fp-ts';
import { runTerraform } from './runTerraform';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { setupKubernetesConfig } from './setupKubernetesConfig';

console.log('Running terraform');

void func.pipe(
    setupKubernetesConfig(),
    taskEither.fromEither,
    taskEither.chain(() => runTerraform()),
    terminateTaskEither
)();
