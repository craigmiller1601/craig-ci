import { either, function as func } from 'fp-ts';
import path from 'path';
import { KubernetesConfig } from './KubernetesConfig';
import os from 'os';
import { mkdirSync, writeFileSync, stringifyYaml } from '@craig-ci/fp-io';

const CLUSTER_NAME = 'home-server';
const USER_NAME = 'admin';

type ProcessEnv = typeof process.env;
type Variables = Readonly<{
    server: string;
    caData: string;
    token: string;
}>;

const createConfig = ({
    server,
    caData,
    token
}: Variables): KubernetesConfig => ({
    apiVersion: 'v1',
    kind: 'Config',
    'current-context': CLUSTER_NAME,
    clusters: [
        {
            name: CLUSTER_NAME,
            cluster: {
                server,
                'certificate-authority-data': caData
            }
        }
    ],
    users: [
        {
            name: USER_NAME,
            user: {
                token
            }
        }
    ],
    contexts: [
        {
            context: {
                cluster: CLUSTER_NAME,
                user: USER_NAME
            },
            name: CLUSTER_NAME
        }
    ]
});

const getVariables = (env: ProcessEnv): either.Either<Error, Variables> => {
    if (
        !env.K8S_USER_TOKEN ||
        !env.KUBERNETES_SERVICE_HOST ||
        !env.K8S_CA_DATA
    ) {
        return either.left(
            new Error(
                'Missing required environment variables for terraform kubernetes backend'
            )
        );
    }

    return either.right({
        token: env.K8S_USER_TOKEN,
        server: `https://${env.KUBERNETES_SERVICE_HOST}`,
        caData: env.K8S_CA_DATA
    });
};

const writeConfig =
    (homedir: string) =>
    (config: KubernetesConfig): either.Either<Error, void> => {
        const configDir = path.join(homedir, '.kube');
        const configPath = path.join(configDir, 'config');

        return func.pipe(
            mkdirSync(configDir),
            either.chain(() => stringifyYaml(config)),
            either.chain((yaml) => writeFileSync(configPath, yaml))
        );
    };

export const setupKubernetesConfig = (
    homedir: string = os.homedir(),
    env: ProcessEnv = process.env
): either.Either<Error, void> => {
    console.log('Preparing kubernetes config');
    return func.pipe(
        getVariables(env),
        either.map(createConfig),
        either.chain(writeConfig(homedir))
    );
};
