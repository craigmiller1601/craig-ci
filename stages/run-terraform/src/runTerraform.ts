import { taskEither, function as func } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import { existsSync } from '@craig-ci/fp-io';

type ProcessEnv = typeof process.env;
type Dependencies = Readonly<{
    cwd: string;
    env: ProcessEnv;
    runCommand: RunCommandType;
}>;

export const runTerraform = (
    dependencies: Dependencies = {
        runCommand,
        env: process.env,
        cwd: process.cwd()
    }
): taskEither.TaskEither<Error, string> => {
    const terraformDir = path.join(dependencies.cwd, 'deploy', 'terraform');
    if (!existsSync(terraformDir)) {
        console.log('Terraform directory is not present, skipping');
        return taskEither.right('');
    }

    const variableString = dependencies.env.TERRAFORM_VARS ?? '';
    return func.pipe(
        dependencies.runCommand('terraform init', {
            cwd: terraformDir
        }),
        taskEither.chain(() =>
            dependencies.runCommand(
                `terraform apply -auto-approve ${variableString}`.trim(),
                {
                    cwd: terraformDir,
                    returnOutput: false
                }
            )
        )
    );
};
