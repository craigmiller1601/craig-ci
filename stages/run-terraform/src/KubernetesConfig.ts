export type KubernetesCluster = Readonly<{
    cluster: KubernetesClusterDetails;
    name: string;
}>;

export type KubernetesClusterDetails = Readonly<{
    'certificate-authority-data': string;
    server: string;
}>;

export type KubernetesContext = Readonly<{
    context: KubernetesContextDetails;
    name: string;
}>;

export type KubernetesContextDetails = Readonly<{
    cluster: string;
    user: string;
}>;

export type KubernetesUser = Readonly<{
    name: string;
    user: KubernetesUserDetails;
}>;

export type KubernetesUserDetails = Readonly<{
    token: string;
}>;

export type KubernetesConfig = Readonly<{
    apiVersion: 'v1';
    clusters: ReadonlyArray<KubernetesCluster>;
    contexts: ReadonlyArray<KubernetesContext>;
    'current-context': string;
    kind: 'Config';
    users: ReadonlyArray<KubernetesUser>;
}>;
