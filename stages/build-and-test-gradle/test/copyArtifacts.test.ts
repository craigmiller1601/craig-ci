import fs from 'fs';
import path from 'path';
import { afterEach, beforeEach, test, expect } from 'vitest';
import { copyArtifacts } from '../src/copyArtifacts';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'copyGradleArtifacts'
);
const ARTIFACTS_DIR = path.join(WORKING_DIR_ROOT, 'artifacts');

const clean = () =>
    fs.rmSync(ARTIFACTS_DIR, {
        recursive: true,
        force: true
    });

beforeEach(() => {
    clean();
});

afterEach(() => {
    clean();
});

test('Copies artifact', () => {
    const result = copyArtifacts(WORKING_DIR_ROOT);
    expect(result).toBeRight();

    expect(fs.existsSync(ARTIFACTS_DIR)).toBe(true);
    const files = fs.readdirSync(ARTIFACTS_DIR);
    expect(files).toEqual(['hello.jar']);
});
