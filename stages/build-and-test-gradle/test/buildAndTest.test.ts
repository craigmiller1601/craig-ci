import { test, vi, expect, beforeEach, MockedFunction } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { buildAndTest } from '../src/buildAndTest';
import { taskEither } from 'fp-ts';

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

test('Builds & tests the project', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const result = await buildAndTest({
        runCommand: runCommandMock
    })();

    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledWith('gradle clean build', {
        returnOutput: false
    });
});
