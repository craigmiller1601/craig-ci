import { either, function as func, readonlyArray } from 'fp-ts';
import path from 'path';
import { cpSync, existsSync, mkdirSync, readdirSync } from '@craig-ci/fp-io';

const getJarFiles = (
    cwd: string
): either.Either<Error, ReadonlyArray<string>> => {
    const gradleLibsDir = path.join(cwd, 'build', 'libs');
    return func.pipe(
        readdirSync(gradleLibsDir),
        either.map(
            func.flow(
                readonlyArray.filter((file) => file.endsWith('.jar')),
                readonlyArray.map((file) => path.join(gradleLibsDir, file))
            )
        )
    );
};

const copyJarFiles =
    (cwd: string) =>
    (jarFiles: ReadonlyArray<string>): either.Either<Error, unknown> => {
        const artifactsDir = path.join(cwd, 'artifacts');

        let mkdirEither: either.Either<Error, unknown> = either.right(
            func.constVoid()
        );
        if (!existsSync(artifactsDir)) {
            mkdirEither = mkdirSync(artifactsDir);
        }

        return func.pipe(
            mkdirEither,
            either.chain(() =>
                func.pipe(
                    jarFiles,
                    readonlyArray.map((file) => {
                        const fileName = path.basename(file);
                        return [file, path.join(artifactsDir, fileName)];
                    }),
                    readonlyArray.map(([src, dest]) => cpSync(src, dest)),
                    either.sequenceArray
                )
            )
        );
    };

export const copyArtifacts = (
    cwd: string = process.cwd()
): either.Either<Error, void> =>
    func.pipe(
        getJarFiles(cwd),
        either.chain(copyJarFiles(cwd)),
        either.map(() => func.constVoid())
    );
