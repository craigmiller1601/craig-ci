import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
}>;

export const buildAndTest = (
    dependencies: Dependencies = {
        runCommand
    }
): taskEither.TaskEither<Error, string> =>
    dependencies.runCommand('gradle clean build', {
        returnOutput: false
    });
