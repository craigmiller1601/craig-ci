import { function as func, taskEither } from 'fp-ts';
import { buildAndTest } from './buildAndTest';
import { copyArtifacts } from './copyArtifacts';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Building & testing gradle project');

void func.pipe(
    buildAndTest(),
    taskEither.chainEitherK(() => copyArtifacts()),
    terminateTaskEither
)();
