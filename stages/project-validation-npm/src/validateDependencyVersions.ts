import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { either, function as func, option, readonlyArray } from 'fp-ts';
import { PackageJson, readNpmProjectFile } from '@craig-ci/npm-support';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';
import { semverSatisifies } from './semverUtils';

type JsEntries = ReadonlyArray<[string, string]>;

const entries = (obj?: { [key: string]: string }): JsEntries =>
    func.pipe(
        option.fromNullable(obj),
        option.map(Object.entries),
        option.getOrElse((): JsEntries => [])
    );

const validateNoBetaDependencies = (
    packageJson: PackageJson
): either.Either<Error, PackageJson> => {
    const hasBeta =
        func.pipe(
            entries(packageJson.dependencies),
            readonlyArray.concat(entries(packageJson.devDependencies)),
            readonlyArray.concat(entries(packageJson.peerDependencies)),
            readonlyArray.filter(([, value]) => value.includes('beta'))
        ).length > 0;

    if (hasBeta) {
        return either.left(
            new Error('Cannot have beta dependencies in NPM release')
        );
    }
    return either.right(packageJson);
};

type PeerDependencyData = Readonly<{
    name: string;
    peerRange: string;
    mainDependencyVersion?: string;
    devDependencyVersion?: string;
}>;

const validatePeerDependencies = (
    packageJson: PackageJson
): either.Either<Error, PackageJson> =>
    func.pipe(
        entries(packageJson.peerDependencies),
        readonlyArray.map(
            ([key, value]): PeerDependencyData => ({
                name: key,
                peerRange: value,
                mainDependencyVersion: packageJson.dependencies?.[key],
                devDependencyVersion: packageJson.devDependencies?.[key]
            })
        ),
        readonlyArray.map((data) => {
            const validMainVersion =
                !data.mainDependencyVersion ||
                semverSatisifies(data.mainDependencyVersion, data.peerRange);
            const validDevVersion =
                !data.devDependencyVersion ||
                semverSatisifies(data.devDependencyVersion, data.peerRange);

            if (validMainVersion && validDevVersion) {
                return either.right(data);
            }

            const version =
                data.mainDependencyVersion ?? data.devDependencyVersion;

            return either.left(
                new Error(
                    `Dependency ${data.name} does not satisfy project's peer range. Version: ${version} Range: ${data.peerRange}`
                )
            );
        }),
        either.sequenceArray,
        either.map(() => packageJson)
    );

export const validateDependencyVersions = (
    context: BuildContext,
    cwd: string = process.cwd()
): either.Either<Error, void> => {
    console.log('Validating NPM dependency versions');
    const noBetaDependencies = isRelease(context.projectInfo.versionType)
        ? validateNoBetaDependencies
        : (p: PackageJson) => either.right(p);

    return func.pipe(
        readNpmProjectFile(cwd),
        either.chainFirst(noBetaDependencies),
        either.chainFirst(validatePeerDependencies),
        either.map(() => func.constVoid())
    );
};
