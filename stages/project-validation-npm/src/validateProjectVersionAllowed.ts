import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither } from 'fp-ts';
import {
    NexusRepoGroupSearchFn,
    searchForNpmReleases as defaultSearchForNpmReleases
} from '@craig-ci/nexus-api';
import { validateProjectVersionAllowed } from '@craig-ci/project-version-validation';

export const validateNpmProjectVersionAllowed = (
    context: BuildContext,
    searchForNpmReleases: NexusRepoGroupSearchFn = defaultSearchForNpmReleases
): taskEither.TaskEither<Error, BuildContext> => {
    console.log('Validating NPM project version');
    return validateProjectVersionAllowed(context, searchForNpmReleases);
};
