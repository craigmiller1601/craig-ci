import { function as func, taskEither, either } from 'fp-ts';
import { readContext } from '@craig-ci/context/io';
import { validateNpmProjectVersionAllowed } from './validateProjectVersionAllowed';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { validateDependencyVersions } from './validateDependencyVersions';

console.log('Validating NPM project');

void func.pipe(
    readContext(),
    either.chainFirst(validateDependencyVersions),
    taskEither.fromEither,
    taskEither.chainFirst(validateNpmProjectVersionAllowed),
    terminateTaskEither
)();
