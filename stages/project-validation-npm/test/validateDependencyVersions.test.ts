import { test, expect } from 'vitest';
import path from 'path';
import { match } from 'ts-pattern';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';
import { validateDependencyVersions } from '../src/validateDependencyVersions';
import { either } from 'fp-ts';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

type DependencyValidationScenario = 'all valid' | 'invalid dependencies';

const createContext = (versionType: VersionType): BuildContext => ({
    projectType: 'NpmApplication',
    projectInfo: {
        group: 'group',
        name: 'name',
        version: isRelease(versionType) ? '1.0.0' : '1.0.0-beta',
        versionType,
        repoType: 'polyrepo'
    }
});

test('skips dependency validation for npm release', () => {
    const cwd = path.join(
        WORKING_DIR_ROOT,
        'npmReleaseApplicationBadDependency'
    );

    const buildContext = createContext('PreRelease');
    const result = validateDependencyVersions(buildContext, cwd);
    expect(result).toBeRight();
});

test.each<DependencyValidationScenario>(['all valid', 'invalid dependencies'])(
    'validating dependencies for npm release. Scenario: %s',
    (scenario) => {
        const workingDir = match(scenario)
            .with('all valid', () => 'npmReleaseApplication')
            .with(
                'invalid dependencies',
                () => 'npmReleaseApplicationBadDependency'
            )
            .exhaustive();
        const cwd = path.join(WORKING_DIR_ROOT, workingDir);

        const buildContext = createContext('Release');
        const result = validateDependencyVersions(buildContext, cwd);
        if (scenario === 'all valid') {
            expect(result).toBeRight();
        } else {
            expect(result).toEqualLeft(
                new Error('Cannot have beta dependencies in NPM release')
            );
        }
    }
);

type NpmPeerValidationScenario =
    | 'invalid beta for release'
    | 'valid beta for pre-release'
    | 'peer lower than dev dependency'
    | 'peer lower than main dependency'
    | 'peer same as main dependency'
    | 'beta peer higher than release main dependency';
const validScenarios: ReadonlyArray<NpmPeerValidationScenario> = [
    'valid beta for pre-release',
    'peer same as main dependency'
];
const preReleaseScenarios: ReadonlyArray<NpmPeerValidationScenario> = [
    'beta peer higher than release main dependency',
    'valid beta for pre-release'
];

test.each<NpmPeerValidationScenario>([
    'invalid beta for release',
    'valid beta for pre-release',
    'peer lower than dev dependency',
    'peer lower than main dependency',
    'peer same as main dependency',
    'beta peer higher than release main dependency'
])('validating npm peer dependencies. Scenario: %s', (scenario) => {
    const workingDir = match(scenario)
        .with('invalid beta for release', () => 'invalidBetaForRelease')
        .with('valid beta for pre-release', () => 'validBetaForPreRelease')
        .with(
            'peer lower than dev dependency',
            () => 'peerDependencyLowerThanDevDependency'
        )
        .with(
            'peer lower than main dependency',
            () => 'peerDependencyLowerThanMainDependency'
        )
        .with(
            'peer same as main dependency',
            () => 'peerDependencySameAsMainDependency'
        )
        .with(
            'beta peer higher than release main dependency',
            () => 'betaPeerIsHigherThanRelease'
        )
        .exhaustive();
    const cwd = path.join(WORKING_DIR_ROOT, '__npmPeers__', workingDir);

    const buildContext = createContext(
        preReleaseScenarios.includes(scenario) ? 'PreRelease' : 'Release'
    );
    const result = validateDependencyVersions(buildContext, cwd);
    if (validScenarios.includes(scenario)) {
        expect(result).toBeRight();
    } else if (scenario === 'invalid beta for release') {
        expect(result).toEqualLeft(
            new Error('Cannot have beta dependencies in NPM release')
        );
    } else {
        expect(result).toBeLeft();
        const error = (result as either.Left<Error>).left;
        expect(error.message).toEqual(
            expect.stringContaining(
                "Dependency @craigmiller160/foo-bar does not satisfy project's peer range"
            )
        );
    }
});
