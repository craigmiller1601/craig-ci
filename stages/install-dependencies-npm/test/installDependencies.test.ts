import { beforeEach, MockedFunction, test, vi, expect } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { installDependencies } from '../src/installDependencies';
import { taskEither } from 'fp-ts';
import { NpmBuildTool } from '../src/NpmBuildTool';

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

test.each<NpmBuildTool>(['npm', 'pnpm', 'yarn'])(
    'Installs dependencies with %s',
    async (tool) => {
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const result = await installDependencies(tool, runCommandMock)();
        expect(result).toBeRight();

        expect(runCommandMock).toHaveBeenCalledWith(`${tool} install`);
    }
);
