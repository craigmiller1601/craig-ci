import { test, expect } from 'vitest';
import path from 'path';
import { match } from 'ts-pattern';
import { function as func } from 'fp-ts';
import { getNpmBuildTool } from '../src/getNpmBuildTool';
import { NpmBuildTool } from '../src/NpmBuildTool';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'npmBuildTool'
);

type NpmBuildToolScenario = NpmBuildTool | 'Unknown';

const getWorkingDirName = (scenario: NpmBuildToolScenario): string =>
    match(scenario)
        .with('Unknown', () => '.')
        .otherwise(func.identity);

test.each<NpmBuildToolScenario>(['Unknown', 'npm', 'pnpm', 'yarn'])(
    'Detects the NPM build tool for %s',
    (scenario) => {
        const workingDirName = getWorkingDirName(scenario);
        const workingDir = path.join(WORKING_DIR_ROOT, workingDirName);
        const result = getNpmBuildTool(workingDir);
        if (scenario === 'Unknown') {
            expect(result).toBeLeft();
        } else {
            expect(result).toEqualRight(scenario);
        }
    }
);
