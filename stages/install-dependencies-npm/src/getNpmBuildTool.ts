import { either } from 'fp-ts';
import {
    getNpmToolFile,
    getPnpmToolFile,
    getYarnToolFile
} from '@craig-ci/npm-support';
import { existsSync } from '@craig-ci/fp-io';
import { NpmBuildTool } from './NpmBuildTool';

export const getNpmBuildTool = (
    cwd: string = process.cwd()
): either.Either<Error, NpmBuildTool> => {
    const npmToolFile = getNpmToolFile(cwd);
    const pnpmToolFile = getPnpmToolFile(cwd);
    const yarnToolFile = getYarnToolFile(cwd);

    if (existsSync(npmToolFile)) {
        return either.right('npm');
    }

    if (existsSync(pnpmToolFile)) {
        return either.right('pnpm');
    }

    if (existsSync(yarnToolFile)) {
        return either.right('yarn');
    }

    return either.left(new Error('Unable to identify NPM build tool'));
};
