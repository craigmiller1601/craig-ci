import { function as func, taskEither } from 'fp-ts';
import { getNpmBuildTool } from './getNpmBuildTool';
import { installDependencies } from './installDependencies';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Installing NPM dependencies');

void func.pipe(
    getNpmBuildTool(),
    taskEither.fromEither,
    taskEither.chain(installDependencies),
    terminateTaskEither
)();
