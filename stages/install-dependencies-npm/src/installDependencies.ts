import { NpmBuildTool } from './NpmBuildTool';
import { taskEither } from 'fp-ts';
import {
    RunCommandType,
    runCommand as defaultRunCommand
} from '@craig-ci/context/cmd';

export const installDependencies = (
    npmBuildTool: NpmBuildTool,
    runCommand: RunCommandType = defaultRunCommand
): taskEither.TaskEither<Error, string> =>
    runCommand(`${npmBuildTool} install`);
