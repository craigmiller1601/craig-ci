import {
    RunCommandType,
    runCommand as defaultRunCommand
} from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import path from 'path';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    env: NodeJS.ProcessEnv;
    cwd: string;
}>;

export const helmLint = (
    dependencies: Dependencies = {
        runCommand: defaultRunCommand,
        env: process.env,
        cwd: process.cwd()
    }
): taskEither.TaskEither<Error, string> =>
    dependencies.runCommand(
        `helm lint ${dependencies.env.HELM_EXTRA_VALUES ?? ''}`,
        {
            cwd: path.join(dependencies.cwd, 'deploy', 'chart'),
            returnOutput: false
        }
    );
