import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { either, function as func } from 'fp-ts';
import { readHelmChart } from '@craig-ci/helm-support';
import { isHelm } from '@craig-ci/context/utils/projectTypeUtils';

export const chartNameIsValid = (
    context: BuildContext,
    cwd: string = process.cwd()
): either.Either<Error, void> => {
    if (isHelm(context.projectType)) {
        return either.right(func.constVoid());
    }

    return func.pipe(
        readHelmChart(cwd),
        either.filterOrElse(
            (chart) => chart.name === context.projectInfo.name,
            () => new Error('Helm chart name must match project name')
        ),
        either.map(() => func.constVoid())
    );
};
