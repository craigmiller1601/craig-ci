import { function as func, either, taskEither } from 'fp-ts';
import { helmLint } from './helmLint';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { readContext } from '@craig-ci/context/io';
import { chartNameIsValid } from './chartNameIsValid';

console.log('Validating helm');

void func.pipe(
    readContext(),
    either.chain((context) => chartNameIsValid(context)),
    taskEither.fromEither,
    taskEither.chain(() => helmLint()),
    terminateTaskEither
)();
