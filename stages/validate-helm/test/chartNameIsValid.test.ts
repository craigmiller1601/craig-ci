import { test, expect } from 'vitest';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { match } from 'ts-pattern';
import path from 'path';
import { chartNameIsValid } from '../src/chartNameIsValid';

type Scenario = 'valid' | 'invalid' | 'helm-project';

const WORKING_DIR_ROOT = path.join(
    process.cwd(),
    '..',
    '..',
    '__working_directories__',
    'dockerReleaseApplication'
);

const createContext = (
    projectType: ProjectType,
    name: string
): BuildContext => ({
    projectType,
    projectInfo: {
        name,
        version: '',
        group: '',
        repoType: 'polyrepo',
        versionType: 'Release'
    }
});

const getContext = (scenario: Scenario): BuildContext =>
    match(scenario)
        .with('valid', () => createContext('NpmApplication', 'my-app'))
        .with('invalid', () => createContext('NpmApplication', 'foobar'))
        .with('helm-project', () => createContext('HelmApplication', 'foobar'))
        .exhaustive();

test.each<Scenario>(['valid', 'invalid', 'helm-project'])(
    'Tests the chart name is valid for scenario %s',
    (scenario) => {
        const context = getContext(scenario);
        const result = chartNameIsValid(context, WORKING_DIR_ROOT);

        if (scenario === 'invalid') {
            expect(result).toEqualLeft(
                new Error('Helm chart name must match project name')
            );
        } else {
            expect(result).toBeRight();
        }
    }
);
