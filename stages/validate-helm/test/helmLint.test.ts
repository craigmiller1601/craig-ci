import { beforeEach, expect, MockedFunction, test, vi } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import { match } from 'ts-pattern';
import { helmLint } from '../src/helmLint';
import { taskEither } from 'fp-ts';

type Extras = 'with-values-file' | 'with-extra-values';

type Scenario = Readonly<{
    extras: ReadonlyArray<Extras>;
}>;

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();
const WORKING_DIR_ROOT = path.join(
    process.cwd(),
    '..',
    '..',
    '__working_directories__'
);

const includesWithValuesFiles = (extras: ReadonlyArray<Extras>): boolean =>
    extras.includes('with-values-file');

const getWorkingDir = (extras: ReadonlyArray<Extras>): string =>
    match(extras)
        .when(includesWithValuesFiles, () =>
            path.join(WORKING_DIR_ROOT, 'mavenReleaseApplication')
        )
        .otherwise(() =>
            path.join(WORKING_DIR_ROOT, 'dockerReleaseApplication')
        );

const getEnv = (extras: ReadonlyArray<Extras>): NodeJS.ProcessEnv =>
    match(extras)
        .when(includesWithValuesFiles, () => ({
            HELM_EXTRA_VALUES:
                '--set connect_fork.operator.token.value=$ONEPASSWORD_TOKEN'
        }))
        .otherwise(() => ({}));

beforeEach(() => {
    vi.resetAllMocks();
});

test.each<Scenario>([
    { extras: [] },
    { extras: ['with-values-file'] },
    { extras: ['with-extra-values'] },
    { extras: ['with-values-file', 'with-extra-values'] }
])('performs helm lint with extras: $extras', async ({ extras }) => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const workingDir = getWorkingDir(extras);
    const env = getEnv(extras);
    console.log(workingDir);
    const result = await helmLint({
        cwd: workingDir,
        runCommand: runCommandMock,
        env
    })();
    expect(result).toBeRight();

    const chartDir = path.join(workingDir, 'deploy', 'chart');

    expect(runCommandMock).toHaveBeenCalledWith(
        `helm lint ${env.HELM_EXTRA_VALUES ?? ''}`,
        {
            cwd: chartDir,
            returnOutput: false
        }
    );
});
