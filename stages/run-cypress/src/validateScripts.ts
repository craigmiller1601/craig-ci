import {
    RunCommandType,
    runCommand as defaultRunCommand
} from '@craig-ci/context/cmd';
import { taskEither, function as func } from 'fp-ts';
import { parseJson } from '@craig-ci/fp-io';

const hasRequiredScripts = (scripts: Record<string, string>): boolean =>
    !!scripts['cypress'];

export const validateScripts = (
    runCommand: RunCommandType = defaultRunCommand
): taskEither.TaskEither<Error, void> =>
    func.pipe(
        runCommand('npm run --json'),
        taskEither.chainEitherK((json) =>
            parseJson<Record<string, string>>(json)
        ),
        taskEither.filterOrElse(
            hasRequiredScripts,
            () => new Error('Project must have cypress script for pipeline')
        ),
        taskEither.map(() => func.constVoid())
    );
