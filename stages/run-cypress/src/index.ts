import { function as func, taskEither } from 'fp-ts';
import { validateScripts } from './validateScripts';
import { runCypress } from './runCypress';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Running cypress tests');

void func.pipe(
    validateScripts(),
    taskEither.chain(() => runCypress()),
    terminateTaskEither
)();
