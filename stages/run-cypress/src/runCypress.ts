import {
    runCommand as defaultRunCommand,
    RunCommandType
} from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';

type ProcessEnv = typeof process.env;

const toZeroIndex = (rawNumber: string): string =>
    (parseInt(rawNumber) - 1).toString();

// Parallel arguments depends on js-config library and are not standard for cypress
const getArgs = (env: ProcessEnv): string => {
    const args = [
        env.CI_NODE_TOTAL ? `--parallel-total=${env.CI_NODE_TOTAL}` : undefined,
        env.CI_NODE_INDEX
            ? `--parallel-index=${toZeroIndex(env.CI_NODE_INDEX)}`
            : undefined
    ]
        .filter((arg): arg is string => !!arg)
        .join(' ');

    if (args) {
        return `-- ${args}`;
    }
    return '';
};

export const runCypress = (
    runCommand: RunCommandType = defaultRunCommand,
    env: ProcessEnv = process.env
): taskEither.TaskEither<Error, string> => {
    const args = getArgs(env);
    return runCommand(`npm run cypress ${args}`, {
        returnOutput: false
    });
};
