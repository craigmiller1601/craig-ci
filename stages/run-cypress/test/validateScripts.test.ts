import { test, expect, beforeEach, vi, MockedFunction } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import fs from 'fs';
import { taskEither } from 'fp-ts';
import { validateScripts } from '../src/validateScripts';

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();
const npmOuptutDir = path.join(__dirname, '__npm-output__');

beforeEach(() => {
    vi.resetAllMocks();
});

type Scenario = 'has-cypress' | 'no-cypress';

test.each<Scenario>(['has-cypress', 'no-cypress'])(
    'Validates available scripts for scenario %s',
    async (scenario) => {
        const outputJsonPath = path.join(npmOuptutDir, `${scenario}.json`);
        const outputJson = fs.readFileSync(outputJsonPath, 'utf8');
        runCommandMock.mockImplementation(() => taskEither.right(outputJson));

        const result = await validateScripts(runCommandMock)();
        if (scenario === 'has-cypress') {
            expect(result).toBeRight();
        } else {
            expect(result).toEqualLeft(
                new Error('Project must have cypress script for pipeline')
            );
        }

        expect(runCommandMock).toHaveBeenCalledWith('npm run --json');
    }
);
