import { test, expect, vi, beforeEach, MockedFunction } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { runCypress } from '../src/runCypress';
import { taskEither } from 'fp-ts';
import { match } from 'ts-pattern';

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

type Scenario = 'standard' | 'with-parallel';
type ProcessEnv = typeof process.env;

const getEnv = (scenario: Scenario): ProcessEnv =>
    match<Scenario, ProcessEnv>(scenario)
        .with('standard', () => ({}))
        .with('with-parallel', () => ({
            CI_NODE_TOTAL: '3',
            CI_NODE_INDEX: '2'
        }))
        .exhaustive();

const getArgs = (scenario: Scenario): string =>
    match(scenario)
        .with('standard', () => '')
        .with('with-parallel', () => '-- --parallel-total=3 --parallel-index=1')
        .exhaustive();

test.each<Scenario>(['standard', 'with-parallel'])(
    'runs cypress for scenario %s',
    async (scenario) => {
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const env = getEnv(scenario);

        const result = await runCypress(runCommandMock, env)();
        expect(result).toBeRight();

        const args = getArgs(scenario);

        expect(runCommandMock).toHaveBeenCalledWith(`npm run cypress ${args}`, {
            returnOutput: false
        });
    }
);
