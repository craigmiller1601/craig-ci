import { function as func, taskEither } from 'fp-ts';
import { readContext } from '@craig-ci/context/io';
import { buildArtifact } from './buildArtifact';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { publishArtifact } from './publishArtifact';
import { testArtifact } from './testArtifact';
import { updateDependencies } from './updateDependencies';

console.log('Building helm artifact');

void func.pipe(
    readContext(),
    taskEither.fromEither,
    taskEither.chainFirst(updateDependencies),
    taskEither.chainFirst(testArtifact),
    taskEither.chainFirst(buildArtifact),
    taskEither.chain(publishArtifact),
    terminateTaskEither
)();
