import { either, taskEither, function as func } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import path from 'path';

type ProcessEnv = typeof process.env;
type Dependencies = Readonly<{
    runCommand: RunCommandType;
    cwd: string;
    env: ProcessEnv;
}>;

const ensureCredentials = (env: ProcessEnv): either.Either<Error, void> => {
    const { NEXUS_USER, NEXUS_PASSWORD } = env;
    if (!NEXUS_USER || !NEXUS_PASSWORD) {
        return either.left(new Error('Missing nexus credentials'));
    }

    return either.right(func.constVoid());
};

export const publishArtifact = (
    context: BuildContext,
    dependencies: Dependencies = {
        runCommand,
        cwd: process.cwd(),
        env: process.env
    }
): taskEither.TaskEither<Error, string> => {
    const artifactsDir = path.join(dependencies.cwd, 'artifacts');
    const fileName = `${context.projectInfo.name}-${context.projectInfo.version}.tgz`;

    return func.pipe(
        ensureCredentials(dependencies.env),
        taskEither.fromEither,
        taskEither.chain(() =>
            dependencies.runCommand(
                `curl -f -u $NEXUS_USER:$NEXUS_PASSWORD http://nexus-standard.nexus/repository/helm-private/ --upload-file ${fileName}`,
                {
                    cwd: artifactsDir
                }
            )
        )
    );
};
