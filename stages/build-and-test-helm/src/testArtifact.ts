import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither, function as func } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    cwd: string;
}>;

export const testArtifact = (
    context: BuildContext,
    dependencies: Dependencies = {
        runCommand,
        cwd: process.cwd()
    }
): taskEither.TaskEither<Error, void> => {
    const chartDir = path.join(dependencies.cwd, 'deploy', 'chart');
    return func.pipe(
        dependencies.runCommand(`helm unittest ${chartDir}`, {
            returnOutput: false
        }),
        taskEither.map(() => func.constVoid())
    );
};
