import { taskEither, function as func, readonlyArray } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { readHelmChart } from '@craig-ci/helm-support';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    cwd: string;
}>;

export const updateDependencies = (
    context: BuildContext,
    dependencies: Dependencies = {
        runCommand,
        cwd: process.cwd()
    }
): taskEither.TaskEither<Error, void> => {
    const deployDir = path.join(dependencies.cwd, 'deploy');
    const chartDir = path.join(deployDir, 'chart');
    return func.pipe(
        readHelmChart(dependencies.cwd),
        taskEither.fromEither,
        taskEither.chain((chart) =>
            func.pipe(
                chart.dependencies ?? [],
                readonlyArray.mapWithIndex((index, dep) =>
                    dependencies.runCommand(
                        `helm repo add dep${index} ${dep.repository}`,
                        {
                            cwd: chartDir,
                            returnOutput: false
                        }
                    )
                ),
                taskEither.sequenceArray
            )
        ),
        taskEither.chain(() =>
            dependencies.runCommand('helm dependency build', {
                cwd: chartDir,
                returnOutput: false
            })
        ),
        taskEither.map(() => func.constVoid())
    );
};
