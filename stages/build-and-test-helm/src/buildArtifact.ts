import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { either, function as func, taskEither } from 'fp-ts';
import path from 'path';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import { cpSync, existsSync, mkdirSync } from '@craig-ci/fp-io';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    cwd: string;
}>;

const copyArtifact = (
    context: BuildContext,
    deployDir: string,
    artifactsDir: string
): either.Either<Error, void> => {
    const fileName = `${context.projectInfo.name}-${context.projectInfo.version}.tgz`;

    let mkdirEither: either.Either<Error, void> = either.right(
        func.constVoid()
    );
    if (!existsSync(artifactsDir)) {
        mkdirEither = mkdirSync(artifactsDir);
    }

    return func.pipe(
        mkdirEither,
        either.chain(() =>
            cpSync(
                path.join(deployDir, fileName),
                path.join(artifactsDir, fileName)
            )
        )
    );
};

export const buildArtifact = (
    context: BuildContext,
    dependencies: Dependencies = {
        runCommand,
        cwd: process.cwd()
    }
): taskEither.TaskEither<Error, void> => {
    const deployDir = path.join(dependencies.cwd, 'deploy');
    const artifactsDir = path.join(dependencies.cwd, 'artifacts');

    return func.pipe(
        dependencies.runCommand(
            `helm package ./chart --version ${context.projectInfo.version}`,
            {
                cwd: deployDir,
                returnOutput: false
            }
        ),
        taskEither.chainEitherK(() =>
            copyArtifact(context, deployDir, artifactsDir)
        )
    );
};
