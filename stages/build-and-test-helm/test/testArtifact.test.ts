import { beforeEach, expect, MockedFunction, test, vi } from 'vitest';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import path from 'path';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import { testArtifact } from '../src/testArtifact';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'helmLibrary'
);
const deployDir = path.join(WORKING_DIR_ROOT, 'deploy');
const chartDir = path.join(deployDir, 'chart');

const context: BuildContext = {
    projectType: 'HelmLibrary',
    projectInfo: {
        group: 'craigmiller160',
        name: 'my-project',
        version: '1.0.0',
        repoType: 'polyrepo',
        versionType: 'Release'
    }
};

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

test('runs unit tests on helm chart', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const result = await testArtifact(context, {
        runCommand: runCommandMock,
        cwd: WORKING_DIR_ROOT
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(1);
    expect(runCommandMock).toHaveBeenNthCalledWith(
        1,
        `helm unittest ${chartDir}`,
        {
            returnOutput: false
        }
    );
});
