import { beforeEach, expect, MockedFunction, test, vi } from 'vitest';
import path from 'path';
import { RunCommandType } from '@craig-ci/context/cmd';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither } from 'fp-ts';
import { updateDependencies } from '../src/updateDependencies';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);
const WORKING_DIR_NO_DEPS_ROOT = path.join(WORKING_DIR_ROOT, 'helmLibrary');
const WORKING_DIR_WITH_DEPS_ROOT = path.join(
    WORKING_DIR_ROOT,
    'helmLibraryWithDependencies'
);

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

const context: BuildContext = {
    projectType: 'HelmLibrary',
    projectInfo: {
        group: 'craigmiller160',
        name: 'my-project',
        version: '1.0.0',
        repoType: 'polyrepo',
        versionType: 'Release'
    }
};

test('updateDependencies', async () => {
    const deployDir = path.join(WORKING_DIR_NO_DEPS_ROOT, 'deploy');
    const chartDir = path.join(deployDir, 'chart');
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const result = await updateDependencies(context, {
        runCommand: runCommandMock,
        cwd: WORKING_DIR_NO_DEPS_ROOT
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(1);
    expect(runCommandMock).toHaveBeenNthCalledWith(1, 'helm dependency build', {
        cwd: chartDir,
        returnOutput: false
    });
});

test('updateDependencies and add repos', async () => {
    const deployDir = path.join(WORKING_DIR_WITH_DEPS_ROOT, 'deploy');
    const chartDir = path.join(deployDir, 'chart');
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const result = await updateDependencies(context, {
        runCommand: runCommandMock,
        cwd: WORKING_DIR_WITH_DEPS_ROOT
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(2);
    expect(runCommandMock).toHaveBeenNthCalledWith(
        1,
        'helm repo add dep0 https://nexus.craigmiller160.us/repository/helm-private',
        {
            cwd: chartDir,
            returnOutput: false
        }
    ),
        expect(runCommandMock).toHaveBeenNthCalledWith(
            2,
            'helm dependency build',
            {
                cwd: chartDir,
                returnOutput: false
            }
        );
});
