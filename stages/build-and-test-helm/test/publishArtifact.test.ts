import { beforeEach, MockedFunction, test, vi, expect } from 'vitest';
import { match } from 'ts-pattern';
import { RunCommandType } from '@craig-ci/context/cmd';
import { publishArtifact } from '../src/publishArtifact';
import path from 'path';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither } from 'fp-ts';

type ProcessEnv = typeof process.env;
type MissingEnvScenario = 'username' | 'password' | 'both';

const getEnvForMissingScenario = (scenario: MissingEnvScenario): ProcessEnv =>
    match<MissingEnvScenario, ProcessEnv>(scenario)
        .with('username', () => ({
            NEXUS_PASSWORD: 'password'
        }))
        .with('password', () => ({
            NEXUS_USER: 'username'
        }))
        .with('both', () => ({}))
        .exhaustive();

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();
const context: BuildContext = {
    projectType: 'HelmLibrary',
    projectInfo: {
        group: 'craigmiller160',
        name: 'my-lib',
        version: '1.0.0',
        versionType: 'Release',
        repoType: 'polyrepo'
    }
};

test.each<MissingEnvScenario>(['username', 'password', 'both'])(
    'Missing required env',
    async (scenario) => {
        const env = getEnvForMissingScenario(scenario);
        const result = await publishArtifact(context, {
            env,
            cwd: process.cwd(),
            runCommand: runCommandMock
        })();
        expect(result).toEqualLeft(new Error('Missing nexus credentials'));

        expect(runCommandMock).not.toHaveBeenCalled();
    }
);

test('Publishes the artifact', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const env: ProcessEnv = {
        NEXUS_USER: 'username',
        NEXUS_PASSWORD: 'password'
    };
    const result = await publishArtifact(context, {
        env,
        cwd: process.cwd(),
        runCommand: runCommandMock
    })();
    expect(result).toBeRight();

    const filename = `${context.projectInfo.name}-${context.projectInfo.version}.tgz`;
    expect(runCommandMock).toHaveBeenCalledWith(
        `curl -f -u $NEXUS_USER:$NEXUS_PASSWORD http://nexus-standard.nexus/repository/helm-private/ --upload-file ${filename}`,
        {
            cwd: path.join(process.cwd(), 'artifacts')
        }
    );
});
