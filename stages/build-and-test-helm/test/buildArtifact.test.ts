import {
    afterEach,
    beforeEach,
    MockedFunction,
    test,
    vi,
    expect
} from 'vitest';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import fs from 'fs';
import { buildArtifact } from '../src/buildArtifact';
import { taskEither } from 'fp-ts';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'helmLibrary'
);
const deployDir = path.join(WORKING_DIR_ROOT, 'deploy');
const artifactsDir = path.join(WORKING_DIR_ROOT, 'artifacts');

const context: BuildContext = {
    projectType: 'HelmLibrary',
    projectInfo: {
        group: 'craigmiller160',
        name: 'my-project',
        version: '1.0.0',
        repoType: 'polyrepo',
        versionType: 'Release'
    }
};

const clean = () => {
    if (fs.existsSync(artifactsDir)) {
        fs.rmSync(artifactsDir, {
            recursive: true,
            force: true
        });
    }
};

beforeEach(() => {
    vi.resetAllMocks();
    clean();
});

afterEach(() => {
    clean();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

test('Builds artifact', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const result = await buildArtifact(context, {
        runCommand: runCommandMock,
        cwd: WORKING_DIR_ROOT
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(1);
    expect(runCommandMock).toHaveBeenNthCalledWith(
        1,
        `helm package ./chart --version ${context.projectInfo.version}`,
        {
            cwd: deployDir,
            returnOutput: false
        }
    );

    const file = path.join(artifactsDir, 'my-project-1.0.0.tgz');
    expect(fs.existsSync(file)).toBe(true);
});
