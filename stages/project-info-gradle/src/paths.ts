import path from 'path';
import { existsSync } from '@craig-ci/fp-io';

export const GRADLE_KOTLIN_PROJECT_FILE = 'build.gradle.kts';
export const GRADLE_GROOVY_PROJECT_FILE = 'build.gradle';
export const DEPLOY_DIRECTORY = 'deploy';

export const hasGradleProjectFile = (cwd: string): boolean =>
    existsSync(path.join(cwd, GRADLE_GROOVY_PROJECT_FILE)) ||
    existsSync(path.join(cwd, GRADLE_KOTLIN_PROJECT_FILE));

export const hasDeployDirectory = (cwd: string): boolean =>
    existsSync(path.join(cwd, DEPLOY_DIRECTORY));
