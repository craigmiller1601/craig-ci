import { either } from 'fp-ts';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { hasDeployDirectory, hasGradleProjectFile } from './paths';
import { match } from 'ts-pattern';

type Checks = Readonly<{
    isGradle: boolean;
    isApp: boolean;
}>;

export const getProjectType = (
    cwd: string = process.cwd()
): either.Either<Error, ProjectType> => {
    const isGradle = hasGradleProjectFile(cwd);
    const isApp = hasDeployDirectory(cwd);
    return match<Checks, either.Either<Error, ProjectType>>({ isGradle, isApp })
        .with({ isGradle: true, isApp: true }, () =>
            either.right('GradleApplication')
        )
        .with({ isGradle: true }, () => either.right('GradleLibrary'))
        .otherwise(() =>
            either.left(new Error('Unable to determine project type'))
        );
};
