import { function as func, either, taskEither } from 'fp-ts';
import { getProjectType } from './getProjectType';
import { getProjectInfo } from './getProjectInfo';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { writeContext } from '@craig-ci/context/io';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Getting project info for gradle project');

void func.pipe(
    getProjectType(),
    either.bindTo('projectType'),
    taskEither.fromEither,
    taskEither.bind('projectInfo', ({ projectType }) =>
        getProjectInfo(projectType)
    ),
    taskEither.map(
        ({ projectType, projectInfo }): BuildContext => ({
            projectType,
            projectInfo
        })
    ),
    taskEither.chainEitherK(writeContext),
    terminateTaskEither
)();
