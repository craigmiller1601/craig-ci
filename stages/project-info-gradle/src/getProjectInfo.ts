import { function as func, taskEither } from 'fp-ts';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import {
    applicationVersionTimestamp,
    AppVersionTimestamp
} from '@craig-ci/context/date';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { isApplication } from '@craig-ci/context/utils/projectTypeUtils';
import { parseJson } from '@craig-ci/fp-io';

const SNAPSHOT_VERSION_REGEX = /^.*-SNAPSHOT/;

export type GradleItem = Readonly<{
    group: string;
    name: string;
    version: string;
}>;

export type GradleProject = Readonly<{
    info: GradleItem;
    dependencies: ReadonlyArray<GradleItem>;
}>;

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    cwd: string;
    buildToolPath: string;
    applicationVersionTimestamp: AppVersionTimestamp;
}>;

const extractJson = (text: string): string => {
    const jsonStartIndex = text.indexOf('{');
    return text.substring(jsonStartIndex);
};

const readGradleProject = (
    runCommand: RunCommandType,
    cwd: string,
    buildToolPath: string
): taskEither.TaskEither<Error, GradleProject> => {
    console.log('Using gradle build tool to read project');
    return func.pipe(
        runCommand(`java -jar ${buildToolPath} ${cwd}`, {
            printOutput: false
        }),
        taskEither.map(extractJson),
        taskEither.chainEitherK((jsonString) =>
            parseJson<GradleProject>(jsonString)
        )
    );
};

export const getProjectInfo = (
    projectType: ProjectType,
    dependencies: Dependencies = {
        runCommand,
        cwd: process.cwd(),
        buildToolPath: '/files/craig-ci-gradle-tool.jar',
        applicationVersionTimestamp
    }
): taskEither.TaskEither<Error, ProjectInfo> =>
    func.pipe(
        readGradleProject(
            dependencies.runCommand,
            dependencies.cwd,
            dependencies.buildToolPath
        ),
        taskEither.map(
            (buildGradle): ProjectInfo => ({
                group: buildGradle.info.group,
                name: buildGradle.info.name,
                version: isApplication(projectType)
                    ? dependencies.applicationVersionTimestamp()
                    : buildGradle.info.version,
                versionType: isApplication(projectType)
                    ? 'Release'
                    : SNAPSHOT_VERSION_REGEX.test(buildGradle.info.version)
                      ? 'PreRelease'
                      : 'Release',
                repoType: 'polyrepo'
            })
        )
    );
