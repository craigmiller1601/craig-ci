import { test, expect } from 'vitest';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import path from 'path';
import { match, P } from 'ts-pattern';
import { getProjectType } from '../src/getProjectType';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

type Scenario =
    | 'GradleKotlinApplication'
    | 'GradleKotlinLibrary'
    | 'GradleGroovyApplication'
    | 'GradleGroovyLibrary'
    | 'Unknown';

const getWorkingDirForScenario = (scenario: Scenario): string =>
    match(scenario)
        .with('GradleGroovyApplication', () => 'gradleGroovyReleaseApplication')
        .with('GradleGroovyLibrary', () => 'gradleGroovyReleaseLibrary')
        .with('GradleKotlinApplication', () => 'gradleKotlinReleaseApplication')
        .with('GradleKotlinLibrary', () => 'gradleKotlinReleaseLibrary')
        .with('Unknown', () => '.')
        .exhaustive();

const getProjectTypeForScenario = (scenario: Scenario): ProjectType =>
    match<Scenario, ProjectType>(scenario)
        .with(
            P.union('GradleGroovyLibrary', 'GradleKotlinLibrary'),
            () => 'GradleLibrary'
        )
        .with(
            P.union('GradleKotlinApplication', 'GradleGroovyApplication'),
            () => 'GradleApplication'
        )
        .with('Unknown', () => 'Unknown')
        .exhaustive();

test.each<Scenario>([
    'GradleKotlinApplication',
    'GradleKotlinLibrary',
    'GradleGroovyApplication',
    'GradleGroovyLibrary',
    'Unknown'
])('Identifies project type %s', (scenario) => {
    const workingDirName = getWorkingDirForScenario(scenario);
    const workingDir = path.join(WORKING_DIR_ROOT, workingDirName);
    const projectType = getProjectTypeForScenario(scenario);

    const result = getProjectType(workingDir);
    if (projectType === 'Unknown') {
        expect(result).toBeLeft();
        return;
    }

    expect(result).toEqualRight(projectType);
});
