import { beforeEach, expect, MockedFunction, test, vi } from 'vitest';
import path from 'path';
import { match } from 'ts-pattern';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { getProjectInfo } from '../src/getProjectInfo';
import { RunCommandType } from '@craig-ci/context/cmd';
import fs from 'fs';
import { ProjectType } from '@craig-ci/context/types/ProjectType';
import { applicationVersionTimestamp } from '@craig-ci/context/date';
import { taskEither } from 'fp-ts';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);
const testDataDir = path.join(process.cwd(), 'test', '__gradle-output__');

type GetProjectIfoArgs = Readonly<{
    versionType: VersionType;
    projectType: ProjectType;
}>;

type VersionTypeValues = Readonly<{
    workingDir: string;
    version: string;
}>;

beforeEach(() => {
    vi.resetAllMocks();
});

const getBuildToolPath = (): string => {
    const parentDir = path.join(
        process.cwd(),
        '..',
        '..',
        'base-images',
        'java',
        'files'
    );
    const fileName = fs
        .readdirSync(parentDir)
        .filter((file) => file.startsWith('craig-ci-gradle-tool'))[0];
    return path.join(parentDir, fileName);
};

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

const getRunCommandReturnValue = (
    args: GetProjectIfoArgs
): taskEither.TaskEither<Error, string> => {
    const fileName = match(args)
        .with(
            { versionType: 'Release', projectType: 'GradleLibrary' },
            () => 'getInfo_Release_GradleLibrary.json'
        )
        .with(
            { versionType: 'PreRelease', projectType: 'GradleLibrary' },
            () => 'getInfo_PreRelease_GradleLibrary.json'
        )
        .with(
            { versionType: 'Release', projectType: 'GradleApplication' },
            () => 'getInfo_Release_GradleApplication.json'
        )
        .run();
    const data = fs.readFileSync(path.join(testDataDir, fileName), 'utf8');
    return taskEither.right(data);
};

test('Handles extra output during gradle initialization', async () => {
    const outputFile = path.join(
        testDataDir,
        'getInfo_PreRelease_GradleLibrary_ExtraOutput.txt'
    );
    const commandOutput = fs.readFileSync(outputFile, 'utf8');
    runCommandMock.mockImplementation(() => taskEither.right(commandOutput));

    const expectedProjectInfo: ProjectInfo = {
        group: 'io.craigmiller160',
        name: 'spring-keycloak-oauth2-resource-server',
        version: '1.1.0-SNAPSHOT',
        versionType: 'PreRelease',
        repoType: 'polyrepo'
    };
    const buildToolPath = getBuildToolPath();
    const now = new Date();
    const timestamp = applicationVersionTimestamp(() => now);
    const result = await getProjectInfo('GradleLibrary', {
        runCommand: runCommandMock,
        cwd: WORKING_DIR_ROOT,
        buildToolPath,
        applicationVersionTimestamp: () => timestamp
    })();
    expect(result).toEqualRight(expectedProjectInfo);
});

test.each<GetProjectIfoArgs>([
    { versionType: 'Release', projectType: 'GradleLibrary' },
    { versionType: 'PreRelease', projectType: 'GradleLibrary' },
    { versionType: 'Release', projectType: 'GradleApplication' }
])(
    'Gradle getProjectInfo for $versionType and $projectType',
    async ({ versionType, projectType }) => {
        runCommandMock.mockImplementation(() =>
            getRunCommandReturnValue({ versionType, projectType })
        );
        const now = new Date();
        const timestamp = applicationVersionTimestamp(() => now);
        const { workingDir, version } = match<
            GetProjectIfoArgs,
            VersionTypeValues
        >({ versionType, projectType })
            .with(
                { versionType: 'Release', projectType: 'GradleLibrary' },
                () => ({
                    workingDir: 'gradleKotlinReleaseLibrary',
                    version: '1.0.0'
                })
            )
            .with(
                { versionType: 'PreRelease', projectType: 'GradleLibrary' },
                () => ({
                    workingDir: 'gradleKotlinPreReleaseLibrary',
                    version: '1.0.0-SNAPSHOT'
                })
            )
            .with(
                { versionType: 'Release', projectType: 'GradleApplication' },
                () => ({
                    workingDir: 'gradleKotlinReleaseApplication',
                    version: timestamp
                })
            )
            .run();
        const fullWorkingDir = path.join(WORKING_DIR_ROOT, workingDir);

        const expectedProjectInfo: ProjectInfo = {
            group: 'io.craigmiller160',
            name: 'spring-gradle-playground',
            version,
            versionType,
            repoType: 'polyrepo'
        };
        const buildToolPath = getBuildToolPath();
        const result = await getProjectInfo(projectType, {
            runCommand: runCommandMock,
            cwd: fullWorkingDir,
            buildToolPath,
            applicationVersionTimestamp: () => timestamp
        })();
        expect(result).toEqualRight(expectedProjectInfo);
    }
);
