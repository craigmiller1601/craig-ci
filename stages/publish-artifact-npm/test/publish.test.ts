import { test, expect, vi, beforeEach, MockedFunction } from 'vitest';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { publish } from '../src/publish';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';

type Scenario = 'with-new-version' | 'no-new-version';

const NEW_VERSION = '1.0.0-beta.3';
const context: BuildContext = {
    projectType: 'NpmLibrary',
    projectInfo: {
        group: 'craigmiller160',
        name: 'my-project',
        version: '1.0.0',
        versionType: 'Release',
        repoType: 'polyrepo'
    }
};

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

test.each<Scenario>(['no-new-version', 'with-new-version'])(
    'Publishes artifact for scenario: %s',
    async (scenario) => {
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const newVersion =
            scenario === 'with-new-version' ? NEW_VERSION : undefined;
        const result = await publish(context, newVersion, runCommandMock)();
        expect(result).toBeRight();

        const version =
            scenario === 'with-new-version'
                ? NEW_VERSION
                : context.projectInfo.version;
        const command = `npm version --allow-same-version --no-git-tag-version ${version} && npm publish`;
        expect(runCommandMock).toHaveBeenCalledWith(command);
    }
);
