import { afterEach, beforeEach, expect, test } from 'vitest';
import path from 'path';
import { copyArtifacts } from '../src/copyArtifacts';
import fs from 'fs';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'npmPublishCopyArtifacts'
);

const clean = () =>
    fs
        .readdirSync(WORKING_DIR_ROOT)
        .filter((file) => file !== 'artifacts' && file !== '.gitignore')
        .forEach((file) =>
            fs.rmSync(path.join(WORKING_DIR_ROOT, file), {
                recursive: true,
                force: true
            })
        );

beforeEach(() => {
    clean();
});

afterEach(() => {
    clean();
});

test('Copies artifacts in preparation for publishing', () => {
    const result = copyArtifacts(WORKING_DIR_ROOT);
    expect(result).toBeRight();

    const worldFilePath = path.join(WORKING_DIR_ROOT, 'lib', 'world.js');
    expect(fs.existsSync(worldFilePath)).toBe(true);

    const helloFilePath = path.join(WORKING_DIR_ROOT, 'hello.js');
    expect(fs.existsSync(helloFilePath)).toBe(true);
});
