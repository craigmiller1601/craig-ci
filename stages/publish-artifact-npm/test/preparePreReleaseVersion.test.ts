import { beforeEach, MockedFunction, test, vi, expect } from 'vitest';
import {
    NexusSearchResult,
    NexusSearchResultItem
} from '@craig-ci/nexus-api/NexusSearchResult';
import { NexusRepoGroupSearchFn } from '@craig-ci/nexus-api';
import { taskEither } from 'fp-ts';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { preparePreReleaseVersion } from '../src/preparePreReleaseVersion';
import { match } from 'ts-pattern';

type Scenario = 'has-match' | 'no-match';

const createItem = (version: string): NexusSearchResultItem => ({
    name: '',
    group: '',
    format: '',
    repository: '',
    version,
    id: '',
    assets: []
});

beforeEach(() => {
    vi.resetAllMocks();
});

const searchForNpmBetasMock: MockedFunction<NexusRepoGroupSearchFn> = vi.fn();

test.each<Scenario>(['has-match', 'no-match'])(
    'preparePreReleaseVersion for NPM for scenario %s',
    async (scenario) => {
        const nexusResult: NexusSearchResult = {
            items: [createItem('1.0.0-beta.2')]
        };
        searchForNpmBetasMock.mockImplementation(() => {
            if (scenario === 'has-match') {
                return taskEither.right(nexusResult);
            }
            return taskEither.right({ items: [] });
        });

        const buildContext: BuildContext = {
            projectType: 'NpmApplication',
            projectInfo: {
                group: 'craigmiller160',
                name: 'my-project',
                versionType: 'PreRelease',
                version: '1.0.0-beta',
                repoType: 'polyrepo'
            }
        };

        const result = await preparePreReleaseVersion(
            buildContext,
            searchForNpmBetasMock
        )();

        const expectedVersion = match(scenario)
            .with('has-match', () => '1.0.0-beta.3')
            .with('no-match', () => '1.0.0-beta.1')
            .exhaustive();

        expect(result).toEqualRight(expectedVersion);
    }
);
