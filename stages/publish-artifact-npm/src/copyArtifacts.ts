import { either, function as func, readonlyArray } from 'fp-ts';
import path from 'path';
import { cpSync, readdirSync } from '@craig-ci/fp-io';

const copyAllFiles =
    (cwd: string, artifactsDir: string) =>
    (files: ReadonlyArray<string>): either.Either<Error, void> =>
        func.pipe(
            files,
            readonlyArray.map((file) => [
                path.join(artifactsDir, file),
                path.join(cwd, file)
            ]),
            readonlyArray.map(([src, dest]) =>
                cpSync(src, dest, {
                    recursive: true,
                    force: true
                })
            ),
            either.sequenceArray,
            either.map(() => func.constVoid())
        );

export const copyArtifacts = (
    cwd: string = process.cwd()
): either.Either<Error, void> => {
    console.log('Copying artifacts for publishing');
    const artifactsDir = path.join(cwd, 'artifacts');

    return func.pipe(
        readdirSync(artifactsDir),
        either.chain(copyAllFiles(cwd, artifactsDir))
    );
};
