import {
    taskEither,
    function as func,
    option,
    readonlyArray,
    either
} from 'fp-ts';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import {
    NexusRepoGroupSearchFn,
    searchForNpmBetas as defaultSearchForNpmBetas
} from '@craig-ci/nexus-api';
import { NexusSearchResult } from '@craig-ci/nexus-api/NexusSearchResult';

const BETA_VERSION_REGEX = /^(?<version>.*-beta)\.(?<betaNumber>\d*)$/;
type BetaRegexGroups = Readonly<{
    version: string;
    betaNumber: string;
}>;

const findMatchingVersion = (
    nexusResult: NexusSearchResult,
    version: string
): option.Option<string> =>
    func.pipe(
        nexusResult.items,
        readonlyArray.findFirst((_) => _.version.startsWith(version)),
        option.map((_) => _.version)
    );

const prepareVersionSearchParam = (version: string): string => {
    const formattedVersion = version.replaceAll('SNAPSHOT', '');
    return `${formattedVersion}*`;
};

const bumpBetaVersion = (fullVersion: string): either.Either<Error, string> => {
    const betaRegexGroups = BETA_VERSION_REGEX.exec(fullVersion)?.groups as
        | BetaRegexGroups
        | undefined;
    if (!betaRegexGroups) {
        return either.left(
            new Error(`Invalid beta version found in Nexus: ${fullVersion}`)
        );
    }

    return func.pipe(
        either.tryCatch(
            () => parseInt(betaRegexGroups.betaNumber) + 1,
            either.toError
        ),
        either.map(
            (newBetaNumber) => `${betaRegexGroups.version}.${newBetaNumber}`
        )
    );
};

export const preparePreReleaseVersion = (
    context: BuildContext,
    searchForNpmBetas: NexusRepoGroupSearchFn = defaultSearchForNpmBetas
): taskEither.TaskEither<Error, string> => {
    const versionSearchParam = prepareVersionSearchParam(
        context.projectInfo.version
    );
    return func.pipe(
        searchForNpmBetas(
            context.projectInfo.group,
            context.projectInfo.name,
            versionSearchParam
        ),
        taskEither.chainEitherK((nexusResult) =>
            func.pipe(
                findMatchingVersion(nexusResult, context.projectInfo.version),
                option.getOrElse(() => `${context.projectInfo.version}.0`),
                bumpBetaVersion
            )
        )
    );
};
