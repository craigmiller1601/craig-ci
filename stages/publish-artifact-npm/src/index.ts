import { function as func, taskEither } from 'fp-ts';
import { readContext } from '@craig-ci/context/io';
import { preparePreReleaseVersion } from './preparePreReleaseVersion';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { copyArtifacts } from './copyArtifacts';
import { publish } from './publish';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';

console.log('Publishing NPM artifact');

void func.pipe(
    readContext(),
    taskEither.fromEither,
    taskEither.bindTo('context'),
    taskEither.bind('newVersion', ({ context }) => {
        if (isRelease(context.projectInfo.versionType)) {
            return taskEither.right(undefined);
        }
        return preparePreReleaseVersion(context);
    }),
    taskEither.chainFirstEitherK(() => copyArtifacts()),
    taskEither.chain(({ context, newVersion }) => publish(context, newVersion)),
    terminateTaskEither
)();
