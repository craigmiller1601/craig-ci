import { BuildContext } from '@craig-ci/context/types/BuildContext';
import {
    RunCommandType,
    runCommand as defaultRunCommand
} from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';

export const getNpmPublishCommand = (version: string): string =>
    `npm version --allow-same-version --no-git-tag-version ${version} && npm publish`;

export const publish = (
    context: BuildContext,
    newVersion?: string,
    runCommand: RunCommandType = defaultRunCommand
): taskEither.TaskEither<Error, string> => {
    const version = newVersion ?? context.projectInfo.version;
    console.log(`Publishing artifact version ${version}`);

    const npmPublishCommand = getNpmPublishCommand(version);
    return runCommand(npmPublishCommand);
};
