import { function as func, taskEither } from 'fp-ts';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { cloneArgocdRepo } from './cloneArgocdRepo';
import { readContext } from '@craig-ci/context/io';
import { helmTemplateToProjectFile } from './helmTemplateToProjectFile';
import { gitCommitAndPush } from './gitCommitAndPush';
import { syncArgocd } from './syncArgocd';

console.log('Deploying application using argocd');

void func.pipe(
    cloneArgocdRepo(),
    taskEither.chainEitherK(() => readContext()),
    taskEither.chainFirst(helmTemplateToProjectFile),
    taskEither.bindTo('buildContext'),
    taskEither.bind('shouldSync', () => gitCommitAndPush()),
    taskEither.chain(({ shouldSync, buildContext }) => {
        if (shouldSync) {
            return syncArgocd(buildContext);
        }

        return taskEither.right('');
    }),
    terminateTaskEither
)();
