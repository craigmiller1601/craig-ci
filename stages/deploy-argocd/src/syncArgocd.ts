import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither, function as func } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import { match, P } from 'ts-pattern';

type ProcessEnv = typeof process.env;

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    env: ProcessEnv;
}>;

const LOGIN_COMMAND = `
argocd login \\
    argocd.craigmiller160.us \\
    --grpc-web \\
    --username $ARGOCD_USERNAME \\
    --password $ARGOCD_PASSWORD
`.trim();

const createSyncCommand = (appName: string): string =>
    `
argocd app \\
    sync \\
    ${appName} \\
    --grpc-web
`.trim();

const createWaitCommand = (appName: string): string =>
    `
argocd app \\
    wait \\
    ${appName} \\
    --timeout 300 \\
    --grpc-web \\
    --sync \\
    --health
`.trim();

const getAppName = (context: BuildContext, env: ProcessEnv): string =>
    match(env)
        .with(
            { K8S_DEPLOY_ENV: P.not(P.nullish) },
            ({ K8S_DEPLOY_ENV }) =>
                `${context.projectInfo.name}-${K8S_DEPLOY_ENV}`
        )
        .otherwise(() => context.projectInfo.name);

export const syncArgocd = (
    context: BuildContext,
    dependencies: Dependencies = {
        runCommand,
        env: process.env
    }
): taskEither.TaskEither<Error, string> => {
    const appName = getAppName(context, dependencies.env);
    return func.pipe(
        dependencies.runCommand(LOGIN_COMMAND),
        taskEither.chain(() =>
            dependencies.runCommand(createSyncCommand(appName))
        ),
        taskEither.chain(() =>
            dependencies.runCommand(createWaitCommand(appName))
        )
    );
};
