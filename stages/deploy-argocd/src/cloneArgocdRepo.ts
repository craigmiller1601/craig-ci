import { taskEither } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
}>;

export const cloneArgocdRepo = (
    dependencies: Dependencies = {
        runCommand
    }
): taskEither.TaskEither<Error, string> =>
    dependencies.runCommand(
        'git clone https://oauth2:$ACCESS_TOKEN@gitlab.com/craigmiller1601/craigmiller160-projects-argocd.git'
    );
