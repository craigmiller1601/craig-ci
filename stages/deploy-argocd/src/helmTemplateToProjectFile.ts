import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { either, taskEither, function as func, readonlyArray } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import { Chart, readHelmChart } from '@craig-ci/helm-support';
import { isHelm } from '@craig-ci/context/utils/projectTypeUtils';
import { existsSync, mkdirSync } from '@craig-ci/fp-io';

type ProcessEnv = typeof process.env;

type Dependencies = Readonly<{
    cwd: string;
    runCommand: RunCommandType;
    env: ProcessEnv;
}>;

type Env = Readonly<{
    K8S_NAMESPACE: string;
    K8S_DEPLOY_ENV?: string;
    HELM_EXTRA_VALUES?: string;
}>;

const setupProjectDir = (
    cwd: string,
    namespace: string,
    name: string,
    deployEnv?: string
): either.Either<Error, string> => {
    console.log('Setting up project directory');
    const argoDir = path.join(cwd, 'craigmiller160-projects-argocd');
    if (!existsSync(argoDir)) {
        return either.left(
            new Error(`Cannot find ArgoCD project directory: ${argoDir}`)
        );
    }

    const appName = deployEnv ? `${name}-prod` : name;
    const projectDir = path.join(
        argoDir,
        'namespaces',
        namespace,
        `${appName}`
    );
    if (!existsSync(projectDir)) {
        return func.pipe(
            mkdirSync(projectDir, {
                recursive: true
            }),
            either.map(() => projectDir)
        );
    }

    return either.right(projectDir);
};

const getEnv = (env: ProcessEnv): either.Either<Error, Env> => {
    if (!env.K8S_NAMESPACE) {
        return either.left(
            new Error('Cannot find K8S_NAMESPACE environment variable')
        );
    }

    return either.right({
        K8S_NAMESPACE: env.K8S_NAMESPACE,
        K8S_DEPLOY_ENV: env.K8S_DEPLOY_ENV,
        HELM_EXTRA_VALUES: env.HELM_EXTRA_VALUES
    });
};

const getImageValueKey = (chart: Chart): string => {
    const hasCraigApp = !!chart.dependencies?.find(
        (dependency) => dependency.name === 'craig_application'
    );

    if (hasCraigApp) {
        return 'craig_application.application.image';
    }

    return 'app_deployment.image';
};

const runHelmCommand = (
    context: BuildContext,
    env: Env,
    cwd: string,
    projectDir: string,
    runCommand: RunCommandType,
    chart: Chart
): taskEither.TaskEither<Error, string> => {
    const chartDir = path.join(cwd, 'deploy', 'chart');
    const appName = env.K8S_DEPLOY_ENV
        ? `${context.projectInfo.name}-${env.K8S_DEPLOY_ENV}`
        : context.projectInfo.name;

    const baseCommand = `
helm template \\
    -n ${env.K8S_NAMESPACE} \\
    ${appName} \\
    ${chartDir} \\
    --values ${path.join(chartDir, 'values.yaml')}
`.trim();

    const image = `nexus-docker.craigmiller160.us/${context.projectInfo.name}:${context.projectInfo.version}`;
    const key = getImageValueKey(chart);

    const appDeployImage = `
\\
    --set ${key}=${image}
`.trim();

    const secretArgs = `
\\
    ${env.HELM_EXTRA_VALUES}
`.trim();

    const commandEnding = `
\\
    > ${projectDir}/project.yaml
`.trim();

    const command = [
        baseCommand,
        !isHelm(context.projectType) ? appDeployImage : undefined,
        env.HELM_EXTRA_VALUES ? secretArgs : undefined,
        commandEnding
    ]
        .filter((part): part is string => !!part)
        .join(' ');
    return runCommand(command);
};

const prepareHelmDependencies = (
    cwd: string,
    chart: Chart,
    runCommand: RunCommandType
): taskEither.TaskEither<Error, string> => {
    const chartDir = path.join(cwd, 'deploy', 'chart');
    return func.pipe(
        chart.dependencies ?? [],
        readonlyArray.mapWithIndex((index, dep) =>
            runCommand(`helm repo add dep${index} ${dep.repository}`, {
                cwd: chartDir
            })
        ),
        taskEither.sequenceArray,
        taskEither.chain(() =>
            runCommand('helm dependency build', {
                cwd: chartDir
            })
        )
    );
};

export const helmTemplateToProjectFile = (
    context: BuildContext,
    dependencies: Dependencies = {
        cwd: process.cwd(),
        runCommand,
        env: process.env
    }
): taskEither.TaskEither<Error, string> => {
    console.log('Generating helm template as ArgoCD project file');
    return func.pipe(
        getEnv(dependencies.env),
        either.bindTo('env'),
        either.bind(
            'projectDir',
            ({ env: { K8S_NAMESPACE, K8S_DEPLOY_ENV } }) =>
                setupProjectDir(
                    dependencies.cwd,
                    K8S_NAMESPACE,
                    context.projectInfo.name,
                    K8S_DEPLOY_ENV
                )
        ),
        either.bind('chart', () => readHelmChart(dependencies.cwd)),
        taskEither.fromEither,
        taskEither.chainFirst(({ chart }) =>
            prepareHelmDependencies(
                dependencies.cwd,
                chart,
                dependencies.runCommand
            )
        ),
        taskEither.chain(({ env, projectDir, chart }) =>
            runHelmCommand(
                context,
                env,
                dependencies.cwd,
                projectDir,
                dependencies.runCommand,
                chart
            )
        )
    );
};
