import { taskEither, function as func } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
}>;

const getChangeCount = (
    cwd: string,
    runCommand: RunCommandType
): taskEither.TaskEither<Error, number> =>
    func.pipe(
        runCommand('git status --porcelain | wc -l', {
            printOutput: false,
            cwd
        }),
        taskEither.map((count) => parseInt(count.trim()))
    );

const doCommitAndPush = (
    cwd: string,
    runCommand: RunCommandType
): taskEither.TaskEither<Error, string> =>
    func.pipe(
        runCommand('git config user.email "craig-ci@gitlab.com"', {
            cwd
        }),
        taskEither.chain(() =>
            runCommand('git config user.name "craig-ci"', {
                cwd
            })
        ),
        taskEither.chain(() =>
            runCommand('git add . ', {
                cwd
            })
        ),
        taskEither.chain(() =>
            runCommand('git commit -m "craig-ci update to argocd project"', {
                cwd
            })
        ),
        taskEither.chain(() => runCommand('git push', { cwd }))
    );

export const gitCommitAndPush = (
    dependencies: Dependencies = {
        runCommand
    }
): taskEither.TaskEither<Error, boolean> => {
    console.log('Committing and pushing ArgocD project update');
    const cwd = path.join(process.cwd(), 'craigmiller160-projects-argocd');
    return func.pipe(
        getChangeCount(cwd, dependencies.runCommand),
        taskEither.chain((count) => {
            if (count === 0) {
                console.log('No changes to commit, skipping push to ArgoCD');
                return taskEither.right(false);
            }

            return func.pipe(
                doCommitAndPush(cwd, dependencies.runCommand),
                taskEither.map(() => true)
            );
        })
    );
};
