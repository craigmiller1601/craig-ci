import { beforeEach, MockedFunction, test, vi, expect } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import { gitCommitAndPush } from '../src/gitCommitAndPush';
import path from 'path';

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

beforeEach(() => {
    vi.resetAllMocks();
});

test('Does nothing when there are no changes', async () => {
    runCommandMock.mockImplementation(() => taskEither.right('  0  '));
    const cwd = path.join(process.cwd(), 'craigmiller160-projects-argocd');

    const result = await gitCommitAndPush({
        runCommand: runCommandMock
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(1);
    expect(runCommandMock).toHaveBeenNthCalledWith(
        1,
        'git status --porcelain | wc -l',
        { cwd, printOutput: false }
    );
});

test('Commits & pushes changes', async () => {
    runCommandMock.mockImplementation((cmd) => {
        if (cmd?.includes('status')) {
            return taskEither.right('   1   ');
        }
        return taskEither.right('');
    });
    const cwd = path.join(process.cwd(), 'craigmiller160-projects-argocd');

    const result = await gitCommitAndPush({
        runCommand: runCommandMock
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(6);
    expect(runCommandMock).toHaveBeenNthCalledWith(
        1,
        'git status --porcelain | wc -l',
        { cwd, printOutput: false }
    );
    expect(runCommandMock).toHaveBeenNthCalledWith(
        2,
        'git config user.email "craig-ci@gitlab.com"',
        { cwd }
    );
    expect(runCommandMock).toHaveBeenNthCalledWith(
        3,
        'git config user.name "craig-ci"',
        { cwd }
    );
    expect(runCommandMock).toHaveBeenNthCalledWith(4, 'git add . ', {
        cwd
    });
    expect(runCommandMock).toHaveBeenNthCalledWith(
        5,
        'git commit -m "craig-ci update to argocd project"',
        { cwd }
    );
    expect(runCommandMock).toHaveBeenNthCalledWith(6, 'git push', { cwd });
});
