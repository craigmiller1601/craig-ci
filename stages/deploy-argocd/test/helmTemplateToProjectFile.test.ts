import {
    test,
    vi,
    beforeEach,
    expect,
    MockedFunction,
    afterEach
} from 'vitest';
import path from 'path';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { applicationVersionTimestamp } from '@craig-ci/context/date';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import { helmTemplateToProjectFile } from '../src/helmTemplateToProjectFile';
import fs from 'fs';
import { match, P } from 'ts-pattern';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';

type HelmScenario =
    | 'standard'
    | 'with-app-deployment'
    | 'with-craig-app'
    | 'with-secrets'
    | 'with-deploy-env';
type ProcessEnv = typeof process.env;

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);
const APP_NAME = 'project';
const APP_VERSION = applicationVersionTimestamp();
const NAMESPACE = 'helm-test';
const BASE_WORKING_DIR = path.join(
    WORKING_DIR_ROOT,
    'helmTemplateToProjectFile'
);
const APP_DEPLOYMENT_WORKING_DIR = path.join(
    WORKING_DIR_ROOT,
    'helmTemplateToProjectFile_appDeployment'
);
const CRAIG_APP_WORKING_DIR = path.join(
    WORKING_DIR_ROOT,
    'helmTemplateToProjectFile_craigApp'
);
const getChartDir = (root: string) => path.join(root, 'deploy', 'chart');
const getArgoDir = (root: string) =>
    path.join(root, 'craigmiller160-projects-argocd');
const getArgoProjectDir = (argoDir: string) =>
    path.join(argoDir, 'namespaces', NAMESPACE, APP_NAME);
const getArgoProjectProdDir = (argoDir: string) =>
    path.join(argoDir, 'namespaces', NAMESPACE, `${APP_NAME}-prod`);

const createContext = (scenario: HelmScenario): BuildContext => ({
    projectType:
        scenario === 'with-app-deployment' || scenario === 'with-craig-app'
            ? 'MavenApplication'
            : 'HelmApplication',
    projectInfo: {
        group: 'us.craigmiller160',
        name: APP_NAME,
        version: APP_VERSION,
        repoType: 'polyrepo',
        versionType: 'Release'
    }
});

const clean = (root: string) =>
    fs
        .readdirSync(getArgoDir(root))
        .filter((file) => '.gitkeep' !== file)
        .forEach((file) =>
            fs.rmSync(path.join(getArgoDir(root), file), {
                recursive: true,
                force: true
            })
        );

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();
beforeEach(() => {
    vi.resetAllMocks();
    clean(BASE_WORKING_DIR);
    clean(CRAIG_APP_WORKING_DIR);
    clean(APP_DEPLOYMENT_WORKING_DIR);
});

afterEach(() => {
    clean(BASE_WORKING_DIR);
    clean(CRAIG_APP_WORKING_DIR);
    clean(APP_DEPLOYMENT_WORKING_DIR);
});

const getEnvForScenario = (scenario: HelmScenario): ProcessEnv => {
    if (
        scenario === 'standard' ||
        scenario === 'with-app-deployment' ||
        scenario === 'with-craig-app'
    ) {
        return {
            K8S_NAMESPACE: NAMESPACE
        };
    }

    if (scenario === 'with-deploy-env') {
        return {
            K8S_NAMESPACE: NAMESPACE,
            K8S_DEPLOY_ENV: 'prod'
        };
    }

    return {
        K8S_NAMESPACE: NAMESPACE,
        HELM_EXTRA_VALUES: '--set other-value=$OTHER_VALUE'
    };
};

const createBaseCommand = (root: string, appName: string): string =>
    `
helm template \\
    -n ${NAMESPACE} \\
    ${appName} \\
    ${getChartDir(root)} \\
    --values ${path.join(getChartDir(root), 'values.yaml')}
`.trim();

const SECRET_ARGS = `
\\
    --set other-value=$OTHER_VALUE
`.trim();

const createCommandEnding = (projectDir: string): string =>
    `
\\
    > ${projectDir}/project.yaml
`.trim();

const createCommandImage = (projectInfo: ProjectInfo): string =>
    `
\\
    --set app_deployment.image=nexus-docker.craigmiller160.us/${projectInfo.name}:${projectInfo.version}
`.trim();
const createCraigAppCommandImage = (projectInfo: ProjectInfo): string =>
    `
\\
    --set craig_application.application.image=nexus-docker.craigmiller160.us/${projectInfo.name}:${projectInfo.version}
`.trim();

const getRootDirForScenario = (scenario: HelmScenario): string =>
    match(scenario)
        .with('with-app-deployment', () => APP_DEPLOYMENT_WORKING_DIR)
        .with('with-craig-app', () => CRAIG_APP_WORKING_DIR)
        .otherwise(() => BASE_WORKING_DIR);

const getDependencyUrl = (scenario: HelmScenario): string =>
    match(scenario)
        .with(
            P.union('with-craig-app', 'with-app-deployment'),
            () => 'https://nexus.craigmiller160.us/repository/helm-private'
        )
        .otherwise(() => 'https://prometheus-community.github.io/helm-charts');

test.each<HelmScenario>([
    'standard',
    'with-app-deployment',
    'with-craig-app',
    'with-secrets',
    'with-deploy-env'
])(
    'Generates helm template and writes to project file for scenario %s',
    async (scenario) => {
        const root = getRootDirForScenario(scenario);
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const env = getEnvForScenario(scenario);
        const context = createContext(scenario);
        const result = await helmTemplateToProjectFile(context, {
            cwd: root,
            env,
            runCommand: runCommandMock
        })();
        expect(result).toBeRight();

        const argoDir = getArgoDir(root);

        let projectDir = getArgoProjectDir(argoDir);
        if (scenario === 'with-deploy-env') {
            projectDir = getArgoProjectProdDir(argoDir);
        }

        expect(fs.existsSync(projectDir)).toBe(true);
        const argoProjectDirStats = fs.statSync(projectDir);
        expect(argoProjectDirStats.isDirectory()).toBe(true);

        const command = match(scenario)
            .with(
                'standard',
                () =>
                    `${createBaseCommand(root, APP_NAME)} ${createCommandEnding(projectDir)}`
            )
            .with(
                'with-app-deployment',
                () =>
                    `${createBaseCommand(root, APP_NAME)} ${createCommandImage(context.projectInfo)} ${createCommandEnding(projectDir)}`
            )
            .with(
                'with-craig-app',
                () =>
                    `${createBaseCommand(root, APP_NAME)} ${createCraigAppCommandImage(context.projectInfo)} ${createCommandEnding(projectDir)}`
            )
            .with(
                'with-secrets',
                () =>
                    `${createBaseCommand(root, APP_NAME)} ${SECRET_ARGS} ${createCommandEnding(projectDir)}`
            )
            .with(
                'with-deploy-env',
                () =>
                    `${createBaseCommand(root, `${APP_NAME}-prod`)} ${createCommandEnding(projectDir)}`
            )
            .exhaustive();

        const chartDir = path.join(root, 'deploy', 'chart');

        const dependencyUrl = getDependencyUrl(scenario);

        expect(runCommandMock).toHaveBeenCalledTimes(3);
        expect(runCommandMock).toHaveBeenNthCalledWith(
            1,
            `helm repo add dep0 ${dependencyUrl}`,
            { cwd: chartDir }
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(
            2,
            'helm dependency build',
            { cwd: chartDir }
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(3, command);
    }
);

test('Cannot find ArgoCD directory', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const argoDir = path.join(process.cwd(), 'craigmiller160-projects-argocd');
    const context = createContext('standard');
    const result = await helmTemplateToProjectFile(context, {
        cwd: process.cwd(),
        env: getEnvForScenario('standard'),
        runCommand: runCommandMock
    })();
    expect(result).toEqualLeft(
        new Error(`Cannot find ArgoCD project directory: ${argoDir}`)
    );
});

test('Cannot find namespace env', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const context = createContext('standard');
    const result = await helmTemplateToProjectFile(context, {
        cwd: process.cwd(),
        env: {},
        runCommand: runCommandMock
    })();
    expect(result).toEqualLeft(
        new Error(`Cannot find K8S_NAMESPACE environment variable`)
    );
});
