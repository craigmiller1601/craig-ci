import { test, beforeEach, vi, expect, MockedFunction } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import { syncArgocd } from '../src/syncArgocd';
import { applicationVersionTimestamp } from '@craig-ci/context/date';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import process from 'node:process';
import { match } from 'ts-pattern';

type ProcessEnv = typeof process.env;
type ArgoScenario = 'standard' | 'with-deploy-env';
const APP_NAME = 'project';
const APP_VERSION = applicationVersionTimestamp();

const context: BuildContext = {
    projectType: 'MavenApplication',
    projectInfo: {
        group: 'us.craigmiller160',
        name: APP_NAME,
        version: APP_VERSION,
        repoType: 'polyrepo',
        versionType: 'Release'
    }
};

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

beforeEach(() => {
    vi.resetAllMocks();
});

const getEnvForScenario = (scenario: ArgoScenario): ProcessEnv => {
    if (scenario === 'with-deploy-env') {
        return {
            K8S_DEPLOY_ENV: 'prod'
        };
    }

    return {};
};

const LOGIN_COMMAND = `
argocd login \\
    argocd.craigmiller160.us \\
    --grpc-web \\
    --username $ARGOCD_USERNAME \\
    --password $ARGOCD_PASSWORD
`.trim();

const createSyncCommand = (appName: string): string =>
    `
argocd app \\
    sync \\
    ${appName} \\
    --grpc-web
`.trim();

const createWaitCommand = (appName: string): string =>
    `
argocd app \\
    wait \\
    ${appName} \\
    --timeout 300 \\
    --grpc-web \\
    --sync \\
    --health
`.trim();

test.each<ArgoScenario>(['standard', 'with-deploy-env'])(
    'Syncs ArgoCD and waits on result for scenario %s',
    async (scenario) => {
        runCommandMock.mockImplementation(() => taskEither.right(''));
        const env = getEnvForScenario(scenario);
        const result = await syncArgocd(context, {
            runCommand: runCommandMock,
            env
        })();
        expect(result).toBeRight();

        const appName = match(scenario)
            .with('standard', () => APP_NAME)
            .with('with-deploy-env', () => `${APP_NAME}-prod`)
            .exhaustive();

        expect(runCommandMock).toHaveBeenCalledTimes(3);
        expect(runCommandMock).toHaveBeenNthCalledWith(1, LOGIN_COMMAND);
        expect(runCommandMock).toHaveBeenNthCalledWith(
            2,
            createSyncCommand(appName)
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(
            3,
            createWaitCommand(appName)
        );
    }
);
