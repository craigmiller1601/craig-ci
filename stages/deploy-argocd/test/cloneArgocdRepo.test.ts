import { beforeEach, MockedFunction, test, vi, expect } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import { cloneArgocdRepo } from '../src/cloneArgocdRepo';

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

beforeEach(() => {
    vi.resetAllMocks();
});

test('clones argocd repo', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));

    const result = await cloneArgocdRepo({
        runCommand: runCommandMock
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledWith(
        'git clone https://oauth2:$ACCESS_TOKEN@gitlab.com/craigmiller1601/craigmiller160-projects-argocd.git'
    );
});
