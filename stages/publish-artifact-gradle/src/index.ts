import { function as func, taskEither } from 'fp-ts';
import { copyArtifacts } from './copyArtifacts';
import { publish } from './publish';
import { terminateTaskEither } from '@craig-ci/context/terminate';

console.log('Publishing gradle artifact');

void func.pipe(
    copyArtifacts(),
    taskEither.fromEither,
    taskEither.chain(() => publish()),
    terminateTaskEither
)();
