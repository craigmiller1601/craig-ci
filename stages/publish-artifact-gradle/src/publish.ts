import { taskEither, function as func, string, readonlyArray } from 'fp-ts';
import {
    RunCommandType,
    runCommand as defaultRunCommand
} from '@craig-ci/context/cmd';

const ALLOWED_TASKS_REGEX = /(generate|publish).*$/;

const getTasksToSkip = (dryRunOutput: string): ReadonlyArray<string> =>
    func.pipe(
        dryRunOutput,
        string.split('\n'),
        readonlyArray.map(string.trim),
        readonlyArray.filter(string.startsWith(':')),
        readonlyArray.map(string.replace(/SKIPPED$/, '')),
        readonlyArray.map(string.replace(/^:/, '')),
        readonlyArray.map(string.trim),
        readonlyArray.filter((task) => !ALLOWED_TASKS_REGEX.test(task))
    );

const generatePublishCommand = (dryRunOutput: string): string => {
    const skipTasks = getTasksToSkip(dryRunOutput)
        .map((task) => `-x ${task}`)
        .join(' ');
    return `gradle publish ${skipTasks}`;
};

export const publish = (
    runCommand: RunCommandType = defaultRunCommand
): taskEither.TaskEither<Error, string> =>
    func.pipe(
        runCommand('gradle publish --dry-run'),
        taskEither.map(generatePublishCommand),
        taskEither.chain(runCommand)
    );
