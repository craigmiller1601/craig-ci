import { either, function as func, readonlyArray } from 'fp-ts';
import path from 'path';
import { cpSync, existsSync, mkdirSync, readdirSync } from '@craig-ci/fp-io';

const getAllArtifactPaths = (
    artifactsDir: string
): either.Either<Error, ReadonlyArray<string>> =>
    func.pipe(
        readdirSync(artifactsDir),
        either.map(
            func.flow(
                readonlyArray.filter((file) => file.endsWith('.jar')),
                readonlyArray.map((file) => path.join(artifactsDir, file))
            )
        )
    );

const ensureLibsDir = (libsDir: string): either.Either<Error, void> => {
    if (existsSync(libsDir)) {
        return either.right(func.constVoid());
    }

    return mkdirSync(libsDir, {
        recursive: true
    });
};

export const copyArtifacts = (
    cwd: string = process.cwd()
): either.Either<Error, void> => {
    const artifactsDir = path.join(cwd, 'artifacts');
    const libsDir = path.join(cwd, 'build', 'libs');

    return func.pipe(
        ensureLibsDir(libsDir),
        either.chain(() => getAllArtifactPaths(artifactsDir)),
        either.chain(
            func.flow(
                readonlyArray.map((jar) =>
                    cpSync(jar, path.join(libsDir, path.basename(jar)))
                ),
                either.sequenceArray
            )
        ),
        either.map(() => func.constVoid())
    );
};
