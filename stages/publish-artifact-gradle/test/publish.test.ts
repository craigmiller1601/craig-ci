import { beforeEach, expect, MockedFunction, test, vi } from 'vitest';
import { RunCommandType } from '@craig-ci/context/cmd';
import { publish } from '../src/publish';
import { taskEither } from 'fp-ts';
import path from 'path';
import fs from 'fs';

beforeEach(() => {
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();
const dryRunPath = path.join(__dirname, '__gradle-output__', 'dryRun.txt');
const dryRunOutput = fs.readFileSync(dryRunPath, 'utf8');

test('publishes the artifact', async () => {
    runCommandMock.mockImplementation((cmd) => {
        if (cmd?.endsWith('--dry-run')) {
            return taskEither.right(dryRunOutput);
        }
        return taskEither.right('');
    });
    const result = await publish(runCommandMock)();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledTimes(2);
    expect(runCommandMock).toHaveBeenNthCalledWith(
        1,
        'gradle publish --dry-run'
    );
    expect(runCommandMock).toHaveBeenNthCalledWith(
        2,
        'gradle publish -x compileKotlin -x compileJava -x pluginDescriptors -x processResources -x classes -x jar -x inspectClassesForKotlinIC'
    );
});
