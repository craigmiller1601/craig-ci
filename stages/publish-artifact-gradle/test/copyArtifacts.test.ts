import { afterEach, beforeEach, test, expect } from 'vitest';
import path from 'path';
import fs from 'fs';
import { copyArtifacts } from '../src/copyArtifacts';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'publishArtifactGradle'
);

const clean = () =>
    fs
        .readdirSync(WORKING_DIR_ROOT)
        .filter((file) => file !== 'artifacts')
        .forEach((file) =>
            fs.rmSync(path.join(WORKING_DIR_ROOT, file), {
                recursive: true,
                force: true
            })
        );

beforeEach(() => {
    clean();
});

afterEach(() => {
    clean();
});

test('Copies jars to staging', () => {
    const result = copyArtifacts(WORKING_DIR_ROOT);
    expect(result).toBeRight();

    const outputPath = path.join(
        WORKING_DIR_ROOT,
        'build',
        'libs',
        'hello.jar'
    );
    expect(fs.existsSync(outputPath)).toBe(true);
});
