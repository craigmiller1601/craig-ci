import { either, function as func, readonlyArray, taskEither } from 'fp-ts';
import { runCommand, RunCommandType } from '@craig-ci/context/cmd';
import path from 'path';
import { cpSync, readdirSync, statSync } from '@craig-ci/fp-io';

type Dependencies = Readonly<{
    runCommand: RunCommandType;
    cwd: string;
}>;

const getArtifactFiles = (
    cwd: string
): either.Either<Error, ReadonlyArray<string>> => {
    const targetDir = path.join(cwd, 'target');
    return func.pipe(
        readdirSync(targetDir),
        either.chain(
            func.flow(
                readonlyArray.map((file) =>
                    func.pipe(
                        statSync(path.join(targetDir, file)),
                        either.map((stats): [string, boolean] => [
                            file,
                            stats.isFile()
                        ])
                    )
                ),
                either.sequenceArray
            )
        ),
        either.map(
            func.flow(
                readonlyArray.filter(([, isFile]) => isFile),
                readonlyArray.map(([file]) => file)
            )
        )
    );
};

const copyArtifactFiles =
    (cwd: string) =>
    (files: ReadonlyArray<string>): either.Either<Error, void> => {
        const targetDir = path.join(cwd, 'target');
        const artifactsDir = path.join(cwd, 'artifacts');
        return func.pipe(
            files,
            readonlyArray.map((file) => [
                path.join(targetDir, file),
                path.join(artifactsDir, file)
            ]),
            readonlyArray.map(([src, dest]) => cpSync(src, dest)),
            either.sequenceArray,
            either.map(() => func.constVoid())
        );
    };

const copyPomXml = (cwd: string): either.Either<Error, void> =>
    cpSync(path.join(cwd, 'pom.xml'), path.join(cwd, 'artifacts', 'pom.xml'));

const copyFiles = (cwd: string): either.Either<Error, void> =>
    func.pipe(
        getArtifactFiles(cwd),
        either.chain(copyArtifactFiles(cwd)),
        either.chain(() => copyPomXml(cwd))
    );

export const buildAndTestArtifact = (
    dependencies: Dependencies = {
        runCommand,
        cwd: process.cwd()
    }
): taskEither.TaskEither<Error, unknown> =>
    func.pipe(
        dependencies.runCommand(
            `mvn clean verify -s \${MAVEN_HOME_OVERRIDE}/settings.xml`,
            {
                returnOutput: false
            }
        ),
        taskEither.chainEitherK(() => copyFiles(dependencies.cwd))
    );
