import { function as func, taskEither } from 'fp-ts';
import { terminateTaskEither } from '@craig-ci/context/terminate';
import { buildAndTestArtifact } from './buildAndTestArtifact';
import { prepareGpg } from './prepareGpg';

console.log('Building maven artifact');

void func.pipe(
    prepareGpg(),
    taskEither.chain(() => buildAndTestArtifact()),
    terminateTaskEither
)();
