import { either, taskEither, function as func } from 'fp-ts';
import { writeFileSync } from '@craig-ci/fp-io';
import path from 'path';
import {
    runCommand as defaultRunCommand,
    RunCommandType
} from '@craig-ci/context/cmd';

type Dependencies = Readonly<{
    cwd: string;
    env: NodeJS.ProcessEnv;
    runCommand: RunCommandType;
}>;

const getPublicKeyPath = (cwd: string): string => path.join(cwd, 'public.asc');
const getPrivateKeyPath = (cwd: string): string =>
    path.join(cwd, 'private.asc');

const writeKeys = (
    cwd: string,
    env: NodeJS.ProcessEnv
): either.Either<Error, void> => {
    const publicKeyPath = getPublicKeyPath(cwd);
    const privateKeyPath = getPrivateKeyPath(cwd);

    const {
        CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64,
        CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64
    } = env;

    if (!CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64) {
        return either.left(
            new Error('Missing CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64 variable')
        );
    }

    if (!CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64) {
        return either.left(
            new Error('Missing CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64 variable')
        );
    }

    return func.pipe(
        writeFileSync(
            publicKeyPath,
            atob(CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64)
        ),
        either.chain(() =>
            writeFileSync(
                privateKeyPath,
                atob(CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64)
            )
        )
    );
};

const importPgpKey = (
    cwd: string,
    env: NodeJS.ProcessEnv,
    runCommand: RunCommandType
): taskEither.TaskEither<Error, void> => {
    const publicKeyPath = getPublicKeyPath(cwd);
    const privateKeyPath = getPrivateKeyPath(cwd);
    const { CRAIGMILLER160_GPG_KEY_PASSWORD } = env;

    if (!CRAIGMILLER160_GPG_KEY_PASSWORD) {
        return taskEither.left(
            new Error('Missing CRAIGMILLER160_GPG_KEY_PASSWORD variable')
        );
    }

    return func.pipe(
        runCommand(`gpg --import ${publicKeyPath}`),
        taskEither.chain(() =>
            runCommand(
                `gpg --batch --yes --import --passphrase '${CRAIGMILLER160_GPG_KEY_PASSWORD}' ${privateKeyPath}`
            )
        ),
        taskEither.map(() => func.constVoid())
    );
};

export const prepareGpg = (
    dependencies: Dependencies = {
        cwd: process.cwd(),
        env: process.env,
        runCommand: defaultRunCommand
    }
): taskEither.TaskEither<Error, unknown> => {
    if (dependencies.env.SIGN_ARTIFACT !== 'true') {
        return taskEither.right(undefined);
    }

    return func.pipe(
        writeKeys(dependencies.cwd, dependencies.env),
        taskEither.fromEither,
        taskEither.chain(() =>
            importPgpKey(
                dependencies.cwd,
                dependencies.env,
                dependencies.runCommand
            )
        )
    );
};
