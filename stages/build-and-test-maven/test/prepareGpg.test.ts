import {
    test,
    expect,
    beforeEach,
    afterEach,
    MockedFunction,
    vi
} from 'vitest';
import { prepareGpg } from '../src/prepareGpg';
import path from 'path';
import fs from 'fs';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import { match } from 'ts-pattern';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'prepareGpg'
);

type SuccessScenario = 'enabled' | 'disabled';
type FailScenario =
    | 'missing-public-key'
    | 'missing-private-key'
    | 'missing-password';
const CRAIGMILLER160_GPG_PUBLIC_KEY = 'PUBLIC_KEY';
const CRAIGMILLER160_GPG_PRIVATE_KEY = 'PRIVATE_KEY';
const CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64 = btoa(
    CRAIGMILLER160_GPG_PUBLIC_KEY
);
const CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64 = btoa(
    CRAIGMILLER160_GPG_PRIVATE_KEY
);
const CRAIGMILLER160_GPG_KEY_PASSWORD = 'CRAIGMILLER160_GPG_KEY_PASSWORD';
const PGP_KEY_ID = '1234567890';

const clean = () =>
    fs
        .readdirSync(WORKING_DIR_ROOT)
        .filter((file) => file !== '.gitkeep')
        .map((file) => path.join(WORKING_DIR_ROOT, file))
        .forEach((file) => fs.rmSync(file));

beforeEach(() => {
    clean();
    vi.resetAllMocks();
});

afterEach(() => {
    clean();
    vi.resetAllMocks();
});

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

test.each<FailScenario>([
    'missing-password',
    'missing-public-key',
    'missing-private-key'
])('Fails to prepare gpg because %s', async (scenario) => {
    const result = await prepareGpg({
        env: {
            SIGN_ARTIFACT: 'true',
            CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64:
                scenario !== 'missing-public-key'
                    ? CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64
                    : undefined,
            CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64:
                scenario !== 'missing-private-key'
                    ? CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64
                    : undefined,
            CRAIGMILLER160_GPG_KEY_PASSWORD:
                scenario !== 'missing-password'
                    ? CRAIGMILLER160_GPG_KEY_PASSWORD
                    : undefined
        },
        cwd: WORKING_DIR_ROOT,
        runCommand: runCommandMock
    })();

    const errorMessage = match<FailScenario, string>(scenario)
        .with(
            'missing-public-key',
            () => 'Missing CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64 variable'
        )
        .with(
            'missing-private-key',
            () => 'Missing CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64 variable'
        )
        .with(
            'missing-password',
            () => 'Missing CRAIGMILLER160_GPG_KEY_PASSWORD variable'
        )
        .exhaustive();

    expect(result).toEqualLeft(new Error(errorMessage));
});

test.each<SuccessScenario>(['enabled', 'disabled'])(
    'Prepare gpg with the feature %s',
    async (scenario) => {
        runCommandMock.mockImplementation((command) => {
            if (command?.includes('--list-keys')) {
                return taskEither.right(PGP_KEY_ID);
            }
            return taskEither.right('');
        });

        const result = await prepareGpg({
            env: {
                SIGN_ARTIFACT: scenario === 'enabled' ? 'true' : 'false',
                CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64,
                CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64,
                CRAIGMILLER160_GPG_KEY_PASSWORD
            },
            cwd: WORKING_DIR_ROOT,
            runCommand: runCommandMock
        })();
        expect(result).toBeRight();

        const publicKeyPath = path.join(WORKING_DIR_ROOT, 'public.asc');
        const privateKeyPath = path.join(WORKING_DIR_ROOT, 'private.asc');

        if (scenario == 'disabled') {
            expect(fs.existsSync(publicKeyPath)).toBe(false);
            expect(fs.existsSync(privateKeyPath)).toBe(false);
            expect(runCommandMock).not.toHaveBeenCalled();
            return;
        }

        expect(fs.existsSync(publicKeyPath)).toBe(true);
        expect(fs.existsSync(privateKeyPath)).toBe(true);

        expect(fs.readFileSync(publicKeyPath, 'utf8')).toEqual(
            CRAIGMILLER160_GPG_PUBLIC_KEY
        );
        expect(fs.readFileSync(privateKeyPath, 'utf8')).toEqual(
            CRAIGMILLER160_GPG_PRIVATE_KEY
        );

        expect(runCommandMock).toHaveBeenCalledTimes(2);
        expect(runCommandMock).toHaveBeenNthCalledWith(
            1,
            `gpg --import ${publicKeyPath}`
        );
        expect(runCommandMock).toHaveBeenNthCalledWith(
            2,
            `gpg --batch --yes --import --passphrase '${CRAIGMILLER160_GPG_KEY_PASSWORD}' ${privateKeyPath}`
        );
    }
);
