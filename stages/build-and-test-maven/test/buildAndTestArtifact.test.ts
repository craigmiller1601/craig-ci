import {
    afterEach,
    beforeEach,
    expect,
    MockedFunction,
    test,
    vi
} from 'vitest';
import { buildAndTestArtifact } from '../src/buildAndTestArtifact';
import { RunCommandType } from '@craig-ci/context/cmd';
import { taskEither } from 'fp-ts';
import path from 'path';
import fs from 'fs';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__',
    'copyMavenArtifacts'
);

const ARTIFACTS_DIR = path.join(WORKING_DIR_ROOT, 'artifacts');

const clean = () =>
    fs.rmSync(ARTIFACTS_DIR, {
        recursive: true,
        force: true
    });

const runCommandMock: MockedFunction<RunCommandType> = vi.fn();

beforeEach(() => {
    vi.resetAllMocks();
    clean();
});

afterEach(() => {
    clean();
});

test('builds maven artifact', async () => {
    runCommandMock.mockImplementation(() => taskEither.right(''));
    const result = await buildAndTestArtifact({
        runCommand: runCommandMock,
        cwd: WORKING_DIR_ROOT
    })();
    expect(result).toBeRight();

    expect(runCommandMock).toHaveBeenCalledWith(
        `mvn clean verify -s \${MAVEN_HOME_OVERRIDE}/settings.xml`,
        {
            returnOutput: false
        }
    );

    expect(fs.existsSync(ARTIFACTS_DIR)).toBe(true);
    const files = fs.readdirSync(ARTIFACTS_DIR);
    expect(files).toEqual(['hello.jar', 'other.txt', 'pom.xml']);
});
