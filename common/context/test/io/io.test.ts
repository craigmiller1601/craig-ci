import { afterEach, beforeEach, test, expect } from 'vitest';
import fs from 'fs';
import path from 'path';
import { BuildContext } from '../../src/types/BuildContext';
import { readContext, writeContext } from '../../src/io';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '..',
    '__working_directories__',
    'context-io'
);

const clean = () =>
    fs
        .readdirSync(WORKING_DIR_ROOT)
        .filter((file) => '.gitkeep' !== file)
        .map((file) => path.join(WORKING_DIR_ROOT, file))
        .forEach((file) => fs.rmSync(file));

beforeEach(() => {
    clean();
});

afterEach(() => {
    clean();
});

const context: BuildContext = {
    projectType: 'NpmApplication',
    projectInfo: {
        group: 'us.craigmiller160',
        name: 'something',
        version: '1.0.0',
        versionType: 'Release',
        repoType: 'polyrepo'
    }
};
const contextJson: string = JSON.stringify(context, null, 2);
const contextFile = path.join(WORKING_DIR_ROOT, 'build-context.json');

test('reads build context', () => {
    fs.writeFileSync(contextFile, contextJson);

    const result = readContext(WORKING_DIR_ROOT);
    expect(result).toEqualRight(context);
});

test('writes build context', () => {
    const result = writeContext(context, WORKING_DIR_ROOT);
    expect(result).toBeRight();

    const json = fs.readFileSync(contextFile, 'utf8');
    expect(json).toEqual(contextJson);
});
