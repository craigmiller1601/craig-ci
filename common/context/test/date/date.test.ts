import { test, expect } from 'vitest';
import {
    applicationVersionTimestamp,
    parseApplicationVersionTimestamp,
    TIME_ZONE,
    TIMESTAMP_FORMAT
} from '../../src/date';
import { formatInTimeZone } from 'date-fns-tz';

test('generates the application version timestamp', () => {
    const now = new Date();
    const result = applicationVersionTimestamp(() => now);
    const expected = formatInTimeZone(now, TIME_ZONE, TIMESTAMP_FORMAT);
    expect(result).toEqual(expected);
});

test('can parse application version timestamp', () => {
    const now = new Date();
    const timestamp = applicationVersionTimestamp(() => now);
    const result = parseApplicationVersionTimestamp(timestamp);
    expect(result).toEqual(now);
});

test('handles parsing an invalid date', () => {
    const result = parseApplicationVersionTimestamp('abcdefg');
    expect(result).toBeUndefined();
});
