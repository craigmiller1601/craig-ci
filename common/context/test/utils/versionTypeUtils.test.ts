import { describe, expect, it } from 'vitest';
import { isPreRelease, isRelease } from '../../src/utils/versionTypeUtils';

describe('projectInfoUtils', () => {
    it('isRelease', () => {
        expect(isRelease('Release')).toEqual(true);
        expect(isRelease('PreRelease')).toEqual(false);
        expect(isRelease('Unknown')).toEqual(false);
    });

    it('isPreRelease', () => {
        expect(isPreRelease('PreRelease')).toEqual(true);
        expect(isPreRelease('Release')).toEqual(false);
        expect(isRelease('Unknown')).toEqual(false);
    });
});
