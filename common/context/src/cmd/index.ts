import spawn from 'cross-spawn';
import { taskEither, either } from 'fp-ts';
import { match } from 'ts-pattern';
import { getCwd } from './getCwd';

type Variables = { [key: string]: string };

export type CommandOptions = Readonly<{
    printOutput: boolean;
    returnOutput: boolean;
    cwd: string;
    variables: Variables;
    env: object;
    shell: boolean;
}>;

const formatCommand = (command: string, variables: Variables): string =>
    Object.entries(variables).reduce(
        (newCmd, [key, value]) => newCmd.replaceAll(`\${${key}}`, value),
        command
    );

export const runCommand = (
    command: string,
    options?: Partial<CommandOptions>
): taskEither.TaskEither<Error, string> => {
    const printOutput = options?.printOutput ?? true;
    const returnOutput = options?.returnOutput ?? true;
    const cwd = options?.cwd ?? getCwd();
    const variables = options?.variables ?? {};

    console.log(`Command: ${command}`);
    const formattedCommand = formatCommand(command, variables);

    return taskEither.tryCatch(
        () =>
            new Promise((resolve, reject) => {
                const childProcess = spawn('sh', ['-c', formattedCommand], {
                    cwd,
                    env: {
                        ...process.env,
                        ...(options?.env ?? {})
                    },
                    shell: options?.shell
                });
                let fullOutput = '';
                childProcess.stdout?.on('data', (data: string) => {
                    if (returnOutput) {
                        fullOutput += data;
                    }

                    if (printOutput) {
                        process.stdout.write(data);
                    }
                });
                childProcess.stderr?.on('data', (data: string) => {
                    if (returnOutput) {
                        fullOutput += data;
                    }

                    if (printOutput) {
                        process.stderr.write(data);
                    }
                });
                childProcess.on('close', (code) => {
                    match(code)
                        .with(0, () => resolve(fullOutput))
                        .otherwise((code) =>
                            reject(
                                `Failed to run command "${command}". Code: ${code} Message: ${fullOutput}`
                            )
                        );
                });
            }),
        either.toError
    );
};

export type RunCommandType = typeof runCommand;
