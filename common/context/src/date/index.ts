import { formatInTimeZone, fromZonedTime } from 'date-fns-tz';
import { isValid, parse } from 'date-fns';

export const TIMESTAMP_FORMAT = `yyyy-MM-dd'T'HH.mm.ss.SSS'Z'`;
export const TIME_ZONE = 'UTC';

export type GetNow = () => Date;

export const applicationVersionTimestamp = (
    getNow: GetNow = () => new Date()
): string => {
    const now = getNow();
    return formatInTimeZone(now, TIME_ZONE, TIMESTAMP_FORMAT);
};

export const parseApplicationVersionTimestamp = (
    timestamp: string
): Date | undefined => {
    const parsed = parse(timestamp, TIMESTAMP_FORMAT, new Date());
    if (!isValid(parsed)) {
        return undefined;
    }
    return fromZonedTime(parsed, TIME_ZONE);
};

export type AppVersionTimestamp = typeof applicationVersionTimestamp;
