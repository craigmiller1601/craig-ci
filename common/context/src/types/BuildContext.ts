import { ProjectType } from './ProjectType';
import { ProjectInfo } from './ProjectInfo';

export type BuildContext = Readonly<{
    projectType: ProjectType;
    projectInfo: ProjectInfo;
}>;
