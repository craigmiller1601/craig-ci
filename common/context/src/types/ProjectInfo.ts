import { RepoType } from './RepoType';
import { VersionType } from './VersionType';

export type ProjectInfo = Readonly<{
    group: string;
    name: string;
    version: string;
    versionType: VersionType;
    repoType: RepoType;
    monorepoChildren?: ReadonlyArray<ProjectInfo>;
}>;
