export type ProjectType =
    | 'NpmLibrary'
    | 'MavenLibrary'
    | 'GradleLibrary'
    | 'GradleApplication'
    | 'NpmApplication'
    | 'MavenApplication'
    | 'DockerApplication'
    | 'DockerImage'
    | 'HelmApplication'
    | 'HelmLibrary'
    | 'Unknown';

export const projectTypes: ReadonlyArray<ProjectType> = [
    'NpmLibrary',
    'MavenLibrary',
    'GradleLibrary',
    'GradleApplication',
    'NpmApplication',
    'MavenApplication',
    'DockerApplication',
    'DockerImage',
    'HelmApplication',
    'HelmLibrary',
    'Unknown'
];
