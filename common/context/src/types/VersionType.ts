export type VersionType = 'Release' | 'PreRelease' | 'Unknown';
