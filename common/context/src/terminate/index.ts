import { either, taskEither, task } from 'fp-ts';

export const terminateEither = <T>(result: either.Either<Error, T>): unknown =>
    either.fold(
        (ex: Error) => {
            console.log('Staged failed with error');
            console.error(ex);
            process.exit(1);
        },
        () => {
            console.log('Stage completed successfully');
            process.exit(0);
        }
    )(result);

export const terminateTaskEither = <T>(
    result: taskEither.TaskEither<Error, T>
): task.Task<unknown> =>
    taskEither.fold(
        (ex: Error) => () => {
            console.log('Staged failed with error');
            console.error(ex);
            process.exit(1);
        },
        () => () => {
            console.log('Stage completed successfully');
            process.exit(0);
        }
    )(result);
