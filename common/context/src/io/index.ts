import { either, function as func } from 'fp-ts';
import path from 'path';
import { BuildContext } from '../types/BuildContext';
import {
    parseJson,
    readFileSync,
    stringifyJson,
    writeFileSync
} from '@craig-ci/fp-io';

const BUILD_CONTEXT_NAME = 'build-context.json';

const getContextFilePath = (cwd: string): string =>
    path.join(cwd, BUILD_CONTEXT_NAME);

export const readContext = (
    cwd: string = process.cwd()
): either.Either<Error, BuildContext> => {
    console.log('Reading build context');
    const contextPath = getContextFilePath(cwd);
    return func.pipe(
        readFileSync(contextPath),
        either.map((jsonString) => {
            console.log('Current build context', jsonString);
            return jsonString;
        }),
        either.chain((jsonString) => parseJson<BuildContext>(jsonString))
    );
};

const prettyStringify = <T>(value: T): either.Either<Error, string> =>
    stringifyJson(value, true);

export const writeContext = (
    context: BuildContext,
    cwd: string = process.cwd()
): either.Either<Error, void> => {
    console.log('Writing build context');
    const contextPath = getContextFilePath(cwd);

    return func.pipe(
        prettyStringify(context),
        either.map((jsonString) => {
            console.log(`New Build Context: ${jsonString}`);
            return jsonString;
        }),
        either.chain((jsonString) => writeFileSync(contextPath, jsonString))
    );
};
