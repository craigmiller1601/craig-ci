import { ProjectType } from '../types/ProjectType';
import { function as func, predicate } from 'fp-ts';

export const isMaven: predicate.Predicate<ProjectType> = (projectType) =>
    ['MavenApplication', 'MavenLibrary'].includes(projectType);

export const isNpm: predicate.Predicate<ProjectType> = (projectType) =>
    ['NpmApplication', 'NpmLibrary'].includes(projectType);

export const isDocker: predicate.Predicate<ProjectType> = (projectType) =>
    ['DockerApplication', 'DockerImage'].includes(projectType);

export const isGradle: predicate.Predicate<ProjectType> = (projectType) =>
    ['GradleApplication', 'GradleLibrary'].includes(projectType);

export const isHelm: predicate.Predicate<ProjectType> = (projectType) =>
    ['HelmApplication', 'HelmLibrary'].includes(projectType);

export const isJvm: predicate.Predicate<ProjectType> = func.pipe(
    isMaven,
    predicate.or(isGradle)
);

export const isApplication: predicate.Predicate<ProjectType> = (projectType) =>
    [
        'DockerApplication',
        'MavenApplication',
        'NpmApplication',
        'GradleApplication',
        'HelmApplication'
    ].includes(projectType);
