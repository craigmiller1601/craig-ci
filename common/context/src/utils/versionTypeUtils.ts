import { predicate } from 'fp-ts';
import { VersionType } from '../types/VersionType';

export const isRelease: predicate.Predicate<VersionType> = (versionType) =>
    'Release' === versionType;

export const isPreRelease: predicate.Predicate<VersionType> = (versionType) =>
    'PreRelease' === versionType;
