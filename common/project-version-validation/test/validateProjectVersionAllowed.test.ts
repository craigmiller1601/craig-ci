import { test, vi, beforeEach, MockedFunction, expect } from 'vitest';
import { VersionType } from '@craig-ci/context/types/VersionType';
import { addDays, subDays } from 'date-fns';
import { applicationVersionTimestamp } from '@craig-ci/context/date';
import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { validateProjectVersionAllowed } from '../src';
import { NexusRepoGroupSearchFn } from '@craig-ci/nexus-api';
import { NexusSearchResult } from '@craig-ci/nexus-api/NexusSearchResult';
import { taskEither } from 'fp-ts';
import { ProjectType } from '@craig-ci/context/types/ProjectType';

const createBuildContext = (
    version: string,
    versionType: VersionType,
    projectType: ProjectType
): BuildContext => ({
    projectType,
    projectInfo: {
        group: 'craigmiller160',
        name: 'project',
        version,
        versionType,
        repoType: 'polyrepo'
    }
});

type LibraryArgs = Readonly<{
    version: string;
    versionType: VersionType;
    allowed: boolean;
}>;

beforeEach(() => {
    vi.resetAllMocks();
});

const mockSearchFn: MockedFunction<NexusRepoGroupSearchFn> = vi.fn();
const createNexusSearchResult = (version: string): NexusSearchResult => ({
    items: [
        {
            version,
            name: '',
            group: '',
            assets: [],
            id: '',
            format: '',
            repository: ''
        }
    ]
});

// Existing version is 1.1.0
test.each<LibraryArgs>([
    { version: '2.0.0', versionType: 'Release', allowed: true },
    { version: '1.0.0', versionType: 'Release', allowed: false },
    { version: '1.3.0-SNAPSHOT', versionType: 'PreRelease', allowed: true },
    { version: '1.3.0-beta', versionType: 'PreRelease', allowed: true },
    { version: '1.0.0-SNAPSHOT', versionType: 'PreRelease', allowed: false },
    { version: '1.0.0-beta', versionType: 'PreRelease', allowed: false }
])(
    'Validating semver library versions for version $version',
    async ({ version, versionType, allowed }) => {
        const nexusResult = createNexusSearchResult('1.1.0');
        mockSearchFn.mockImplementation(() => taskEither.right(nexusResult));
        const context = createBuildContext(
            version,
            versionType,
            'MavenLibrary'
        );

        const result = await validateProjectVersionAllowed(
            context,
            mockSearchFn
        )();

        if (allowed) {
            expect(result).toBeRight();
        } else {
            expect(result).toBeLeft();
        }
    }
);

const existingTimestamp = new Date();
const invalidTimestamp = subDays(existingTimestamp, 1);
const validTimestsamp = addDays(existingTimestamp, 1);

type AppArgs = Readonly<{
    version: string;
    allowed: boolean;
}>;

test.each<AppArgs>([
    {
        version: applicationVersionTimestamp(() => invalidTimestamp),
        allowed: false
    },
    {
        version: applicationVersionTimestamp(() => validTimestsamp),
        allowed: true
    },
    {
        version: 'abcdefg',
        allowed: false
    }
])(
    'Validating timestamp application versions for version $version',
    async ({ version, allowed }) => {
        const nexusResult = createNexusSearchResult(
            applicationVersionTimestamp(() => existingTimestamp)
        );
        mockSearchFn.mockImplementation(() => taskEither.right(nexusResult));
        const context = createBuildContext(
            version,
            'Release',
            'MavenApplication'
        );

        const result = await validateProjectVersionAllowed(
            context,
            mockSearchFn
        )();

        if (allowed) {
            expect(result).toBeRight();
        } else {
            expect(result).toBeLeft();
        }
    }
);
