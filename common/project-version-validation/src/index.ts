import { BuildContext } from '@craig-ci/context/types/BuildContext';
import { taskEither, function as func, readonlyArray, either } from 'fp-ts';
import { NexusRepoGroupSearchFn } from '@craig-ci/nexus-api';
import { NexusSearchResult } from '@craig-ci/nexus-api/NexusSearchResult';
import { ProjectInfo } from '@craig-ci/context/types/ProjectInfo';
import { isRelease } from '@craig-ci/context/utils/versionTypeUtils';
import semver from 'semver';
import { isApplication } from '@craig-ci/context/utils/projectTypeUtils';
import { parseApplicationVersionTimestamp } from '@craig-ci/context/date';
import { compareAsc } from 'date-fns';

const isSemverVersionValid = (
    nexusResult: NexusSearchResult,
    version: string
): boolean =>
    func.pipe(
        nexusResult.items,
        readonlyArray.map((item) => item.version),
        readonlyArray.filter(
            (itemVersion) =>
                !itemVersion.includes('SNAPSHOT') ||
                !itemVersion.includes('beta')
        ),
        readonlyArray.filter(
            (itemVersion) => semver.compare(version, itemVersion) <= 0
        ),
        readonlyArray.isEmpty
    );

const isTimestampVersionValid = (
    nexusResult: NexusSearchResult,
    version: string
): either.Either<Error, boolean> => {
    const parsedVersion = parseApplicationVersionTimestamp(version);
    if (!parsedVersion) {
        return either.left(
            new Error(`Version is not valid timestamp: ${version}`)
        );
    }

    return func.pipe(
        nexusResult.items,
        readonlyArray.map((item) =>
            parseApplicationVersionTimestamp(item.version)
        ),
        readonlyArray.filter((version): version is Date => !!version),
        readonlyArray.filter(
            (version) => compareAsc(parsedVersion, version) <= 0
        ),
        readonlyArray.isEmpty,
        either.right
    );
};

const doTimestampReleaseVersionValidation = (
    projectInfo: ProjectInfo,
    searchFn: NexusRepoGroupSearchFn
): taskEither.TaskEither<Error, ProjectInfo> =>
    func.pipe(
        searchFn(projectInfo.group, projectInfo.name),
        taskEither.chainEitherK((nexusResult) =>
            isTimestampVersionValid(nexusResult, projectInfo.version)
        ),
        taskEither.filterOrElse(
            func.identity,
            () =>
                new Error(
                    `Project version is invalid. Project: ${projectInfo.group}:${projectInfo.name} Version: ${projectInfo.version}`
                )
        ),
        taskEither.map(() => projectInfo)
    );

const doSemverReleaseVersionValidation = (
    projectInfo: ProjectInfo,
    searchFn: NexusRepoGroupSearchFn
): taskEither.TaskEither<Error, ProjectInfo> =>
    func.pipe(
        searchFn(projectInfo.group, projectInfo.name),
        taskEither.filterOrElse(
            (nexusResult) =>
                isSemverVersionValid(nexusResult, projectInfo.version),
            () =>
                new Error(
                    `Project version is invalid. Project: ${projectInfo.group}:${projectInfo.name} Version: ${projectInfo.version}`
                )
        ),
        taskEither.map(() => projectInfo)
    );

const doPreReleaseVersionValidation = (
    projectInfo: ProjectInfo,
    searchFn: NexusRepoGroupSearchFn
): taskEither.TaskEither<Error, ProjectInfo> => {
    const strippedVersion = projectInfo.version.replace(
        /-(SNAPSHOT|beta)$/,
        ''
    );
    return func.pipe(
        searchFn(projectInfo.group, projectInfo.name),
        taskEither.filterOrElse(
            (nexusResult) => isSemverVersionValid(nexusResult, strippedVersion),
            () =>
                new Error(
                    `Project version is invalid. Project: ${projectInfo.group}:${projectInfo.name} Version: ${projectInfo.version}`
                )
        ),
        taskEither.map(() => projectInfo)
    );
};

export const validateProjectVersionAllowed = (
    context: BuildContext,
    searchForVersions: NexusRepoGroupSearchFn
): taskEither.TaskEither<Error, BuildContext> => {
    const projectInfos: ReadonlyArray<ProjectInfo> = [
        context.projectInfo,
        ...(context.projectInfo.monorepoChildren ?? [])
    ];

    return func.pipe(
        projectInfos,
        readonlyArray.map((projectInfo) => {
            if (
                isRelease(projectInfo.versionType) &&
                !isApplication(context.projectType)
            ) {
                return doSemverReleaseVersionValidation(
                    projectInfo,
                    searchForVersions
                );
            }

            if (isRelease(projectInfo.versionType)) {
                return doTimestampReleaseVersionValidation(
                    projectInfo,
                    searchForVersions
                );
            }

            return doPreReleaseVersionValidation(
                projectInfo,
                searchForVersions
            );
        }),
        taskEither.sequenceArray,
        taskEither.map(() => context)
    );
};
