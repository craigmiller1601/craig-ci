import { either, function as func } from 'fp-ts';
import path from 'path';
import { existsSync, readFileSync, parseYaml } from '@craig-ci/fp-io';

export type ChartType = 'application' | 'library';
export const helmChartTypes: ReadonlyArray<ChartType> = [
    'application',
    'library'
];

export type ChartDependency = Readonly<{
    name: string;
    alias?: string;
    repository: string;
    version: string;
}>;

export type Chart = Readonly<{
    apiVersion: string;
    name: string;
    type: ChartType;
    version: string;
    appVersion?: string;
    dependencies?: ReadonlyArray<ChartDependency>;
}>;

const getChartFilePath = (cwd: string): string =>
    path.join(cwd, 'deploy', 'chart', 'Chart.yaml');

export const readHelmChart = (
    cwd: string = process.cwd()
): either.Either<Error, Chart> => {
    const chartFilePath = getChartFilePath(cwd);
    if (!existsSync(chartFilePath)) {
        return either.left(new Error(`Cannot find ${chartFilePath}`));
    }

    return func.pipe(
        readFileSync(chartFilePath),
        either.chain((yamlString) => parseYaml<Chart>(yamlString))
    );
};
