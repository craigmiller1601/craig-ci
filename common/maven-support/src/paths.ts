import path from 'path';

export const MAVEN_PROJECT_FILE = 'pom.xml';
export const DEPLOY_DIRECTORY = 'deploy';

export const getMavenProjectFilePath = (cwd: string = process.cwd()): string =>
    path.join(cwd, MAVEN_PROJECT_FILE);

export const getDeployDirectory = (cwd: string = process.cwd()): string =>
    path.join(cwd, DEPLOY_DIRECTORY);
