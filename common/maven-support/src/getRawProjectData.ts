import { either, function as func } from 'fp-ts';
import { PomXml } from './PomXml';
import { getMavenProjectFilePath } from './paths';
import { existsSync, parseXml, readFileSync } from '@craig-ci/fp-io';

export const getRawProjectData = (
    cwd: string = process.cwd()
): either.Either<Error, PomXml> => {
    const mavenProjectFilePath = getMavenProjectFilePath(cwd);

    if (!existsSync(mavenProjectFilePath)) {
        return either.left(new Error('Could not find maven project file'));
    }

    return func.pipe(
        readFileSync(mavenProjectFilePath),
        either.chain((file) => parseXml<PomXml>(file))
    );
};
