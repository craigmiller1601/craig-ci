import { test, expect } from 'vitest';
import path from 'path';
import { getRawProjectData } from '../src/getRawProjectData';
import { PomXml } from '../src/PomXml';

const WORKING_DIR_ROOT = path.join(
    __dirname,
    '..',
    '..',
    '..',
    '__working_directories__'
);

test('pom.xml does not exist', () => {
    const result = getRawProjectData(WORKING_DIR_ROOT);
    expect(result).toEqualLeft(new Error('Could not find maven project file'));
});

test('parses pom.xml', () => {
    const cwd = path.join(WORKING_DIR_ROOT, 'mavenReleaseLibrary');
    const result = getRawProjectData(cwd);
    const expectedPomXml: PomXml = {
        project: expect.objectContaining<PomXml['project']>({
            groupId: ['io.craigmiller160'],
            artifactId: ['email-service'],
            version: ['1.2.0'],
            properties: [
                {
                    'api.test.processor.version': ['1.2.2'],
                    'web.utils.version': ['1.1.2'],
                    'postgres.version': ['42.2.18']
                }
            ],
            dependencies: [
                {
                    dependency: [
                        {
                            groupId: ['org.springframework.boot'],
                            artifactId: ['spring-boot-starter-actuator']
                        },
                        {
                            groupId: ['org.postgresql'],
                            artifactId: ['postgresql'],
                            version: ['${postgres.version}']
                        },
                        {
                            groupId: ['io.craigmiller160'],
                            artifactId: ['api-test-processor'],
                            version: ['${api.test.processor.version}'],
                            scope: ['test']
                        },
                        {
                            groupId: ['io.craigmiller160'],
                            artifactId: ['spring-web-utils'],
                            version: ['${web.utils.version}']
                        }
                    ]
                }
            ]
        }) as PomXml['project']
    };
    expect(result).toEqualRight(expectedPomXml);
});
