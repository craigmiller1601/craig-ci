import { defineConfig } from 'vitest/config';
import path from 'path';

export default defineConfig({
    test: {
        root: path.join(process.cwd(), 'test'),
        setupFiles: [path.join(process.cwd(), 'test', 'setup.ts')]
    }
});
