#!/bin/bash

LIB_DIR=./lib
TYPES_DIR=./types
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SWC_CONFIG="$SCRIPT_DIR/../../swc/.swcrc_ts.json"

if [ -d "$LIB_DIR" ]; then
  echo "Cleaning existing lib directory"
  rm -rf "$LIB_DIR"
fi

if [ -d "$TYPES_DIR" ]; then
  echo "Cleaning existing types directory"
  rm -rf "$TYPES_DIR"
fi

echo "Compiling source files"
swc ./src -d ./lib --config-file "$SWC_CONFIG"
mv ./lib/src/* ./lib
rm -rf ./lib/src

echo "Generating type files"
tsc --declaration --emitDeclarationOnly --outDir "$TYPES_DIR"