import { either, function as func } from 'fp-ts';
import { PackageJson } from './types';
import { getNpmProjectFile } from './constants';
import { parseJson, readFileSync } from '@craig-ci/fp-io';

export const readNpmProjectFile = (
    cwd: string = process.cwd()
): either.Either<Error, PackageJson> => {
    const projectFile = getNpmProjectFile(cwd);
    return func.pipe(
        readFileSync(projectFile),
        either.chain((json) => parseJson<PackageJson>(json))
    );
};
