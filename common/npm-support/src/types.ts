export type Dependencies = Readonly<{
    [key: string]: string;
}>;

export type PackageJson = Readonly<{
    name: string;
    version: string;
    dependencies?: Dependencies;
    devDependencies?: Dependencies;
    peerDependencies?: Dependencies;
    publishDirectory?: string;
    files?: ReadonlyArray<string>;
}>;
