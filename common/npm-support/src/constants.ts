import path from 'path';

const NPM_PROJECT_FILE = 'package.json';
const DEPLOY_DIR = 'deploy';
const NPM_TOOL_FILE = 'package-lock.json';
const PNPM_TOOL_FILE = 'pnpm-lock.yaml';
const YARN_TOOL_FILE = 'yarn.lock';

export const getNpmProjectFile = (cwd: string): string =>
    path.join(cwd, NPM_PROJECT_FILE);
export const getDeployDir = (cwd: string): string => path.join(cwd, DEPLOY_DIR);
export const getNpmToolFile = (cwd: string): string =>
    path.join(cwd, NPM_TOOL_FILE);
export const getPnpmToolFile = (cwd: string): string =>
    path.join(cwd, PNPM_TOOL_FILE);
export const getYarnToolFile = (cwd: string): string =>
    path.join(cwd, YARN_TOOL_FILE);
