import { either } from 'fp-ts';
import yaml from 'yaml';

export const parseYaml = <T>(yamlString: string): either.Either<Error, T> =>
    either.tryCatch(() => yaml.parse(yamlString) as T, either.toError);

export const stringifyYaml = <T>(obj: T): either.Either<Error, string> =>
    either.tryCatch(() => yaml.stringify(obj), either.toError);
