import { either } from 'fp-ts';

export const parseJson = <T>(json: string): either.Either<Error, T> =>
    either.tryCatch(() => JSON.parse(json) as T, either.toError);

export const stringifyJson = <T>(
    obj: T,
    indent: boolean = false
): either.Either<Error, string> =>
    either.tryCatch(
        () => JSON.stringify(obj, null, indent ? 2 : undefined),
        either.toError
    );
