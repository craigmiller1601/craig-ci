import { either, function as func } from 'fp-ts';
import fs from 'fs';

export const readFileSync = (filePath: string): either.Either<Error, string> =>
    either.tryCatch(() => fs.readFileSync(filePath, 'utf8'), either.toError);

export const existsSync = (filePath: string): boolean =>
    func.pipe(
        either.tryCatch(() => fs.existsSync(filePath), either.toError),
        either.fold(() => false, func.identity)
    );

export const writeFileSync = (
    filePath: string,
    content: string
): either.Either<Error, void> =>
    either.tryCatch(() => fs.writeFileSync(filePath, content), either.toError);

export const readdirSync = (
    dirPath: string
): either.Either<Error, ReadonlyArray<string>> =>
    either.tryCatch(() => fs.readdirSync(dirPath), either.toError);

type MkdirOptions = Readonly<{
    recursive: boolean;
}>;
export const mkdirSync = (
    dirPath: string,
    options?: MkdirOptions
): either.Either<Error, void> =>
    func.pipe(
        either.tryCatch(() => fs.mkdirSync(dirPath, options), either.toError),
        either.map(() => func.constVoid())
    );

type CpOptions = Partial<
    Readonly<{
        recursive: boolean;
        force: boolean;
    }>
>;
export const cpSync = (
    srcPath: string,
    destPath: string,
    options?: CpOptions
): either.Either<Error, void> =>
    either.tryCatch(
        () => fs.cpSync(srcPath, destPath, options),
        either.toError
    );

export const statSync = (filePath: string): either.Either<Error, fs.Stats> =>
    either.tryCatch(() => fs.statSync(filePath), either.toError);
