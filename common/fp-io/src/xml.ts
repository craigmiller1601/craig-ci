import { Parser } from 'xml2js';
import { either, option, function as func } from 'fp-ts';

export const parseXml = <T>(xml: string): either.Either<Error, T> => {
    const parser = new Parser();
    return func.pipe(
        either.tryCatch(() => {
            let parsed: option.Option<T> = option.none;
            parser.parseString(xml, (error: Error | null, result: T) => {
                if (error) {
                    throw error;
                }
                parsed = option.some(result);
            });
            return parsed;
        }, either.toError),
        either.chain(
            either.fromOption(() => new Error('No parsed XML to return'))
        )
    );
};
