const path = require('path');
const fs = require('fs');
const TerserPlugin = require('terser-webpack-plugin');

const swcDirPath = path.join(__dirname, '..', '..', 'swc');
const swcJsPath = path.join(swcDirPath, '.swcrc_js.json');
const swcTsPath = path.join(swcDirPath, '.swcrc_ts.json');
const swcJsConfig = JSON.parse(fs.readFileSync(swcJsPath, 'utf8'));
const swcTsConfig = JSON.parse(fs.readFileSync(swcTsPath, 'utf8'));

const createConfig = (fileName) => ({
    mode: 'production',
    entry: path.join(process.cwd(), 'src', 'index.ts'),
    target: 'node',
    output: {
        path: path.join(process.cwd(), 'build'),
        filename: `${fileName}.js`,
        clean: true
    },
    resolve: {
        extensions: ['.ts', '.mts', '.cts', '.js', '.mjs', '.cjs'],
        symlinks: true
    },
    module: {
        rules: [
            {
                test: /\.[cm]?ts$/,
                exclude: /node_modules/,
                use: {
                    loader: 'swc-loader',
                    options: swcTsConfig
                }
            },
            {
                test: /\.[cm]?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'swc-loader',
                    options: swcJsConfig
                }
            }
        ]
    },
    optimization: {
        usedExports: true,
        minimize: true,
        minimizer: [new TerserPlugin()]
    }
});

module.exports = {
    createConfig
};
