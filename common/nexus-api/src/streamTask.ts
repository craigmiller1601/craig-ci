import { taskEither, either } from 'fp-ts';
import { Stream } from 'stream';

export const streamTask = (
    stream: Stream
): taskEither.TaskEither<Error, void> =>
    taskEither.tryCatch(
        () =>
            new Promise((resolve, reject) => {
                stream.on('finish', resolve);
                stream.on('error', reject);
            }),
        either.toError
    );
