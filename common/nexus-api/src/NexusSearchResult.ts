export type NexusSearchResultAsset = Readonly<{
    downloadUrl: string;
    path: string;
    id: string;
}>;

export type NexusSearchResultItem = Readonly<{
    id: string;
    repository: string;
    format: string;
    group: string;
    name: string;
    version: string;
    assets: ReadonlyArray<NexusSearchResultAsset>;
}>;

export type NexusSearchResult = Readonly<{
    items: ReadonlyArray<NexusSearchResultItem>;
}>;
