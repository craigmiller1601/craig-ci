import { taskEither } from 'fp-ts';
import { AxiosResponse } from 'axios';

export const extractResponseData = <T>(
    resTaskEither: taskEither.TaskEither<Error, AxiosResponse<T>>
): taskEither.TaskEither<Error, T> =>
    taskEither.map((res: AxiosResponse<T>) => res.data)(resTaskEither);
